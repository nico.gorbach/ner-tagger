import axios from "axios";
import baseURL from "../constants";

// creating a custom instance of axios make fetches more convenient by giving custom defaults
const Axios = axios.create({
  baseURL
});

Axios.defaults.headers.post["Content-Type"] = "application/json";

// Use the axios instance exported from here instead of importing the default one when fetching data
export default Axios;
