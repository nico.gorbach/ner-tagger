let baseURL = "";

if (process.env.NODE_ENV === "development") {
  baseURL = "http://localhost:8000/backend/api/";
} else {
  baseURL = "http://data-and-ai.westeurope.cloudapp.azure.com/backend/api/";
}

export default baseURL;
