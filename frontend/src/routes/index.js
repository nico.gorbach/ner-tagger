import Login from "../components/Login";
import MiniDrawer from "../components/MiniDrawer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "../components/PrivateRoute";

function Routes() {
  return (
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <PrivateRoute path="/" component={MiniDrawer} />
        <PrivateRoute path="/404" component={MiniDrawer} />
      </Switch>
    </Router>
  );
}

export default Routes;
