import styled from "styled-components";

export const MainContainer = styled.div`
  height: 100%;
  background-color: #5cb8b5;
  height: calc(100vh - 80px);
`;

export const SubContainer = styled.div`
  height: calc(100% - 50px);
  overflow: scroll;
  margin-top: 50px;
`;

export const SubSubContainer = styled.div`
  display: grid;
  grid-template-rows: 230px 180px 170px;
  padding: 0 20px;
`;

export const Section = styled.div`
  width: 100%;
  display: grid;
  grid-template-rows: 2fr 20fr 2fr;
  justify-items: center;
`;

export const Seperator = styled.hr`
  background-color: rgb(220, 220, 220);
  border: none;
  height: 1px;
  width: 100%;
  margin: 0;
`;

export const Contents = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

export const Contents2 = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;
