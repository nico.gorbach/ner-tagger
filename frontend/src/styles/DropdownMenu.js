import styled from "styled-components";

export const DropDownContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 6fr;
  gap: 10px;
  justify-content: space-between;
`;

