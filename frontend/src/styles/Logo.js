import styled from "styled-components";

export const LogoContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: -50px;
  &.small-window-width {
    display: none;
  }
`;

export const Logo = styled.img`
  height: 30px;
`;
