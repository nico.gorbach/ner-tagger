import styled from "styled-components";

export const DocBody = styled.div`
  width: 100%;
  height: calc(100vh - 80px);
  z-index: 1;
  &.highlight-disabled {
    cursor: default;
    user-select: none;
  }
`;

export const DocContainer = styled.div`
  /* padding: 60px; */
  /* padding-top: 35px; */
  overflow: scroll;
  border-top-left-radius: 20px;
  background-color: rgb(252, 252, 252);
  height: 100%;
  width: 100%;
  &.small-window-width {
    border-top-right-radius: 20px;
  }
`;

export const Doc = styled.div`
  text-align: justify;
  color: rgb(70, 70, 70);
  letter-spacing: 2px;
  font-family: Roboto;
  line-height: 30px;
  font-size: 15px;
  padding: 50px;
  padding-top: 40px;
`;

export const Character = styled.span`
  background-color: ${(props) =>
    props.searchChar ? props.searchChar.background_color_hex : null};
  color: ${(props) =>
    props.searchChar ? props.searchChar.font_color_hex : null};
  &::selection {
    background-color: ${(props) => props.backgroundcolor};
    color: ${(props) => props.color};
  }
`;

export const AnnotatedText = styled.span`
  /* color: rgb(40, 40, 40); */
  color: ${(props) => props.color};
  padding: 5px 0px 6px 0px;
  background-color: ${(props) => props.backgroundcolor};
  border-radius: 3px;
  cursor: pointer;
`;
