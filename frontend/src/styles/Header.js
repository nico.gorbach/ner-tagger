import styled from "styled-components";

export const MainContainer = styled.div`
  width: 100%;
  background-color: #5cb8b5;
  margin: 0px;
  display: grid;
  grid-template-columns: 260px 1fr;
  &.small-window-width {
    grid-template-columns: 0 1fr;
  }
`;

export const LeftSection = styled.div`
  &.new-class {
    background-color: yellow;
  }
`;

export const RightSection = styled.div`
  display: flex;
  justify-content: center;
  padding: 0px 50px;
`;

export const RightSubSection = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
