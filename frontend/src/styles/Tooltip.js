import styled from "styled-components";
import ReactTooltip from "react-tooltip";

export const Tooltip = styled(ReactTooltip)`
  &.custom {
    padding: 0.5px;
    padding-left: 6px;
    padding-right: 6px;
    font-family: Roboto;
    background-color: rgba(0, 0, 0, 0.7);
    &.clear-tip {
      background-color: ${(props) => props.backgroundcolor};
      color: ${(props) => props.color};
      letter-spacing: 5px;
      opacity: 1 !important;
    }
    &.clear-tip2 {
      background-color: ${(props) => props.backgroundcolor};
      color: ${(props) => props.color};
      letter-spacing: 5px;
      opacity: 1 !important;
      width: 16px;
      height: 16px;
      display: flex;
      justify-content: center;
      border-radius: 20px;
      cursor: pointer;
      padding: 0;
    }
  }
`;
