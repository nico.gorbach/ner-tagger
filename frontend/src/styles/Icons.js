import styled from "styled-components";
import { ReactComponent as Icon } from "../assets/next.svg";

export const NextIcon = styled(Icon)`
  height: 60px;
  color: white;
  cursor: pointer;
  border-radius: 100px;
  padding: 10px;
  &:hover {
    background-color: rgba(100, 100, 100, 0.1);
  }
`;

export const PreviousIcon = styled(NextIcon)`
  transform: rotate(180deg);
`;

export const NextButtonsContainer = styled.div`
  width: 120px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 0px;
  z-index: 99999;
  height: 100%;
`;
