import styled from "styled-components";

export const ProgressContainer = styled.div``;

export const ProgressIndicator = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  margin: 0 auto;
  font-size: 1.6em;
  font-weight: 300;
  color: #798687;
  user-select: none;
  padding-bottom: 3px;
`;
