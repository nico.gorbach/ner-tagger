import styled from "styled-components";
import ToggleButton from "react-bootstrap/ToggleButton";

export const ToggleButtonContainer = styled.div``;

export const ToggleButtonCustom = styled(ToggleButton)`
  text-align: center;
  letter-spacing: 4px;
  border: solid 1px white !important;
  box-shadow: none !important;
  font-family: Roboto;
  color: white;
  background-color: transparent;
  &:hover {
    color: ${(props) => props.activecolor.fontColor} !important;
    background-color: ${(props) => props.activecolor.background} !important;
  }
  &.custom-buttom-active {
    color: ${(props) => props.activecolor.fontColor} !important;
    background-color: ${(props) => props.activecolor.background} !important;
  }
`;

export const ToggleButtonOnOffContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 0;
  width: 80%;
`;

export const ToggleButtonOnOffSubContainer = styled.div`
  padding: 8px;
  border-radius: 30px;
  &:hover {
    background-color: rgba(100, 100, 100, 0.1);
  }
`;
