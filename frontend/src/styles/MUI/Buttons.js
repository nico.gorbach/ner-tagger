import styled from "styled-components";
import { Button, ButtonGroup, IconButton } from "@material-ui/core/";

export const MUIButton = styled(Button)`
  background-color: #90cdcb;
  &:focus {
    outline-color: #bfe2e0;
  }
  &:hover {
    background-color: #b4dddc;
  }
`;

export const MUIButtonGroup = styled(ButtonGroup)`
  background-color: ${(props) => props.backgroundcolor};
  color: ${(props) => props.color};
  letter-spacing: 4px;
  font-family: Roboto;
`;

export const MUIIconButton = styled(IconButton)`
  &:focus {
    outline: none;
  }
  border-radius: 100px !important;
`;

export const MUISubmitButtonIcon = styled(MUIIconButton)`
  padding: 8px;
  position: absolute;
  right: 24px;
  top: 100px;
  /* background-color: rgb(200, 200, 200); */
  border-radius: 100px;
  &:hover {
    /* background-color: #b4dddc; */
    background-color: rgb(240, 240, 240);
  }
  &:focus {
    /* outline-color: #bfe2e0; */
    outline: none;
    border-radius: 100px;
  }
  cursor: pointer;
`;
