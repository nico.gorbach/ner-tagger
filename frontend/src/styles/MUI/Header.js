import styled from "styled-components";
import { Toolbar } from "@material-ui/core/";

export const MUIToolbar = styled(Toolbar)`
  background-color: #5cb8b5;
`;
