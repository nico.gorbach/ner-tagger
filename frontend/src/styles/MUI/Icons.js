import styled from "styled-components";
import { PublishOutlined, Menu } from "@material-ui/icons";

export const MUIPublishOutlinedIcon = styled(PublishOutlined)`
  font-size: 20px;
`;

export const MUIMenuIcon = styled(Menu)`
  fill: ${(props) => props.color};
  &:focus {
    outline: none;
  }
`;
