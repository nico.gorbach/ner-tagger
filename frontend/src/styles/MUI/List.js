import styled from "styled-components";
import { List, ListItem } from "@material-ui/core/";

export const MUIList = styled(List)`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

export const MUIListItem = styled(ListItem)`
  && {
    &:hover {
      background-color: ${(props) =>
        props.isdraweropen ? "Transparent" : "#79bdba"};
      cursor: ${(props) => (props.isdraweropen ? "default" : "pointer")};
    }
  }
`;
