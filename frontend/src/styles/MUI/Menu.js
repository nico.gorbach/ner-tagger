import styled from "styled-components";
import { MenuItem, Button } from "@material-ui/core/";

export const MUIMenuItem = styled(MenuItem)`
  color: ${(props) =>
    props.index === props.selectedindex ? props.color : "black"};
  background-color: ${(props) =>
    props.index === props.selectedindex ? props.backgroundcolor : "white"};
  letter-spacing: 4px;
  font-family: Roboto;
  &:hover {
    background-color: ${(props) => props.backgroundcolor};
    color: ${(props) => props.color};
  }
  font-size: 15px;
`;

export const MUIMenuButton = styled(Button)`
  background-color: ${(props) => props.backgroundcolor};
  color: ${(props) => props.color};
  letter-spacing: 4px;
  font-family: Roboto;
  min-width: 160px;
  text-transform: none;
  &:hover {
    background-color: ${(props) => props.backgroundcolor};
  }
  &:focus {
    outline: none;
  }
  cursor: default !important;
`;
