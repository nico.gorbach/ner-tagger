import styled from "styled-components";
import { Drawer } from "@material-ui/core/";

export const MUIDrawer = styled(Drawer)`
  background-color: #b5e0de;
`;
