import { combineReducers } from "redux";
import annot from "./annotation";
import settings from "./settings";
import auth from "./auth";

const rootReducer = combineReducers({
  auth,
  annot,
  settings,
});

export default rootReducer;
