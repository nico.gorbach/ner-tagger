import {
  SET_DOC,
  SET_LOADING_DOC,
  SET_DOC_ANNOTATIONS,
  SET_NODES,
  SET_SELECTED_TEXT_START_ACTION,
  SET_ANNOT_DISTRIBUTION,
} from "../../actions/types";

const initState = {
  doc: { text: null, textArray: [] },
  loadingDoc: false,
  docAnnotations: [],
  nodes: [],
  selectedTextStart: null,
  annotDistrib: [],
};

export default function annot(state = initState, action) {
  switch (action.type) {
    case SET_DOC: {
      return {
        ...state,
        doc: action.payload,
        docAnnotations: [],
      };
    }
    case SET_LOADING_DOC: {
      return { ...state, loadingDoc: action.payload };
    }
    case SET_DOC_ANNOTATIONS: {
      return { ...state, docAnnotations: action.payload };
    }
    case SET_NODES: {
      return { ...state, nodes: action.payload, loadingDoc: false };
    }
    case SET_SELECTED_TEXT_START_ACTION: {
      return { ...state, selectedTextStart: action.payload };
    }
    case SET_ANNOT_DISTRIBUTION: {
      return { ...state, annotDistrib: action.payload };
    }
    default:
      return state;
  }
}
