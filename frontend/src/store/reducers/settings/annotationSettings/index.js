import {
  SET_DO_SMART_ANNOTATE,
  SET_DO_SELECT_WHITE_SPACE,
} from "../../../actions/types";

const initState = {
  doSmartAnnotate: true,
  doSelectWhiteSpace: false,
};

export default function annot(state = initState, action) {
  switch (action.type) {
    case SET_DO_SMART_ANNOTATE: {
      return { ...state, doSmartAnnotate: action.payload };
    }
    case SET_DO_SELECT_WHITE_SPACE: {
      return { ...state, doSelectWhiteSpace: action.payload };
    }
    default:
      return state;
  }
}
