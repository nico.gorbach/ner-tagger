import { combineReducers } from "redux";
import config from "./config";
import annot from "./annotationSettings";

const settings = combineReducers({
  config,
  annot,
});

export default settings;
