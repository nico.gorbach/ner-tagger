export default function updateSelectedProjectDependentStates(
  selectedProject,
  state
) {
  const taskOptions = state.projectOptions
    .filter((project) => project.name === selectedProject.name)
    .map((project) => project.tasks)
    .flat();

  const selectedTasks = taskOptions.filter((task) => !task.optional);

  const searchStringOptions = taskOptions
    .filter((task) =>
      selectedTasks.map((selectedTask) => selectedTask.name).includes(task.name)
    )
    .map((task) => task.search_strings)
    .flat()
    .filter(
      (item, index, array) =>
        array.map((arrayItem) => arrayItem.name).indexOf(item.name) === index
    );

  const selectedSearchStrings = searchStringOptions.filter(
    (searchStr) => !searchStr.optional
  );

  const annotationLabels = taskOptions
    .filter((task) =>
      selectedTasks.map((selectedTask) => selectedTask.name).includes(task.name)
    )
    .map((task) => task.annotation_labels)
    .flat()
    .filter(
      (item, index, array) =>
        array.map((arrayItem) => arrayItem.name).indexOf(item.name) === index
    );

  const currentLabel = !annotationLabels.includes(state.currentLabel)
    ? annotationLabels[0]
    : state.currentLabel;

  return {
    taskOptions,
    selectedTasks,
    searchStringOptions,
    annotationLabels,
    currentLabel,
    selectedSearchStrings,
  };
}
