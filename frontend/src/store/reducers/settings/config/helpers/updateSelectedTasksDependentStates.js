export default function updateSelectedTasksDependentStates(
  selectedTasks,
  state
) {
  const searchStringOptions = state.taskOptions
    .filter((task) =>
      selectedTasks.map((item) => item.name).includes(task.name)
    )
    .map((task) => task.search_strings)
    .flat()
    .filter(
      (item, index, array) =>
        array.map((arrayItem) => arrayItem.name).indexOf(item.name) === index
    );
  const annotationLabels = state.taskOptions
    .filter((task) =>
      selectedTasks.map((selectedTask) => selectedTask.name).includes(task.name)
    )
    .map((task) => task.annotation_labels)
    .flat()
    .filter(
      (item, index, array) =>
        array.map((arrayItem) => arrayItem.name).indexOf(item.name) === index
    );
  const currentLabel = !annotationLabels.includes(state.currentLabel)
    ? annotationLabels[0]
    : state.currentLabel;

  return { searchStringOptions, annotationLabels, currentLabel };
}
