import {
  SET_CORPUS,
  SET_PROJECT_OPTIONS,
  SET_SELECTED_PROJECT,
  SET_TASK_OPTIONS,
  SET_SELECTED_TASKS,
  SET_SEARCH_STRING_OPTIONS,
  SET_SELECTED_SEARCH_STRINGS,
  SET_ANNOTATION_LABELS,
  SET_CURRENT_LABEL,
} from "../../../actions/types";
import updateSelectedProjectDependentStates from "./helpers/updateSelectedProjectDependentStates";
import updateSelectedTasksDependentStates from "./helpers/updateSelectedTasksDependentStates";
import configData from "../../../../Config";
import corpus from "../../../../Data/datastock_CV_samples.json";

const initState = {
  corpus,
  projectOptions: configData,
  selectedProject: null,
  taskOptions: [],
  selectedTasks: [],
  annotationLabels: [],
  searchStringOptions: [],
  selectedSearchStrings: [],
  currentLabel: null,
};

export default function config(state = initState, action) {
  switch (action.type) {
    case SET_CORPUS: {
      return { ...state, corpus: action.payload };
    }
    case SET_PROJECT_OPTIONS: {
      return {
        ...state,
        projectOptions: action.payload,
      };
    }
    case SET_SELECTED_PROJECT: {
      const selectedProject = action.payload;
      const {
        taskOptions,
        selectedTasks,
        searchStringOptions,
        selectedSearchStrings,
        annotationLabels,
        currentLabel,
      } = updateSelectedProjectDependentStates(selectedProject, state);
      return {
        ...state,
        selectedProject,
        taskOptions,
        selectedTasks,
        searchStringOptions,
        selectedSearchStrings,
        annotationLabels,
        currentLabel,
      };
    }
    case SET_TASK_OPTIONS: {
      const taskOptions = action.payload;
      const selectedTasks = action.payload.filter((task) => !task.optional);
      return {
        ...state,
        taskOptions,
        selectedTasks,
      };
    }
    case SET_SELECTED_TASKS: {
      const selectedTasks = action.payload;
      const {
        searchStringOptions,
        annotationLabels,
        currentLabel,
      } = updateSelectedTasksDependentStates(selectedTasks, state);
      return {
        ...state,
        selectedTasks,
        searchStringOptions,
        annotationLabels,
        currentLabel,
      };
    }
    case SET_ANNOTATION_LABELS: {
      return { ...state, annotationLabels: action.payload };
    }
    case SET_SEARCH_STRING_OPTIONS: {
      return { ...state, searchStringOptions: action.payload };
    }
    case SET_SELECTED_SEARCH_STRINGS: {
      return { ...state, selectedSearchStrings: action.payload };
    }
    case SET_CURRENT_LABEL: {
      return { ...state, currentLabel: action.payload };
    }
    default:
      return state;
  }
}
