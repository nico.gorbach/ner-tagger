import { SET_SELECTED_PROJECT } from "./types";

export default function setSelectedProjectAction(value) {
  return {
    type: SET_SELECTED_PROJECT,
    payload: value,
  };
}
