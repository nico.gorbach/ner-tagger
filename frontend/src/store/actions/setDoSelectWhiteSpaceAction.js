import { SET_DO_SELECT_WHITE_SPACE } from "./types";

export default function setDoSelectWhiteSpaceAction(value) {
  return {
    type: SET_DO_SELECT_WHITE_SPACE,
    payload: value,
  };
}
