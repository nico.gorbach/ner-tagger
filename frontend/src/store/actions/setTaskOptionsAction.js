import { SET_TASK_OPTIONS } from "./types";

export default function setTaskOptionsAction(value) {
  return {
    type: SET_TASK_OPTIONS,
    payload: value,
  };
}
