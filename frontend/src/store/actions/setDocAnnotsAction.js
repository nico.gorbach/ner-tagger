import { SET_DOC_ANNOTATIONS } from "./types";

export default function setDocAnnotsAction(value) {
  return {
    type: SET_DOC_ANNOTATIONS,
    payload: value,
  };
}
