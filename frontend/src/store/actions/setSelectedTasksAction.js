import { SET_SELECTED_TASKS } from "./types";

export default function setSelectedTasksAction(value) {
  return {
    type: SET_SELECTED_TASKS,
    payload: value,
  };
}
