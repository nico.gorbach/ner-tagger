import { LOGIN, LOGOUT, LOGIN_ERROR } from "./types";
import Axios from "../../axios/not_authenticated";

export const loginAction = ({ token, user }) => {
  return {
    type: LOGIN,
    payload: { token, user },
  };
};

export const loginErrorAction = (error) => {
  return {
    type: LOGIN_ERROR,
    payload: error,
  };
};

export const logoutAction = () => {
  return {
    type: LOGOUT,
  };
};

export const login = ({ email, password, rememberMe }) => (dispatch) => {
  return Axios.post("auth/token/", { email, password })
    .then((resp) => {
      const token = resp.data.access;
      const user = resp.data.users;
      if (token) {
        dispatch(loginAction({ token, user }));
        if (rememberMe) {
          localStorage.setItem("token", token);
          localStorage.setItem("user", JSON.stringify(user));
        }
      }
      return resp;
    })
    .catch((err) => err.response);
};
