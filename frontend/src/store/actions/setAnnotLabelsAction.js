import { SET_ANNOTATION_LABELS } from "./types";

export default function setAnnotLabelsAction(value) {
  return {
    type: SET_ANNOTATION_LABELS,
    payload: value,
  };
}
