import { SET_ANNOT_DISTRIBUTION } from "./types";
import Axios from "../../axios/authenticated";

export const setAnnotDistributionAction = (data) => {
  return {
    type: SET_ANNOT_DISTRIBUTION,
    payload: data,
  };
};

export const setAnnotDistribution = () => (dispatch) => {
  return Axios.get("users/annotation-distribution/")
    .then((resp) => {
      dispatch(setAnnotDistributionAction(resp.data));
      return resp;
    })
    .catch((err) => err.response);
};
