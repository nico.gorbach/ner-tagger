import { SET_CORPUS } from "./types";

export default function setCorpusAction(value) {
  return {
    type: SET_CORPUS,
    payload: value,
  };
}
