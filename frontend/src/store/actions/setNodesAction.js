import { SET_NODES } from "./types";

export default function setNodesAction(value) {
  return {
    type: SET_NODES,
    payload: value,
  };
}
