import { SET_PROJECT_OPTIONS } from "./types";

export default function setProjectOptionsAction(value) {
  return {
    type: SET_PROJECT_OPTIONS,
    payload: value,
  };
}
