import { SET_SELECTED_SEARCH_STRINGS } from "./types";

export default function setSelectedSearchStringsAction(value) {
  return {
    type: SET_SELECTED_SEARCH_STRINGS,
    payload: value,
  };
}
