import { SET_SELECTED_TEXT_START_ACTION } from "./types";

export default function setSelectedTextStartAction(value) {
  return {
    type: SET_SELECTED_TEXT_START_ACTION,
    payload: value,
  };
}
