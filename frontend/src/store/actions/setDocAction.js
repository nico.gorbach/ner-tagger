import { SET_DOC } from "./types";
import Axios from "../../axios/authenticated";
import setLoadingDocAction from "./setLoadingDocAction";

export default function setDocIndexAction(value) {
  return {
    type: SET_DOC,
    payload: value,
  };
}

export const setDocAction = (value) => {
  return {
    type: SET_DOC,
    payload: value,
  };
};

export const setDoc = () => (dispatch) => {
  return Axios.get("/docs/unannotated/max-exploration/")
    .then((resp) => {
      const data = resp.data.pop();
      const { content_pure_text } = data;

      const text = content_pure_text.replace(/(\r\n|\r|\n){2,}/g, "$1\n");
      // .split(/\\/)
      // .map((item) => (item === "n" ? "\n" : item))
      // .join("")
      // .replace(/[\r\n]{2,}/g, "n")
      // .replace(/nn/g, "\n");

      data["text"] = text;
      data["textArray"] = text.split("");

      const payload = data;
      dispatch(setDocAction(payload));

      return resp;
    })
    .then((resp) => {
      dispatch(setLoadingDocAction(false));
      return resp;
    })
    .catch((err) => {
      dispatch(setLoadingDocAction(false));
      return err.response;
    });
};
