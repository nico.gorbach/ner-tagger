import { SET_CURRENT_LABEL } from "./types";

export default function setCurrentLabelAction(value) {
  return {
    type: SET_CURRENT_LABEL,
    payload: value,
  };
}
