import { LOGIN, LOGOUT, LOGIN_ERROR } from "./types";
import Axios from "../../axios/authenticated";

export const fetchUserAction = ({ token, user }) => {
  return {
    type: LOGIN,
    payload: { token, user },
  };
};

export const fetchUser = () => (dispatch) => {
  return Axios.get("users/me/")
    .then((resp) => {
      const user = resp.data[0];
      if (user) {
        dispatch(fetchUserAction({ user }));
      }
      return resp;
    })
    .catch((err) => err.response);
};
