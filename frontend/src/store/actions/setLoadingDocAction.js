import { SET_LOADING_DOC } from "./types";

export default function setLoadingDocAction(value) {
  return {
    type: SET_LOADING_DOC,
    payload: value,
  };
}
