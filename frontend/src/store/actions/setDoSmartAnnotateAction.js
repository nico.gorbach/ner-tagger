import { SET_DO_SMART_ANNOTATE } from "./types";

export default function setDoSmartAnnotateAction(value) {
  return {
    type: SET_DO_SMART_ANNOTATE,
    payload: value,
  };
}
