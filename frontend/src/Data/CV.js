import React from "react";

export default function CV() {
  return (
    <div id="resume_body" className="vcard single_form-content">
      {" "}
      <div id="basic_info_row" className="last basicInfo-content">
        <div id="basic_info_cell" className="data_display">
          {" "}
          <h1 id="resume-contact" className="fn " itemProp="name">
            Litigation Associate
          </h1>{" "}
          <div id="contact_info_container">
            {" "}
            <div
              className="adr"
              itemProp="address"
              itemScope
              itemType="http://schema.org/PostalAddress"
            >
              <p
                id="headline_location"
                className="locality"
                itemProp="addressLocality"
              >
                Godfrey, IL
              </p>
            </div>{" "}
            <div className="separator-hyphen">-</div>{" "}
          </div>{" "}
          <p id="res_summary" className="summary">
            Accomplished attorney with a wide range of experience in civil
            litigation including representation of major financial institutions,
            collection agency, insurance defense, subrogation, personal injury
            on behalf of plaintiffs, asbestos and class action litigation.{" "}
            <br />
            Excellent legal writing and oral argument skills with particular
            effectiveness in motion preparation and presentation. Exceptional
            success in appellate work, having won 23 of 30 appeals.
          </p>{" "}
        </div>
      </div>{" "}
      <div className="section-item workExperience-content">
        {" "}
        <div>
          <div className="section_title">
            <h2>Work Experience</h2>
          </div>
        </div>{" "}
        <div id="work-experience-items" className="items-container">
          {" "}
          <div
            id="workExperience-EeQu5WqfmPKbwRkqK6c2tA"
            className="work-experience-section "
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Litigation Associate</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Kozeny &amp; McCubbin</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>St. Louis, MO</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">November 2009 to August 2014</p>{" "}
              <p className="work_description">
                Responsibilities <br />
                Take legal and pre-trial action necessary to defend large
                national financial institutions in tort and contract litigation
                including preparation of pleadings, investigation of clams,
                discovery, prepare and argue motions and prepare for and conduct
                trial. <br /> <br />
                Accomplishments <br />
                Successfully defended numerous institutions and obtained
                dismissal of many cases; obtained settlement of over $500,000 on
                behalf of client. <br /> <br />
                Skills Used <br />
                All legal skills required to properly analyze and prepare case
                for trial, including negotiation.
              </p>{" "}
            </div>
          </div>{" "}
          <div
            id="workExperience-EeQu5WqfmPGbwRkqK6c2tA"
            className="work-experience-section "
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Litigation Associate</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Lakin Law Firm</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>Wood River, IL</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">March 2004 to November 2008</p>{" "}
              <p className="work_description">
                Class action and personal injury litigation; member of
                litigation team which obtained $43.2 million verdict against
                Ford (Jablonski v. Ford) <br /> <br />
                Successfully prepared and settled numerous class action cases{" "}
                <br /> <br />
                Won appeal of trial court order allowing class wide arbitration
              </p>{" "}
            </div>
          </div>{" "}
          <div
            id="workExperience-EeQu5WqfmPCbwRkqK6c2tA"
            className="work-experience-section "
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Litigation Attorney</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Talbert &amp; Associates</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>Alton, IL</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">November 2001 to February 2004</p>{" "}
              <p className="work_description">
                Responsibilities <br />
                Preparation, filing and resolution of personal injury claims on
                behalf of plaintiffs. <br /> <br />
                Accomplishments <br />
                Successfully obtained policy limits settlements in numerous
                cases. <br /> <br />
                Skills Used <br />
                Analysis of potential claims; interview clients and witnesses;
                draft and file necessary pleadings, including complaints;
                prepare discovery, depose witnesses, including doctors; prepare
                and present pre-trial motions; negotiation of settlements,
                prepare and conduct trial, including jury instructions; continue
                to represent client though all phases of appellate process.
              </p>{" "}
            </div>
          </div>{" "}
          <div
            id="workExperience-EeQu5WqfmO-bwRkqK6c2tA"
            className="work-experience-section "
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Litigation Associate</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Roth &amp; Associates</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>Granite City, IL</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">April 1999 to June 2000</p>{" "}
              <p className="work_description">
                Provided defense of personal injury cases through representation
                of insured defendants. <br /> <br />
                Won appeal wherein municipality was denied insurance coverage
                for damages resulting from faulty fire hydrant when appellate
                court agreed hydrant was not an insured device.
              </p>{" "}
            </div>
          </div>{" "}
          <div
            id="workExperience-EeQu5WqfmO6bwRkqK6c2tA"
            className="work-experience-section "
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Attorney</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Peel, Beatty &amp; Motil</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>Glen Carbon, IL</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">April 1991 to April 1999</p>{" "}
              <p className="work_description">
                Personal injury on behalf of plaintiffs and general civil
                litigation. <br />
                Prepared and won numerous appeals. Won appeal of Missouri
                Workers' Compensation case <br /> <br />
                Obtained policy limits settlement in legal malpractice as a
                result of taking defendant's expert witness' deposition.
              </p>{" "}
            </div>
          </div>{" "}
          <div
            id="workExperience-EeQu5WqfmO2bwRkqK6c2tA"
            className="work-experience-section last"
          >
            <div className="data_display">
              {" "}
              <p className="work_title title">Attorney</p>{" "}
              <div className="work_company">
                {" "}
                <span className="bold">Smith, Larson &amp; Pitts</span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div className="inline-block">
                  <span>East Alton, IL</span>
                </div>{" "}
              </div>{" "}
              <div className="separator-hyphen">-</div>{" "}
              <p className="work_dates">March 1987 to November 1989</p>{" "}
              <p className="work_description">
                Responsibilities <br /> <br />
                Represented injured plaintiffs; helped design, create and
                supervise toxic tort litigation (asbestos) department. <br />{" "}
                <br />
                Accomplishments <br /> <br />
                Able to achieve successful settlements on behalf of injured
                clients, including asbestos cases wherein numerous plaintiffs
                were named in the same suit. <br /> <br />
                Skills Used <br /> <br />
                All skills necessary for successful preparation and resolution
                of personal injury cases on behalf of clients, and particularly
                issue analysis, negotiation and argument of complex motions.
              </p>{" "}
            </div>
          </div>{" "}
        </div>{" "}
      </div>{" "}
      <div className="section-item education-content">
        {" "}
        <div>
          <div className="section_title">
            <h2>Education</h2>
          </div>
        </div>{" "}
        <div id="education-items" className="items-container">
          {" "}
          <div
            id="education-EeQu5WqfmPabwRkqK6c2tA"
            className="education-section "
          >
            <div
              className="data_display"
              itemProp="alumniOf"
              itemScope
              itemType="http://schema.org/EducationalOrganization"
            >
              {" "}
              <p className="edu_title">Juris Doctorate in Law School</p>{" "}
              <div className="edu_school">
                {" "}
                <span className="bold" itemProp="name">
                  University of Illinois College of Law
                </span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div
                  className="inline-block"
                  itemProp="address"
                  itemScope
                  itemType="http://schema.org/PostalAddress"
                >
                  <span itemProp="addressLocality">Chan\mpaign-Utrbana</span>
                </div>{" "}
              </div>{" "}
              <p className="edu_dates">1970 to 1974</p>{" "}
            </div>
          </div>{" "}
          <div
            id="education-EeQu5WqfmPSbwRkqK6c2tA"
            className="education-section last"
          >
            <div
              className="data_display"
              itemProp="alumniOf"
              itemScope
              itemType="http://schema.org/EducationalOrganization"
            >
              {" "}
              <p className="edu_title">Bachelor of Arts</p>{" "}
              <div className="edu_school">
                {" "}
                <span className="bold" itemProp="name">
                  University of Illinois at Champaign
                </span>{" "}
                <div className="separator-hyphen">-</div>{" "}
                <div
                  className="inline-block"
                  itemProp="address"
                  itemScope
                  itemType="http://schema.org/PostalAddress"
                >
                  <span itemProp="addressLocality">Champaign, IL</span>
                </div>{" "}
              </div>{" "}
              <p className="edu_dates">1969</p>{" "}
            </div>
          </div>{" "}
        </div>{" "}
      </div>{" "}
      <div className="section-item skills-content">
        {" "}
        <div>
          <div className="section_title">
            <h2>Skills</h2>
          </div>
        </div>{" "}
        <div id="skills-items" className="items-container">
          <div className="data_display">
            <div className="skill-container resume-element">
              <span className="skill-text">
                Excellent ability in preparing and presenting motions and
                appeals. Have won 23 of 30 appeals. Have been involved in
                litigation my entire career.
              </span>
            </div>
          </div>
        </div>{" "}
      </div>{" "}
      <div className="section-item military-content">
        {" "}
        <div>
          <div className="section_title">
            {" "}
            <div id="add_military" /> <h2>Military Service</h2>{" "}
          </div>
        </div>{" "}
        <div id="military-items" className="items-container">
          <div
            id="military-EeRo-EeZFE2XVuIbfZYckw"
            className="military-section last"
          >
            <div className="data_display">
              {" "}
              <p className="military_country">
                <span className="bold">Service Country:</span> United States
              </p>{" "}
              <p className="military_branch">
                <span className="bold">Branch:</span> Army
              </p>{" "}
              <p className="military_rank">
                <span className="bold">Rank:</span> Specialist Fifth Class
              </p>{" "}
              <p className="military_date">June 1970 to February 1972</p>{" "}
              <p className="military_description">
                Served overseas in South Korea; supervised record retention
                unit; obtained Top Secret Security Clearance; awarded rank of
                Specialist Fifth Class after 17 months in service which required
                "double waivers" of time-in-grade and time-in-service. <br />{" "}
                <br />
                Awarded Honorable Discharge, Good Conduct Medal and Armed Forces
                Expeditionary Medal
              </p>{" "}
            </div>
          </div>
        </div>{" "}
      </div>{" "}
      <div className="section-item additionalInfo-content">
        {" "}
        <div>
          <div className="section_title">
            <h2>Additional Information</h2>
          </div>
        </div>{" "}
        <div id="additionalinfo-items" className="items-container">
          <div id="additionalinfo-section" className="last">
            <div className="data_display">
              <p>
                In 2002, appointed to Board of Directors of the Illinois Lawyers
                Assistance Program by the Illinois Supreme Court Justice for
                Fifth District of Illinois and approved by all the other
                Justices; past president and continuing board member; trained
                intervener.
              </p>
            </div>
          </div>
        </div>{" "}
      </div>{" "}
    </div>
  );
}
