const CVdata = `<?xml version="1.0" encoding="utf-8"?>
<root>
<page>
<pageurl>http://www.indeed.com/r/68958e87283fb127?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>be8c4701ba1a7c62d11e297e6a673419</uniq_id>
      <url>http://www.indeed.com/r/68958e87283fb127?hl=en&amp;co=US</url>
      <zipcode>35616</zipcode>
      <name>Ccr III, Social Media</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Ccr III, Social Media</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Tuscumbia, AL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeLPkB3BPfmx17PItWPLJQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Ccr III, Social Media</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Walgreens</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Tuscumbia, AL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2005 to Present</p>
<p class="work_description">Corportate Customer Service/Social Media Representative for Walgreens company. Have knowledge with: Word,Excel, Power Point, Outlook, Visible, Kronos, EPower, and Polaris. Due to the nature of my job I am able to handle stressful calls professionally and quickly. Capable of making decisions in order to resolve conflict.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeLPkExxc4Cx17PItWPLJQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associates in Medical office administration</p>
<div class="edu_school">
<span class="bold" itemprop="name">Nwscc</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Muscle Shoals, AL</span></div>
</div>
<p class="edu_dates">2001 to 2003</p>
</div></div>
<div id="education-EeLy9WoUl-KWgL9oLXgqwg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">CHHS</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Tuscumbia, AL</span></div>
</div>
<p class="edu_dates">1995</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Typist, office management, experience with fast paced production</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F68958e87283fb127/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/4427b5431f7f2b08?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7ed5d99feb09a221e5b6d9f25164e539</uniq_id>
      <url>http://www.indeed.com/r/4427b5431f7f2b08?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Door Host</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Door Host</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeahVD_lrNaNTgxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Door Host</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Chicago Athletic Association</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2015 to Present</p>
</div></div>
<div id="workExperience-EeahVGn1MJSnRc7c3iCmnw" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Doorman/ Valet</p>
<div class="work_company">
<span class="bold">THE JAMES HOTEL</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2014 to July 2015</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeahVBtcpZCNTgxatb0mLA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor's in Business Administration</p>
<div class="edu_school">
<span class="bold" itemprop="name">Dominican University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">River Forest, IL</span></div>
</div>
<p class="edu_dates">2011 to 2016</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F4427b5431f7f2b08/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/efbbf3b215d3a760?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>d34624ef812375d3cfdb89930c941f62</uniq_id>
      <url>http://www.indeed.com/r/efbbf3b215d3a760?hl=en&amp;co=US</url>
      <zipcode>60657</zipcode>
      <name>Senior Policy Service Rep</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Senior Policy Service Rep</h1>
<h2 id="headline" itemprop="jobTitle">Senior Policy Service Rep</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Calumet Park, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeZZDM7IuzuTZCLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Senior Policy Service Rep</p>
<div class="work_company"><span class="bold">CNA Insurance Company</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2014 to July 2016</p>
<p class="work_description">• Issue new and renewal insurance policies for one or more lines of business <br>• Handles multiple policy transactions <br>• Processed premium bearing transactions <br>• Review incoming business transactions and related information for completeness, accuracy and quality. <br>• Work with underwriters, agents, and broker of moderate complexity for multiple lines of business <br>• Distribute quote letters and insurance policies to agents, brokers, underwriters and insureds. <br>• Provided excellent customer service to all internal and external customers</p>
</div></div>
<div id="workExperience-EeZZDM7I4kyTZCLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer Service Relationship Specialist</p>
<div class="work_company">
<span class="bold">Allstate Insurance Company</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Woodridge, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2010 to August 2013</p>
<p class="work_description">• Consistently maintain 94% or above on first call resolution, 96% or above quality scores and 92% or above for customer satisfaction scores with Allstate. <br>• Resolve questions/issues while listening with compassion and displaying empathy for the customer's situation. <br>• Completes customer's transactional request as provided by the customer. <br>• Demonstrates accuracy in processing changes to customer's policies based on the information provided <br>• Identify other insurance needs that could benefit the customer while retaining the client. <br>• Verify coverage for customers or verify that policy changes were made and provide documentation for the customer. <br>• Remain current on new marketing campaigns in order to respond appropriately to marketing related inquires using all available resource tools. <br>• Acknowledge customers request for additional insurance product by quoting and writing policy. <br>• Document conversation with customer and transaction processed (including all documents sent out by our company) and be able to clearly explain details of their nature.</p>
</div></div>
<div id="workExperience-EeZZDM7I4k2TZCLhBGsw1Q" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Life/Annuity Claim Adjuster</p>
<div class="work_company">
<span class="bold">Bankers Life &amp; Casualty Insurance Company</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 1999 to November 2007</p>
<p class="work_description">• Maintained a 99% accuracy rate with a 100% rate completing the processing with in a seven day turnaround period according to department procedures. <br>• Processed death claims for Life and Annuity products in an accurate and timely manner, according to compliance and state regulations. <br>• Initiate claim set up, verify policy status, and evaluate policy for eligibility of coverage. <br>• Provide customer service to all internal and external customers. <br>• Evaluate policy for eligibility of coverage, approve, deny or rescind. <br>• Interpret policy provisions. Calculate death benefits, commuted value, claim interest and federal tax withholding, <br>• Processed claims electronically, producing a check according to company's policy and procedures. <br>• Interpret medical records for contestable life claims. <br>• Engaged vendors to conduct claim investigations for homicide and suicide cases. <br>• Negotiate claim settlements, assimilate data, evaluate facts and negotiate solutions to complex claims. <br>• Prepared federal and state regulatory forms when needed. Update claims register on a daily basis. <br>• Consulted with in-house medical and legal professional for their expert opinion as necessary. <br>• Provided written and verbal correspondence with all levels of customers, internal and external. <br>• Create a monthly report for upper management of all paid, denied, and rescinded claims for the month</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeZZDM7I4k-TZCLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor's in Health Management</p>
<div class="edu_school"><span class="bold" itemprop="name">University of Phoenix</span></div>
<p class="edu_dates">March 2012 to July 2014</p>
</div></div>
<div id="education-EeZZDM7I4lGTZCLhBGsw1Q" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associates of Arts in Computer Skills</p>
<div class="edu_school"><span class="bold" itemprop="name">University of Phoenix</span></div>
<p class="edu_dates">January 2010 to March 2012</p>
</div></div>
</div>
</div>
<div class="section-item certification-content">
<div><div class="section_title"><h2>Certifications</h2></div></div>
<div id="certification-items" class="items-container"><div id="certification-Eea10lWtAgaJ3FlI6yVEOA" class="certification-section last"><div class="data_display">
<p class="certification_title">ALHC DESIGNATION</p>
<p class="certification_date">June 1999 to Present</p>
<p class="certification_description">Associate Life and Health Claims</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Energetic and enthusiastic insurance professional with many years of customer service and insurance experience. Motivated to succeed in a fast-paced and deadline driven professional environment. Comprehensive knowledge of claims adjustment with special knowledge of life insurance death claims. <br> <br>• Organizational skills        ●    Self starter</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fefbbf3b215d3a760/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/07f383d472ea82c1?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>47a6b89007349207dd8a1d7af157f9ef</uniq_id>
      <url>http://www.indeed.com/r/07f383d472ea82c1?hl=en&amp;co=US</url>
      <zipcode>82002</zipcode>
      <name>Breakfast Attendant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Breakfast Attendant</h1>
<h2 id="headline" itemprop="jobTitle">Versatile Employee</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Cheyenne, WY</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">I can be reached at this phone number at any time. I can provide great availability for work on any shift hours and I am readily flexible which has helped me become a verssstile and dependable employee.</p>
<p id="relocation_status">Willing to relocate to: Fort Collins, CO - Greeley, CO - Northglenn, CO</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebAjhAfsLe8EAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Breakfast Attendant</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Quality Inn and Suites</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Cheyenne, WY</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2016 to Present</p>
<p class="work_description">I am responsible for maintaining availability of breakfast items included in the complimentary breakfast offered to guests every morning by the Choice Quality Inn located in Cheyenne, WY. I provide this complimentary breakfast customer service from Friday, Saturday, and Sunday, 6:00 AM to approximately 1:00 PM.</p>
</div></div>
<div id="workExperience-EeYUkYyssfqBiQxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Independent Contractor/Revealed Auditor</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">GfK Mystery Shopping Company</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">New York, NY</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2015 to Present</p>
<p class="work_description">Responsibilities: <br>I have been responsible for completing accurate revealed audits and mystery shops for GFK's clientele companies such as Best Buy, AT&amp;T,  Sprint,  Verizon, and T-Mobile.  The revealed audits included attention to detail and positive interaction with store management and their staff presenting them with letters of authorization, giving them reasons why I was in the store that day, and the leave behind forms that communicated to Management the preliminary results of the revealed audits for both Best Buy and AT@T clientele.  The revealed audits and smartphone/home appliance mystery shops measured customer service and product knowledge to customers interested in purchasing an upgrade phone model of either Apple or Android operating systems.  These smartphone and home appliance mystery shops included stores like Lowes, Home Depot, Best Buy, AT@T, Sprint, Verizon, and T-Mobile.  All the revealed audits and smartphone/home appliance mystery shop online reports were uploaded to GFK's? website for clientele's review.</p>
</div></div>
<div id="workExperience-EeYUkZKL1e2V6VlI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Auditor/Mystery Shopping</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Ipsos</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Parsippany, NJ</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2012 to Present</p>
<p class="work_description">Responsibilities: <br>I was responsible for the inspection and evaluation of wireless brand model phone smartphone products' functionality and store availability for Verizon, AT&amp;T, Sprint, and T-T-Mobile wireless carriers.  These inspections and evaluations included both Apple and Android operating systems brand model smartphones.  These inspections and evaluations also included home appliance and electronics store products like Lowes, Home Depot,  Con's, and Appliance Furniture Watehouse, and Best Buy stores. <br> <br>Accomplishments: <br>I accomplished completing the online evaluation reports fir these audits and mystery shops in a timely and accurate manner.  I accomplished submitting the accurate evaluation reports to the Ipsos website for client's review. <br> <br>Skills Used: <br>The skills I acquired were attention to detail and capturing quality pictures of store buildings' exteriors and the stores' wireless smartphone products and accessories.</p>
</div></div>
<div id="workExperience-EeXNTvVqD-60XyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Inventory Specialist</p>
<div class="work_company">
<span class="bold">RGIS Inventory, Inc.</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Golden, CO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2015 to June 2015</p>
<p class="work_description">Responsibilities: <br>On the job with this inventory company consisted of the speedy and accurate of inventory clientele companies like Ultra Beauty, Sephora, Fred Myers Jewelers, Walmart and Target Super Stores.  The position required attention to detail and accuracy was a top priority with Management, co-workers, and clientele management and staff.  I was also responsible for keeping store isles clear of excess trash and debris, product boxes/totes that held merchandise moved to straightedges for co-workers, management, and customers' safety.</p>
</div></div>
<div id="workExperience-EeWVrY1IsIi-bgxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Warehouse Associate/Watertester</p>
<div class="work_company">
<span class="bold">Onin Staffing - Arrow Fabricated Tubing - Light Industrial Plant</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Garland, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2013 to May 2013</p>
<p class="work_description">Responsibilities: <br>This position was a temporary staffing assignment through Onin Staffing for this light industrial plant in Garland, TX.  The position entailed water testing of aluminum/copper tubing ensuring the functionality of said tubing after testing for malfunctions/leakages, packing and inventory counts of approved tubing for daily shipments to clientele.  I was also responsible for the correct price labeling for the approved tubing for the daily shipments.</p>
</div></div>
<div id="workExperience-EeWVre2n3Ma-bgxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Order Filler/Warehouse Associate</p>
<div class="work_company">
<span class="bold">Neiman Marcus Fulfillment Center</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Dallas, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2012 to December 2012</p>
<p class="work_description">Responsibilities: <br>I was hired on as a seasonal temporary warehouse associate for the Christmas season as a merchandise pic and pac Order Filler.  The position consisted of picking out and locating the merchandise from the various shelves and boxed storage locations for customer order tickets.  The position required attention to detail and safe operation of conveyor belts that transported merchandise totes to either packaging or shipping and receiving daily.</p>
</div></div>
<div id="workExperience-EeWVrtcCelaFt1lI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">General Construction Crew Member</p>
<div class="work_company">
<span class="bold">True Blue - Labor Ready Plano/Carrolton, Metro-Plex Area Cities, TX Branch Locations</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Dallas, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2012 to October 2012</p>
<p class="work_description">Responsibilities: <br>I have been responsible to fulfill temporary general construction hard hat work at the Blockbuster warehouse distribution center in McKinney, TX, a renovation of a JC Penny's in Plano, TX, an elementary school renovation/remodeling in Plano, TX, a reset project for the multi million dollar TX warehouse franchise Gardenridge and to enhance the customer shopping experience, I was put in charge of clearing product isles from excess trash and debris for easy access for customers, co-workers, and supervisors, and finally a Texas Lone Star Auto Auction that consisted of parking and driving vehicles up for auction on the auction sales floor for customer inspection and purchase. <br> <br>Accomplishments: <br>The impact I had with all of these opportunities was keeping customer service and the goal for each of these temporary opportunities top priority. <br> <br>Skills Used: <br>The skills I acquired were keeping customer requests and ticket orders a top priority since these temporary assignments were to increase the customer buying sales and revenue and prepare the store for customer shopping experience.</p>
</div></div>
<div id="workExperience-EeWVrxeApAimZ1n7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Substitute Teacher</p>
<div class="work_company">
<span class="bold">Laramie County School District #1</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Cheyenne, WY</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2010 to February 2012</p>
<p class="work_description">Responsibilities: <br>Responsible for classroom instruction and behavioral management of students grades K-12 and all district schools.  Provided professional assistance to regularly staffed teachers in their absence and followed various classroom teacher lesson plan instruction and behavioral modification plans for certain students.  While there in the classroom, instilled fun therapeutic skills during classroom activity movement from one activity to the next utilizing a count ddown system, 6, 3, 2, 1 minute intervals to wrap up the activity and move to the next activity increasing the development of the natural memory retention and timing when moving from one classroom activity to the another. <br> <br>Accomplishments: <br>The accomplishments with this opportunity were being able to test the effectiveness of therapuetuc classroom activity movement imanagement and student behavioral modification plans for a positive classroom experhence for students in any grade level or district school.</p>
</div></div>
<div id="workExperience-EeWVr6QVGviFt1lI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Survey Phone Interviewer</p>
<div class="work_company">
<span class="bold">Aspen Media Market</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Boulder, CO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2006 to October 2007</p>
<p class="work_description">Responsibilities: <br>Obtained research information from various businesses conducting phone interviews of mini and major surveys.  Position required attention to detail when acquiring research information to process through to the company's main database systems. <br> <br>Accomplishments: <br>The accomplishments from this position included fulfillment's of quota surveys for company research goals. <br> <br>Skills Used: <br>Skills acquired were attention to detail as well as accurate data entry.</p>
</div></div>
<div id="workExperience-EeWVsAljZoumZ1n7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Living Skills/Job Coach</p>
<div class="work_company">
<span class="bold">Magic City Enterprises</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Cheyenne, WY</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 1999 to June 2000</p>
<p class="work_description">Responsibilities: <br>Responsible for the direct care and supervision of developmentally disabled clients. and impimplementing their therapeutic learning and behavioral modification exercises from their IEP Case Plans.  These therapeutic techniques were instilled with the client's daily routine schedule in their residential and rehabilitation environments. <br> <br>Accomplishments: <br>The accomplishments for this opportunity were evaluating the effectiveness of the therapeutic learning and behavioral modification plans from each individual client's IEP Case Plan daily. <br> <br>Skills Used: <br>The skills acquired from this opportunity were familiarity with charting evaluations made for each individual client under my direct care and supervision as well as providing appropriate  community interaction incentives and rewards.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeWVqK_v8MK-bgxatb0mLA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associates of Arts in Criminal Justice</p>
<div class="edu_school"><span class="bold" itemprop="name">Laramie County Community College</span></div>
</div></div>
<div id="education-EeWVqNRisKO-bgxatb0mLA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts in History</p>
<div class="edu_school"><span class="bold" itemprop="name">Colorado State University</span></div>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Typing 50 Wpm (2 years)</span>, <span class="skill-text">Data Entry (2 years)</span>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F07f383d472ea82c1/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/e950bb3c5a00b8eb?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7332e05afd9fb4a2c943519c7eae2b5f</uniq_id>
      <url>http://www.indeed.com/r/e950bb3c5a00b8eb?hl=en&amp;co=US</url>
      <zipcode>60611</zipcode>
      <name>Drafter</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Drafter</h1>
<h2 id="headline" itemprop="jobTitle">Drafter</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Willowbrook, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To obtain a position in which my organizational, design, CAD and computer skills can be beneficial.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeIflXMwxDGMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Drafter</p>
<div class="work_company"><span class="bold">Better-Bilt Products</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2012 to September 2012</p>
<p class="work_description">contract/Beco Group) <br>- Drafting of new detail and assembly drawings in AutoCAD 2004.</p>
</div></div>
<div id="workExperience-EeIflXMwxDKMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">OSP Coordinator</p>
<div class="work_company">
<span class="bold">Comcast</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elmhurst, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2011 to July 2012</p>
<p class="work_description">contract/Espo Corp) <br>-Documentation of Fiber Optic commercial installations system maps and schematics using AutoCad Map 3D 2010 software.  Create AutoCad drawings buildings showing fiber path.  Keep fiber chart records up to date in Microsoft Excel.  Quality Control maps of prepared others.  Maintain priority of projects in corporate tracker.</p>
</div></div>
<div id="workExperience-EeIflXMwxDOMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Design Drafter/Cad Operator</p>
<div class="work_company">
<span class="bold">Komori America Corp</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Rolling Meadows, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2007 to May 2009</p>
<p class="work_description">contract/Espo Corp) <br>- Drafting of customer's plant layout drawings in AutoCAD 2008.  Coordinate with sales and service about equipment installation and start-up.   Design of new and retrofit assemblies, sub-assemblies and details.  Documentation of bill of materials using Microsoft Word and Excel.  Create and maintain customer files.  Obtain vendor quotes, place orders and expedite. Correspond with customers and field service to assure proper equipment installation.</p>
</div></div>
<div id="workExperience-EeIflXMwxDSMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Assembly and Detail Drafter</p>
<div class="work_company">
<span class="bold">T &amp; H Lemont</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Countryside, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2007 to June 2007</p>
<p class="work_description">contract/Beco Group) <br>- Drafting of new detail and assembly drawings in AutoCAD 2006.  Design new sub-assemblies and generate bill of material.  Generate and revise bills of materials in Microsoft Word.  Communicate between engineers and fabricators to eliminate misunderstandings.</p>
</div></div>
<div id="workExperience-EeIflXMwxDWMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Parts Catalog, Detail Drafter</p>
<div class="work_company">
<span class="bold">Hunter Automated</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Schaumburg, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2004 to February 2007</p>
<p class="work_description">Construct Parts Manuals for new equipment.  Communicate with Parts Department concerning customer's orders and upgrades.  Update Parts Manuals per equipment upgrades of existing machines.  Organize and sustain catalog priority list using Microsoft Excel.  Maintain bill of material and archive files using KEA420/Sonic data base system.  Create and revise exploded parts illustrations using Corel Draw.  Coordinate with engineering, documentation of new designs and upgrades.  Produce new drawings in AutoCAD Mechanical Desktop 2006.</p>
</div></div>
<div id="workExperience-EeIflXMwxDaMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Engineering Assistant/Detail Drafter</p>
<div class="work_company">
<span class="bold">Amerline Enterprises Co</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Cicero, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 2003 to June 2004</p>
<p class="work_description">New drawings and revisions in AutoCAD LT. Generate Engineering Change Orders.  Maintain all drawing files - Cad and hard copy.  Enter orders in Vantage. Set up standards, methods and operations for jobs.  Data entry into Microsoft Excel, of production time and job closing.</p>
</div></div>
<div id="workExperience-EeIflXMwxDeMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Machine Designer/Parts Sales</p>
<div class="work_company">
<span class="bold">Evtec Corporation</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Woodridge, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 1988 to December 2002</p>
<p class="work_description">Design, detail and prepare bill of materials for custom high-speed rotary machines used in the printing industry.  Re-design of machines to achieve product improvement and/or cost reduction.  Work closely with sales and engineers to develop new products.  Create master engineering release forms. Release bill of materials and drawings per customer specifications.  Establish numbering systems for drawings and purchased parts.  Parts sales and order entry.  Search out vendors, obtain quotes, order parts, expedite orders and ship parts to customers.   Train detail drafters on FastCAD software.  Assign work to assembly drafters and detail drafters and check.  Troubleshoot on assembly floor and in machine shop.  Maintain engineering job priority list.</p>
</div></div>
<div id="workExperience-EeIflXMw60iMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Engineering Coordinator/Machine Designer</p>
<div class="work_company">
<span class="bold">Datm Incorporated</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Lisle, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 1982 to September 1988</p>
<p class="work_description">Design, detail and prepare bill of materials of sub-assemblies.   Assign work to assembly drafters and detail drafters, and check. Maintain drawing files.  Prepare and release bill of materials for new orders per customer specifications. Assist in purchasing of machined parts.</p>
</div></div>
<div id="workExperience-EeIflXMw60mMofHkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Mechanical Detail Drafter</p>
<div class="work_company">
<span class="bold">C. E. Raymond</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 1981 to September 1981</p>
<p class="work_description">Prepare detail mechanical drawings per engineers' specifications.</p>
</div></div>
<div id="workExperience-EeIflXMw60qMofHkoT2skA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Mechanical Detail Drafter</p>
<div class="work_company">
<span class="bold">Danly Machine</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Cicero, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 1977 to April 1981</p>
<p class="work_description">Draft detail and assembly drawings per engineer's specifications.  Produce and follow through Engineering Change Notices by correcting and upgrading existing drawings and bill of materials.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeIflXMw60yMofHkoT2skA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">AA</p>
<div class="edu_school">
<span class="bold" itemprop="name">Morton Jr. College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Cicero, IL</span></div>
</div>
<p class="edu_dates">September 1975 to June 1977</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fe950bb3c5a00b8eb/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/fdde6e622f8cf83e?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>912d69587692d2853edd8861b408e71a</uniq_id>
      <url>http://www.indeed.com/r/fdde6e622f8cf83e?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Dishwasher/Food Prep</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Dishwasher/Food Prep</h1>
<h2 id="headline" itemprop="jobTitle">Looking for work as a dishwasher or similiar</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">I have several years working in the hospitality and food service industry as a dishwasher and have also served as a butcher for two years giving me very good kitchen skills. I am computer literate, always on-time and I even showed up for work during the great snow blizzard of 2011 and can lift up to 70 pounds. I also hold a ServSafe Food Handling Certificate-obtained January 2015.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-Eea8qqZBLJi5_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Dishwasher/Food Prep</p>
<div class="work_company">
<span class="bold">Hospitality One</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Glendale Heights, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2015 to December 2016</p>
<p class="work_description">I am well versed in the workings of a  <br>    THREE COMPARTMENT SINK <br>    SERV SAFE CERTIFICATION <br>    KOSHER Kitchen  <br>    HOBART Dish washing machines</p>
</div></div>
<div id="workExperience-Eea8q5_tV865_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Houseman / Porter</p>
<div class="work_company">
<span class="bold">Aramark</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Rosemont, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2011 to December 2014</p>
<p class="work_description">Main duties: Providing excellent customer service  <br>Setting up rooms for guests  <br>Washing Dishes  <br>Serving food and drinks <br>Clearing and cleaning tables</p>
</div></div>
<div id="workExperience-Eea8rHrGRoO5_llI6yVEOA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Meat Wrapper / Butcher</p>
<div class="work_company">
<span class="bold">Treasure Island Foods</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2009 to July 2011</p>
<p class="work_description">Main duties: Providing excellent customer service. <br>Wrapping various cuts of meat and fish. <br>Closing up of department including cleaning all tools, saws, tables, and counters Intake and lifting of incoming meat deliveries.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeIzO6mlXX-qLZvrOz8UFQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">B.F.A. in art</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Kansas</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Lawrence, KS</span></div>
</div>
<p class="edu_dates">August 1986 to December 1991</p>
</div></div></div>
</div>
<div class="section-item certification-content">
<div><div class="section_title"><h2>Certifications</h2></div></div>
<div id="certification-items" class="items-container">
<div id="certification-Eea8qW3b4OS5_llI6yVEOA" class="certification-section "><div class="data_display">
<p class="certification_title">Basset</p>
<p class="certification_date">July 2015 to Present</p>
</div></div>
<div id="certification-Eea8qbWTtX-eeyLhBGsw1Q" class="certification-section last"><div class="data_display">
<p class="certification_title">ServSafe Food Handler</p>
<p class="certification_date">January 2015 to January 2018</p>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Ffdde6e622f8cf83e/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/1dfcc5176995bac6?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>24c8e30b43181a642ce20f18b0c08cfc</uniq_id>
      <url>http://www.indeed.com/r/1dfcc5176995bac6?hl=en&amp;co=US</url>
      <zipcode>60647</zipcode>
      <name>Wrapping Machine Operator</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Wrapping Machine Operator</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Dolton, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To secure a position where I can utilize my knowledge and skills within a <br>progressive company that encourages professional growth and development.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeSbg7N2jwKLjyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Wrapping Machine Operator</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Nestle Confections and Snacks</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Franklin Park, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2013 to Present</p>
<p class="work_description">Responsibilities <br>Prep and load wrapping paper onto wrapping machines. <br>Check and verify that wrapping paper correctly aligns and seals. <br>Check and verify every hour that the machine displays the correct code date and that the code date correctly displays on the wrapper. <br>Take machines apart, clean and sanitize all parts, and put machines back together in a timely manner. <br>Change-over machines between fun-sized, mini, and 6-pack bars.</p>
</div></div>
<div id="workExperience-EeSbgE1P7LigvFlI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Office Services Representative</p>
<div class="work_company">
<span class="bold">Canon Business Services</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>University Park, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2012 to October 2013</p>
<p class="work_description">Review, prioritizes and submit print request from mainframe, mini-server, desktop according to complexity and urgency. <br>* Deliver print jobs within established time frame. Maintain logs of all work submitted and completed. <br>* Responsible for minor maintenance of reproduction equipment by solving paper jams, placing service calls and conducting routine <br>cleaning. <br>* Hardware: Xerox Nuvera 125 Freeflow System, Xerox 700 Digital Color Press, and Xerox 4112.</p>
</div></div>
<div id="workExperience-EeSbgE1P7LmgvFlI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Personal Trainer</p>
<div class="work_company">
<span class="bold">Charter Fitness</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Olympia Fields, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2012 to May 2013</p>
<p class="work_description">Conduct fitness consultations and assessments. <br>* Develop individualized programs that ensure client safety and enhancement of their personal fitness goals. <br>* Assist, monitor and instruct clients by ensuring safe and effective <br>use of gym equipment and weight machines.</p>
</div></div>
<div id="workExperience-EeSbgE1P7LqgvFlI6yVEOA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">3rd Shift Senior Print Services Operator</p>
<div class="work_company">
<span class="bold">CNA Insurance</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 1997 to September 2009</p>
<p class="work_description">Review, print and post-process insurance policies and claim checks from mainframe for distribution to claim adjusters and policy holders. <br>* Equipment: Windows95, word6.0, Excel, PowerPoint, DOS, MVS, JES2, Flex Server, 3090 Mainframe, SunBlade System 1000, Xerox(4635) Laser Printers, Hunkeler Roll Feed System, Kodak Optistar System, Siemens(2240) Printers, and Series 3 and MS53 Gunther equipment.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeSbhBV9UMqLjyLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certification in Fitness Training in Fitness Training</p>
<div class="edu_school">
<span class="bold" itemprop="name">International Sports Sciences Association</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2009 to 2011</p>
</div></div>
<div id="education-EeSbgE1P7LygvFlI6yVEOA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Radio/TV Broadcasting</p>
<div class="edu_school">
<span class="bold" itemprop="name">CSB School of Broadcasting Accelerated</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2008 to 2009</p>
</div></div>
<div id="education-EeSbgE1P7L6gvFlI6yVEOA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Diploma</p>
<div class="edu_school">
<span class="bold" itemprop="name">Thornton Township High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Harvey, IL</span></div>
</div>
<p class="edu_dates">August 1990 to June 1994</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Fitness Training, Radio/TV Broadcasting</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F1dfcc5176995bac6/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/3b5dec7665215bb7?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>4285df8680355386f7b41b0038fd02ac</uniq_id>
      <url>http://www.indeed.com/r/3b5dec7665215bb7?hl=en&amp;co=US</url>
      <zipcode>60608</zipcode>
      <name>Job Seeker</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn anonymous" itemprop="name">Job Seeker</h1>
<h2 id="headline" itemprop="jobTitle">student</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Palatine, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">very very hardworker. will take on any challenge thats possible for me.</p>
</div></div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeSoQlONib2LoyLhBGsw1Q" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">palatine high school</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Palatine, IL</span></div>
</div>
<p class="edu_dates">2014 to 2015</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>very hardworker, will ask to do over time, listens and takes critism well .</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F3b5dec7665215bb7/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/f8a9dad6d8b7f6e9?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>c1d272dddd707f2a7258254863d23ae7</uniq_id>
      <url>http://www.indeed.com/r/f8a9dad6d8b7f6e9?hl=en&amp;co=US</url>
      <zipcode>62630</zipcode>
      <name>Litigation Associate</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Litigation Associate</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Godfrey, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Accomplished attorney with a wide range of experience in civil litigation including representation of major financial institutions, collection agency, insurance defense, subrogation, personal injury on behalf of plaintiffs, asbestos and class action litigation. <br>Excellent legal writing and oral argument skills with particular effectiveness in motion preparation and presentation.  Exceptional success in appellate work, having won 23 of 30 appeals.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeQu5WqfmPKbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Litigation Associate</p>
<div class="work_company">
<span class="bold">Kozeny &amp; McCubbin</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>St. Louis, MO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2009 to August 2014</p>
<p class="work_description">Responsibilities <br>Take legal and pre-trial action necessary to defend large national financial institutions in tort and contract litigation including preparation of pleadings, investigation of clams, discovery, prepare and argue motions  and prepare for and conduct trial.  <br> <br>Accomplishments <br>Successfully defended numerous institutions and obtained dismissal of many cases; obtained settlement of over $500,000 on behalf of client.  <br> <br>Skills Used <br>All legal skills required to properly analyze and prepare case for trial, including negotiation.</p>
</div></div>
<div id="workExperience-EeQu5WqfmPGbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Litigation Associate</p>
<div class="work_company">
<span class="bold">Lakin Law Firm</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Wood River, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2004 to November 2008</p>
<p class="work_description">Class action and personal injury litigation; member of litigation team which obtained $43.2 million verdict against Ford   (Jablonski v. Ford) <br> <br>Successfully prepared and settled numerous class action cases <br> <br>Won appeal of trial court order allowing class wide arbitration</p>
</div></div>
<div id="workExperience-EeQu5WqfmPCbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Litigation Attorney</p>
<div class="work_company">
<span class="bold">Talbert &amp; Associates</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Alton, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2001 to February 2004</p>
<p class="work_description">Responsibilities <br>Preparation, filing and resolution of personal injury claims on behalf of plaintiffs.  <br> <br>Accomplishments <br>Successfully obtained policy limits settlements in numerous cases.  <br> <br>Skills Used <br>Analysis of potential claims; interview clients and witnesses; draft and file necessary pleadings, including complaints; prepare discovery, depose witnesses, including doctors; prepare and present pre-trial motions; negotiation of settlements, prepare and conduct trial, including jury instructions; continue to represent client though all phases of appellate process.</p>
</div></div>
<div id="workExperience-EeQu5WqfmO-bwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Litigation Associate</p>
<div class="work_company">
<span class="bold">Roth &amp; Associates</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Granite City, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 1999 to June 2000</p>
<p class="work_description">Provided defense of personal injury cases through representation of insured defendants.  <br> <br>Won appeal wherein municipality was denied insurance coverage for damages resulting from faulty fire hydrant when appellate court agreed hydrant was not an insured device.</p>
</div></div>
<div id="workExperience-EeQu5WqfmO6bwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Attorney</p>
<div class="work_company">
<span class="bold">Peel, Beatty &amp; Motil</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Glen Carbon, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 1991 to April 1999</p>
<p class="work_description">Personal injury on behalf of plaintiffs and general civil litigation.  <br>Prepared and won numerous appeals. Won appeal of Missouri Workers' Compensation case <br> <br>Obtained policy limits settlement in legal malpractice as a result of taking defendant's expert witness' deposition.</p>
</div></div>
<div id="workExperience-EeQu5WqfmO2bwRkqK6c2tA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Attorney</p>
<div class="work_company">
<span class="bold">Smith, Larson &amp; Pitts</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>East Alton, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 1987 to November 1989</p>
<p class="work_description">Responsibilities  <br> <br>Represented injured plaintiffs; helped design, create and supervise  toxic tort litigation (asbestos) department. <br> <br>Accomplishments <br> <br>Able to achieve  successful settlements on behalf of injured clients, including asbestos cases wherein numerous plaintiffs were named in the same suit.  <br> <br>Skills Used <br> <br>All skills necessary for successful preparation and resolution of personal injury cases on behalf of clients, and particularly issue analysis, negotiation and argument of complex motions.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeQu5WqfmPabwRkqK6c2tA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Juris Doctorate in Law School</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Illinois College of Law</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chan\mpaign-Utrbana</span></div>
</div>
<p class="edu_dates">1970 to 1974</p>
</div></div>
<div id="education-EeQu5WqfmPSbwRkqK6c2tA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Illinois at Champaign</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Champaign, IL</span></div>
</div>
<p class="edu_dates">1969</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Excellent ability in preparing and presenting motions and appeals. Have  won 23 of 30 appeals.  Have been involved in litigation my entire career.</span></div></div></div>
</div>
<div class="section-item military-content">
<div><div class="section_title">
<div id="add_military"></div>
<h2>Military Service</h2>
</div></div>
<div id="military-items" class="items-container"><div id="military-EeRo-EeZFE2XVuIbfZYckw" class="military-section last"><div class="data_display">
<p class="military_country"><span class="bold">Service Country:</span> United States</p>
<p class="military_branch"><span class="bold">Branch:</span> Army</p>
<p class="military_rank"><span class="bold">Rank:</span> Specialist Fifth Class</p>
<p class="military_date">June 1970 to February 1972</p>
<p class="military_description">Served overseas in South Korea; supervised record retention unit; obtained Top Secret Security Clearance; awarded rank of Specialist Fifth Class after 17 months in service which required "double waivers" of time-in-grade and time-in-service. <br> <br>Awarded Honorable Discharge, Good Conduct Medal and Armed Forces Expeditionary Medal</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>In 2002, appointed to Board of Directors of the Illinois Lawyers Assistance Program by the Illinois Supreme Court Justice for Fifth District of Illinois and approved by all the other Justices; past president and continuing board member; trained intervener.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Ff8a9dad6d8b7f6e9/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/5853cf32ecd31fb7?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>648f4317f5b918e62f854d354be1f844</uniq_id>
      <url>http://www.indeed.com/r/5853cf32ecd31fb7?hl=en&amp;co=US</url>
      <zipcode>60633</zipcode>
      <name>Property Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Property Manager</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeXp8j_T8XWogiLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Property Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Caratachea Properties</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Blue Island, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2012 to Present</p>
<p class="work_description">Responsibilities <br>Answer all tenant calls. <br>Collect all rent payments  <br>Handle all tenant issues <br>Negotiated leases</p>
</div></div>
<div id="workExperience-EeXp8csneS-tB1n7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">AVP - Assistant Branch Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">PNC BANK</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2011 to Present</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeXp8bfzZe6cAAxatb0mLA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">BS Economics</p>
<div class="edu_school"><span class="bold" itemprop="name">Northern Illinois Univerisry</span></div>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Microsoft office (10+ years)</span>, <span class="skill-text">Mac (6 years)</span>, <span class="skill-text">Sales Management (2 years)</span>, <span class="skill-text">Commercial lending (5 years)</span>, <span class="skill-text">Finance (7 years)</span>, <span class="skill-text">Property Management (4 years)</span>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F5853cf32ecd31fb7/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/94ec72eb79bd4c75?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>96d0919dd4aed22c6596d2e938830207</uniq_id>
      <url>http://www.indeed.com/r/94ec72eb79bd4c75?hl=en&amp;co=US</url>
      <zipcode>60608</zipcode>
      <name>Everything</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Everything</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeYXgwZ-PlyrXAxatb0mLA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Everything</p>
<p class="work_description">i've worked a lot of odds and ends, i am now of age to join the work force i can do many things mainly in manual labor. i've never held an official job title but am ready to be taught.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeYXgWmxNYeUdln7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">College prepatory</p>
<div class="edu_school">
<span class="bold" itemprop="name">De La Salle Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chiacgo</span></div>
</div>
<p class="edu_dates">2014 to 2017</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F94ec72eb79bd4c75/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/6c880b029c76d277?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>6c83d029c674b12a6a29dc9b43423c2b</uniq_id>
      <url>http://www.indeed.com/r/6c880b029c76d277?hl=en&amp;co=US</url>
      <zipcode>63005</zipcode>
      <name>Food Service Supervisor</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Food Service Supervisor</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Saint Louis County, MO</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebBBl_WZzifXllI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Food Service Supervisor</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Aramark</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">St. Louis, MO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2009 to Present</p>
<p class="work_description">Maintain a safe, sanitary work environment, which conforms to all standards and regulations Adhere to safety policies and accident reporting procedures. <br>Review monthly menu and ensure that food items are ordered. Assist with inventory and Requisition orders as needed for approval.</p>
</div></div>
<div id="workExperience-EebBBpLOgHChAFn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">HHA</p>
<div class="work_company">
<span class="bold">Age at home</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Saint Louis County, MO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2012 to November 2016</p>
<p class="work_description">Supports patients by providing housekeeping and laundry services; shopping for food and other household requirements; preparing and serving meals and snacks; running errands. Assists patients by providing personal services, such as, bathing, dressing, and grooming.</p>
</div></div>
<div id="workExperience-EebBBqf3N9GhAFn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">House Cleaner</p>
<div class="work_company">
<span class="bold">Self Opportunity</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>St. Louis, MO</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2011 to September 2015</p>
<p class="work_description">Responsible for cleaning houses. Vacuums, washes dishes, sweeps floors, launders clothes, cleans and scrubs counters, and dusts surfaces. <br>Clean houses with a variety of chemicals, disinfectants, and machines. <br>Vacuum hardwood floors and carpet. <br>Sweep up debris. <br>Clean toilets with toilet brush and chemicals. <br>Wash soiled clothes and linens. <br>Place fresh linens on bed. <br>Wash, fold, and stock towels.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebBBjUd-RufXllI6yVEOA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Hsd</p>
<div class="edu_school"><span class="bold" itemprop="name">University City Sr. High School</span></div>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Food service Mangement, homehealth and house cleaning (10+ years)</span></div></div></div>
</div>
<div class="section-item certification-content">
<div><div class="section_title"><h2>Certifications</h2></div></div>
<div id="certification-items" class="items-container"><div id="certification-EebBCVRbYc-hAFn7OsNiYg" class="certification-section last"><div class="data_display">
<p class="certification_title">Safe serv Handler</p>
<p class="certification_date">February 2009 to Present</p>
<p class="certification_description">Trained to handle beverages and food  and the proper way to sanitize food area.I t is valid for 5 yrs .</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Experience in providing comprehensive housekeeping services on a live-out basis at families’ primary residences. I'm very multitasking and have exceptional customer service in all my job fields. Very passionate at what I do.  manage the business to ensure that it is profitable. I also use proper sanitary. Train new employees. Home health assist with daily living and be a good companion while bringing life ,joy and happiness.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F6c880b029c76d277/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/f703e2f2dec4dc0a?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>ffa97fd88354e8a3ead48f6ab14fd077</uniq_id>
      <url>http://www.indeed.com/r/f703e2f2dec4dc0a?hl=en&amp;co=US</url>
      <zipcode>63005</zipcode>
      <name>Field Marketing Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Field Marketing Manager</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">St. Louis, MO</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="relocation_status">Willing to relocate to: St. Louis, MO</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebA-CmUNZ2ybyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Field Marketing Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Cricket Wireless</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2016 to Present</p>
</div></div>
<div id="workExperience-EeZtR83L9DyqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Promotional Model/Brand Ambassador</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Self Employed</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2015 to Present</p>
</div></div>
<div id="workExperience-EeZtRww0yJ2GfyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Jimmy John's Gourmet Sandwiches</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2012 to Present</p>
</div></div>
<div id="workExperience-EeZtR540uhiqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Hostess</p>
<div class="work_company"><span class="bold">Mission Taco Joint</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2016 to August 2016</p>
</div></div>
<div id="workExperience-EeZtRyavCoqqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Team Member</p>
<div class="work_company"><span class="bold">State Farm</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2015 to February 2016</p>
</div></div>
<div id="workExperience-EeZtRz-GwB2qHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Receptionist</p>
<div class="work_company"><span class="bold">Indigo Auto Group</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2015 to August 2015</p>
</div></div>
<div id="workExperience-EeZtR41yahmqHVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Opening Barista</p>
<div class="work_company"><span class="bold">Russell's On Macklind</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2015 to July 2015</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeZtRvv3OCmqHVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization"><div class="edu_school"><span class="bold" itemprop="name">Francis Howell High School</span></div></div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Microsoft Office (4 years)</span>, <span class="skill-text">Necho (Less than 1 year)</span>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Ff703e2f2dec4dc0a/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/14d3fe1b1adf50f1?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>51ea9cc8bbda5af2a6a3561bd3232f30</uniq_id>
      <url>http://www.indeed.com/r/14d3fe1b1adf50f1?hl=en&amp;co=US</url>
      <zipcode>60647</zipcode>
      <name>Underwriter</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Underwriter</h1>
<h2 id="headline" itemprop="jobTitle">Underwriter - American Financial Network, Inc</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Carol Stream, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeOc7tE2GmK_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Underwriter</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">American Financial Network, Inc</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2014 to Present</p>
</div></div>
<div id="workExperience-EeOc7tE2GmO_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Sr Underwriter</p>
<div class="work_company">
<span class="bold">Integra Mortgage</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Schaumburg, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2011 to 2014</p>
</div></div>
<div id="workExperience-EeOc7tE2aIS_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Sr. Trainer</p>
<div class="work_company">
<span class="bold">Diehl &amp; Associates</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Indianapolis, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2011 to 2011</p>
</div></div>
<div id="workExperience-EeOc7tE2aIW_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">SR. Government Underwriter</p>
<div class="work_company">
<span class="bold">Bridgeview Bank</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Downers Grove, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2010 to 2011</p>
</div></div>
<div id="workExperience-EeOc7tE2aIa_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Underwriter</p>
<div class="work_company">
<span class="bold">GSF Mortgage</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Aurora, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2010 to 2010</p>
</div></div>
<div id="workExperience-EeOc7tE2aIi_bLPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">President</p>
<div class="work_company"><span class="bold">Draper and Kramer Mtg</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2009 to 2010</p>
<p class="work_description">2009 Winner of Retail Support Person of the Year by the IAMP <br> <br>Over 27 years in the mortgage industry.</p>
</div></div>
<div id="workExperience-EeOc7tE2aIe_bLPItWPLJQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Underwriter</p>
<div class="work_company">
<span class="bold">Draper and Kramer Mtg</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Lombard, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2007 to 2010</p>
<p class="work_description">Received D.E. in 1999.    VA Sars 2004      LAPP 2008 <br>Sr. Sars with Draper and Kramer Mortgage Corp. <br> <br>Participated in over thirty educational seminars and managerial programs sponsored by <br>FHA, VA, Fannie Mae, Freddie Mac and private investors for professional growth and personal enrichment <br> <br>Member of the NAPMW. Passed the 203K test through Bank United. Six Time <br>Wholesale Underwriter of the year nominee. Circle of Excellence winner for 2003. <br> <br>Instructor for IAMP &amp; NAPMW accredited FHA classes</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Motivated, multi-faceted, professional with initiative, persistence and integrity. <br>Trustworthy, dependable and punctual. Detailed oriented and self motivated with high <br>energy level. Excellent oral and written communication skills. Task-conscious team <br>player - committed and focused. Good organizational skills and ability to prioritize work. <br> <br>PROFESSIONAL QUALIFICATIONS/OVERVIEW: <br> <br>Responsible for the loan management process by providing credit and loan underwriting <br>expertise to a team of Specialty Banking Advisors and Relationship Managers. Analyzes <br>prospective loan transactions and credit worthiness of borrowers <br>• Function as a group leader in a centralized operation by assisting in the training, <br>assigning, and overseeing the work of lower level underwriters. <br>• Prepared credit analysis and provides opinions and recommendations regarding <br>proposed credit and potential risks. Ensures all due diligence and background evaluation <br>is complete and thorough before credit approval is granted. <br>• Analyzed business/consumer financial statements, individual credit reports, calculate <br>cash flow and balance sheet ratios as appropriate, determine appropriate risk grades, <br>review other relevant information, and recommend the credit decision. <br>• Underwrote residential mortgage loans in accordance with FNMA, FHLMC, <br>FHA/HUD, VA and internal guidelines ensuring compliance with company and investor <br>standards. Maintain internal underwriting guidelines and regulations. <br>• Adjudicated loan applications and negotiated loan terms within lending authority, <br>balancing the customers' needs with Company's risk profile, growth, and earnings goals. <br>Protected Company's assets by making sound credit decisions. Takes responsibility to <br>deliver accurate and comprehensive fact-based applicant summary in written form. <br>• Verified employment history and deposit information, obtaining and reviewing <br>residential mortgage credit reports, property appraisals and tax returns. <br>• Made preliminary and final loan decisions regarding the salability of the loan. <br>• Maintained policy updates for conventional, FHA and VA loan program(s) to ensure <br>that changes are implemented and incorporated in product guidelines of policies and <br>procedures manual.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F14d3fe1b1adf50f1/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/965d2395ff415d16?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>caed4b465167fff523c9e9c692b15d30</uniq_id>
      <url>http://www.indeed.com/r/965d2395ff415d16?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Roofer/Laborer</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Roofer/Laborer</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Whiting, IN</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeWpxNYLChqpeFn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Roofer/Laborer</p>
<div class="work_company">
<span class="bold">Rogers Roofing</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hammond, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2015 to September 2015</p>
<p class="work_description">Responsibilities <br>Every day I would lay tarps around the house then tear off all existing materials and replace it with new materials. After the new roof is installed I cleaned up all the debris.  <br> <br>Skills Used <br>Leadership</p>
</div></div>
<div id="workExperience-EeWpxewlpvCAuyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Roofer/Laborer</p>
<div class="work_company">
<span class="bold">Great Lakes Roofing</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hammond, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2014 to July 2015</p>
<p class="work_description">Responsibilities <br> I would tear off all of the existing materials and replace it with new materials. Once the new roof is installed  I cleaned up all the debris left on the roof and the ground.</p>
</div></div>
<div id="workExperience-EeWpyAxQJD-AuyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">PACT-SN</p>
<div class="work_company">
<span class="bold">US NAVY</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Great Lakes, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2014 to February 2014</p>
<p class="work_description">Responsibilities <br>Every day I would study aircrafts and ships, ranks amd recognition  learned how to march in cadence, present arms, exercise and stand watch <br> <br>Accomplishments <br>I was the PFC guide-on in my division <br> <br>Skills Used <br>Leadership and dedication</p>
</div></div>
<div id="workExperience-EeWppAHf6FepeFn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">General laborer</p>
<div class="work_company"><span class="bold">Hoist</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2014 to 2014</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeZC49elGvGtYSLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">4 process arc welding</p>
<div class="edu_school">
<span class="bold" itemprop="name">Calumet Welding Cemter</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Griffith, IN</span></div>
</div>
<p class="edu_dates">2016 to 2016</p>
</div></div>
<div id="education-EeWpo9-BYr-peFn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High School diploma</p>
<div class="edu_school"><span class="bold" itemprop="name">George Rogers Clark</span></div>
</div></div>
</div>
</div>
<div class="section-item military-content">
<div><div class="section_title">
<div id="add_military"></div>
<h2>Military Service</h2>
</div></div>
<div id="military-items" class="items-container"><div id="military-EeWpyB2_fL2AuyLhBGsw1Q" class="military-section last"><div class="data_display">
<p class="military_country"><span class="bold">Service Country:</span> United States</p>
<p class="military_branch"><span class="bold">Branch:</span> Navy</p>
<p class="military_rank"><span class="bold">Rank:</span> E1</p>
<p class="military_date">January 2014 to February 2014</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F965d2395ff415d16/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/78ffc7000d1ec633?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7d60bedbbf5e6d9cb001d09ec2194830</uniq_id>
      <url>http://www.indeed.com/r/78ffc7000d1ec633?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Job Seeker</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn anonymous" itemprop="name">Job Seeker</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeZ5iZXyF8yF_CLhBGsw1Q" class="work-experience-section last"><div class="data_display"><p class="work_description">I have a diverse employment background including paralegal/legal secretary for Chicago law <br>firms several years ago.   I pursued Aviation and after 2 airline bankruptcies, returned to school <br>to complete my education.  I am approaching my job search as entry level as I have attended <br>school since 2009 and am currently pursuing my Master's degree in an online environment.  My <br>strongest skill is conflict resolution, customer service and written and verbal communication.  In <br>dealing with clients, customers or other parties, I can extract the necessary information and <br>achieve an end result that is mutually acceptable to both parties, <br> <br>Employment Law <br> <br>Conduct initial intake interviews and determine if client has meritorious claim under <br>direction of attorney <br>Disciplinary action and termination <br>Compliance with federal law <br>Workplace harassment <br>Employment at­will/wrongful discharge <br>Severance agreements <br>Unemployment compensation claims <br>Inform employee of rights and responsibilities <br>Employment discrimination (Race, Gender, Age, Disability, Religion, National Origin) <br>Hostile environment, such as sexual harassment <br>Family and Medical Leave Act <br>Contract violations <br>Whistleblower retaliation <br>Wage and hour disputes <br>Prepare basic pleadings <br>Draft correspondence and legal documents <br>Draft responses to EEOC and state agency charges <br>Respond to subpoenas and document requests <br>Compile, organize and index discovery <br>File court documents electronically (PACE) <br>Keep of track of attorney time and billing Timeslips <br> <br>Family Law <br> <br>Prepared Petitions, Orders <br>Reviewed and Responded to Discovery Requests <br>Conducted potential client interviews <br>Managed Court and Appointment Calendars <br>Written and verbal communication with clients, opposing counsel and court personnel <br>Maintained case files <br>Drafted and reviewed correspondence, Petitions, Pleadings and Motions <br>Maintained client billing <br> <br>Securities and Corporate Law <br> <br>Assisted attorneys in preparation of SEC filings, specifically Private Placement Memoranda <br>and accompanying BlueSky Documentation Compliance <br>Prepared and maintained meeting minutes, resolutions and other corporate records <br>Prepared financial documents, legal agreements, contracts, underwriting documents, <br>proxy statements, annual report and closing statements <br>Ensured correctness, validity and completeness of all legal and financial documents.</p></div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeZ5iZXyF9KF_CLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">M.A. in Legal Studies</p>
<div class="edu_school"><span class="bold" itemprop="name">University of Illinois</span></div>
</div></div>
<div id="education-EeZ5iZXyF9CF_CLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">B.A. in General Studies w/Professional Writing</p>
<div class="edu_school"><span class="bold" itemprop="name">Southern New Hampshire University</span></div>
<p class="edu_dates">May 2016</p>
</div></div>
<div id="education-EeZ5iZXyF86F_CLhBGsw1Q" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school"><span class="bold" itemprop="name">Governors State University</span></div>
<p class="edu_dates">2012 to 2014</p>
</div></div>
<div id="education-EeZ5iZXyF82F_CLhBGsw1Q" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school"><span class="bold" itemprop="name">William Rainey Harper College</span></div>
<p class="edu_dates">2009 to 2012</p>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F78ffc7000d1ec633/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/129893edf1c9c488?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>a2fc9fe769db4e25736decea75bae83a</uniq_id>
      <url>http://www.indeed.com/r/129893edf1c9c488?hl=en&amp;co=US</url>
      <zipcode>60653</zipcode>
      <name>Senior Research Associate</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Senior Research Associate</h1>
<h2 id="headline" itemprop="jobTitle">Senior Research Associate</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeLGIcHCQfGS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Senior Research Associate</p>
<div class="work_company">
<span class="bold">Technomic, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2011 to March 2013</p>
<p class="work_description">• Coded, reviewed and analyzed consumer brand metrics surveys to identify restaurant competitors, crave-able menu items and factors that determine restaurant choice <br>• Monitored quality and accuracy of weekly uploads ( e-Marketing briefs, high priority briefs, Foodservice Digest and editorials) to the Digital Resources Library <br>• Supported Product Coordinator with team projects and training of interns and contractors <br>• Updated the Digital Resource Library (DRL) homepage biweekly so that clients are familiar with any new research and developments Technomic has found in the food industry <br>• Researched, edited and revised restaurant positionings for the top 1,000 restaurants located in United States, Canada and United Kingdom, modifying any outdated information and making sure that all profiles are consistent to the current DRL guidelines <br>• Updated database manual for easier and more concise direction for projects and publications <br>• Created procedures and guidelines for quality control team</p>
</div></div>
<div id="workExperience-EeLGIcHCQfKS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Accounting/Administrative Intern</p>
<div class="work_company">
<span class="bold">CHD North America</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2012 to October 2012</p>
<p class="work_description">• Assisted accountant with Accounts Payable, Accounts Receivable and timekeeping <br>• Generated client billing, set up payment schedules, distributed information to account managers regularly, edited billing and entered receivables into accounting program <br>• Reconciled inter-company accounts, company credit card expenses and balance sheet accounts on a revolving basis <br>• Managed and maintained company timesheets while assisting the accountant in the integration of the Paylocity database <br>• Completed supply orders as items are needed <br>• Greeted visitors at front desk and directed them to the appropriate parties <br>• Sorted and distributed incoming mail</p>
</div></div>
<div id="workExperience-EeLGIcHCQfOS1L9oLXgqwg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Assistant Head Cashier</p>
<div class="work_company">
<span class="bold">Zara USA</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2009 to May 2011</p>
<p class="work_description">• Assisted the head cashier in motivating, training and managing a team of cashiers in the Women's, Men's and Children's department <br>• Opened and closed seven registers, placed supply orders, banking deposits and transfers between stores and the warehouse <br>• Recorded and supervised cashier discrepancies and communicated them to management and the corporate office <br>• Maintained adequate coverage throughout the store focusing primarily in the Women's departments, while managing daily scheduling and break coverage <br>• Provided excellent customer service, making sure customers enjoyed their experience and ensured fast and accurate transactions</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeLGIcHCQfWS1L9oLXgqwg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts in Finance</p>
<div class="edu_school"><span class="bold" itemprop="name">Roosevelt University</span></div>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Microsoft Word, Excel, PowerPoint, Quickbooks</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F129893edf1c9c488/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/6a7f4a79c1a13279?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>5c693c74d274cdfd6981394c78fe9da8</uniq_id>
      <url>http://www.indeed.com/r/6a7f4a79c1a13279?hl=en&amp;co=US</url>
      <zipcode>19319</zipcode>
      <name>Warehouse</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Warehouse</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Norristown, PA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Challenging position where I can continue to contribute to the growth and profitability of my employer.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeaH5tYkwwGZlipAlk2Pyg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Warehouse</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">AGIS</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Amler PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2016 to Present</p>
<p class="work_description">It's a gasket company  <br>Pick orders <br>Production of parts. <br>Preparing shipments</p>
</div></div>
<div id="workExperience-EeaH5uhZQLyu5A2SBVVUiQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Warehouse</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">AGIS</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Amler PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2016 to Present</p>
<p class="work_description">It's a gasket company  <br>Pick orders <br>Production of parts. <br>Preparing shipments</p>
</div></div>
<div id="workExperience-EeWyalZou-CpeFn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Loader</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Peoplshare/Turn 5</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">West Chester, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 2015 to Present</p>
<p class="work_description">Responsibilities <br>Load trucks and sort package. <br> <br>Skills Used <br>Forklift n hand trucks.</p>
</div></div>
<div id="workExperience-EeUgG-l8R2iR0DFyFaRtIQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Batcher</p>
<div class="work_company">
<span class="bold">Hypercat Advanced Catalyst Products</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>West Chester, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2010 to June 2015</p>
<p class="work_description">Washcoat Batcher <br>Perform a number of tasks for this manufacturer of high quality catalyst devices, custom designs emission control devices and provider of high quality catalysts at exceedingly competitive prices.  These include: <br> <br>• Blending of various precious metals and chemicals to create washcoat <br>• Prepare and apply appropriate washcoat to applicable items <br>• Process newly coated items <br>• Inventory management <br>• Safety</p>
</div></div>
<div id="workExperience-EeUgG-l8bnmR0DFyFaRtIQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Warehouse Associate</p>
<div class="work_company">
<span class="bold">Fireside Hearth and Home</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Limerick, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2003 to January 2009</p>
<p class="work_description">Perform a number of tasks for this provider of hearth and healthy home products.  These include: <br> <br>• Pick Contractor Orders <br>• Cut Marble <br>• Unload/Load Trucks <br>• Install gas/wood burning fireplaces (new construction) <br>• Scheduler (Stone work and finish work)</p>
</div></div>
<div id="workExperience-EeUgG-l8lYqR0DFyFaRtIQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Supervisor</p>
<div class="work_company">
<span class="bold">So Fun, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Bridgeport, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 1997 to December 2002</p>
<p class="work_description">SHIPPING/RECEIVING <br>Perform a number of tasks for this children's clothing manufacturer. These include: <br> <br>• Handle all aspects of shipping. <br>• Handle all aspects of receiving. <br>• Complete orders and ship to appropriate site. <br>• Receive all incoming materials and distribute to the appropriate departments. <br>• Responsible for resolving issues concerning inventory. <br>• Responsible for resolution of any issues relating to the warehouse.</p>
</div></div>
<div id="workExperience-EeUgG-l8vJuR0DFyFaRtIQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">SHIPPER/MECHANIC</p>
<div class="work_company">
<span class="bold">Dees Fluid Power</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Limerick, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 1990 to January 1997</p>
<p class="work_description">Perform a number of tasks for this hydraulic specialist company.  These include: <br> <br>• Responsible for shipping and receiving of materials and units. <br>• Responsible for shipments utilizing a computerized UPS system. <br>• Secured transportation for all larger units when necessary. <br>• Controlled inventory utilizing a computerized system. <br>• Responsible for ordering supplies when necessary. <br>• Assembly of hoses for larger hydraulic units. <br>• Managed customer requests at counter. <br>• Assisted in the training of distributors.</p>
</div></div>
<div id="workExperience-EeUgG-l846yR0DFyFaRtIQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">INSTALLER</p>
<div class="work_company">
<span class="bold">Valley Forge Heating and Cooling</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Phoenixville, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 1987 to August 1990</p>
<p class="work_description">Installed ductwork for heating and air conditioning systems. <br>• Worked on commercial as well as private contracts.</p>
</div></div>
<div id="workExperience-EeUgG-l8462R0DFyFaRtIQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">FOREMAN</p>
<div class="work_company">
<span class="bold">Lamons Gasket</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>King of Prussia, PA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 1986 to November 1987</p>
<p class="work_description">Supervised heat exchange department. <br>• Manufactured double jacket metal gaskets for industrial use.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeUgG-l9Cr6R0DFyFaRtIQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Plumbing and heating</p>
<div class="edu_school">
<span class="bold" itemprop="name">MONTGOMERY COUNTY COMMUNITY COLLEGE</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Norristown, PA</span></div>
</div>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>SPECIAL SKILLS <br> <br>Certified Fork Lift operator through current employers program <br> <br>SALARY INFORMATION <br> <br>Negotiable, depending upon position, location, responsibilities and the opportunity for personal income growth. <br> <br>REFERENCES WILL BE PROVIDED UPON REQUEST</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F6a7f4a79c1a13279/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/dbfd54ac429d1d1f?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>ce0d5b7e87d555bc642306f44d022423</uniq_id>
      <url>http://www.indeed.com/r/dbfd54ac429d1d1f?hl=en&amp;co=US</url>
      <zipcode>60657</zipcode>
      <name>Quality Control Adjunct Instructor</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Quality Control Adjunct Instructor</h1>
<h2 id="headline" itemprop="jobTitle">Director of Program Development</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Skokie, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeRNiDFp3bOXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Quality Control Adjunct Instructor</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Symbol Training Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Skokie, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2014 to Present</p>
<p class="work_description">o Deliver 10-week Quality Control for Manufacturing class curriculum to CNC 205 students <br>o Includes training in inspection, use of gages, and industrial print reading</p>
</div></div>
<div id="workExperience-EeRNiDFp3bWXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">(PTS) Manufacturing Bridge Coordinator/Lead Instructor</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Pathways to Success</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2011 to Present</p>
<p class="work_description">Erie Neighborhood House  -  Workforce Development Department <br>o Teach occupational bridge class for careers in computerized numerical control (CNC) <br>o Develop bridge curriculum including math, measurement, and print reading <br>o Provide career counseling, screening, testing and referrals for program recruits <br>o Ensure attainment of program performance benchmarks and participant goals <br>o Establish and maintain partnerships with training providers and employers <br>o Contribute to narrative of public and private proposals <br>o Follow-up with participants for one year after beginning of bridge training <br>o Represent PTS at job fair, open house, and other recruitment events <br>o Create physical files for all participants <br>o Enter personal, demographic, and outcome data into Efforts-to-Outcomes (ETO) tracking system <br>o Generate program reports upon request and write success stories</p>
</div></div>
<div id="workExperience-EeRNiDFp3bSXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Vocational ESL Instructor (part-time)</p>
<div class="work_company"><span class="bold">Tooling and Manufacturing Association</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2013 to August 2013</p>
<p class="work_description">TMA) <br>o Taught English as a Second Language to incumbent workers at TMA member companies</p>
</div></div>
<div id="workExperience-EeRNiDFp3baXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Manufacturing Bridge Instructor/Part-time ESL Instructor</p>
<div class="work_company"><span class="bold">PTS</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2008 to 2009</p>
<p class="work_description">Erie Neighborhood House <br>o Was offered a position in a workforce development pilot program for advanced manufacturing.</p>
</div></div>
<div id="workExperience-EeRNiDFqBMeXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Volunteer English Language Tutor/ESL Instructor</p>
<div class="work_company"><span class="bold">ESL</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2007 to 2009</p>
<p class="work_description">Erie Neighborhood House <br>o Started volunteer tutoring and teaching at Erie House while working as a bartender.</p>
</div></div>
<div id="workExperience-EeRNiDFqBMiXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Bartender</p>
<div class="separator-hyphen">-</div>
<p class="work_dates">2005 to 2008</p>
</div></div>
<div id="workExperience-EeRNiDFqBMmXCpTgI3RMKQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Programmer/Research Assistant</p>
<div class="work_company">
<span class="bold">University of Victoria, B.C., Canada, and Oberlin College</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Victoria, BC</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2002 to 2004</p>
<p class="work_description">o Perceptual psychological research involved programming computer games for use by high-functioning children with autism spectrum disorders.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeRNiDFqBMuXCpTgI3RMKQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">B.A. in Neuroscience</p>
<div class="edu_school"><span class="bold" itemprop="name">Oberlin College</span></div>
<p class="edu_dates">2002</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Bilingual English/Spanish</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fdbfd54ac429d1d1f/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/a6c95c212802f77c?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>01a4ac2d5e74a03f3e99ec9ab581766a</uniq_id>
      <url>http://www.indeed.com/r/a6c95c212802f77c?hl=en&amp;co=US</url>
      <zipcode>23838</zipcode>
      <name>Fitness consultant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Fitness consultant</h1>
<h2 id="headline" itemprop="jobTitle">Receptionist/Assistant</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chesterfield, VA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Looking to help ignite and organize a company with my current front desk, reception and commission sales knowledge.</p>
<p id="relocation_status">Willing to relocate to: Midlothian, VA - Henrico, VA - Colonial Heights, VA</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeUV2RZ861SpYpTgI3RMKQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Fitness consultant</p>
<div class="work_company">
<span class="bold">Victory Lady Fitness</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Midlothian, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2014 to May 2015</p>
<p class="work_description">weigh and measure of members and equipment assistance. <br> <br>Honors and Activities                 HOSA Competitor two years in a row.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebA8dWwb8S9h6mUQsuaQg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associate of Science in Psychology</p>
<div class="edu_school">
<span class="bold" itemprop="name">John Tyler Community College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chester, VA</span></div>
</div>
<p class="edu_dates">August 2014 </p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Current Knowledge of Microsoft 2013 and newer versions; including Word, Publisher, Excel, and PowerPoint <br>Workplace skills: Responsible, Willing to learn, Effecient.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fa6c95c212802f77c/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/5db8597d0e32437a?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>88f1fefeb09bc6cde1b571e13da2d61a</uniq_id>
      <url>http://www.indeed.com/r/5db8597d0e32437a?hl=en&amp;co=US</url>
      <zipcode>99003</zipcode>
      <name>Production/Cooler</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Production/Cooler</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Spokane, WA</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeJPzzNgm6CqLZvrOz8UFQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Production/Cooler</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Darigold</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Spokane, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2008 to Present</p>
<p class="work_description">Able to work in a fast pace environment safely. Can lift up to 100 pounds repeatedly. Work in colder temperatures. Able to stand on my feet and work for long periods of time. Can communicate as a productive team member and perform my duties in a timely manner.</p>
</div></div>
<div id="workExperience-EeJP0EsONGOiJROqxCdrIA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Production</p>
<div class="work_company">
<span class="bold">Wilcox</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Cheney, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2004 to August 2008</p>
<p class="work_description">I worked as a team in a physical demanding environment. Had to stand on feet for long periods of time and work various shifts. Had to palletize milk, lift heavy weight frequently.  Wilcox went out of business and I transferred to Darigold witch required the same job performances.</p>
</div></div>
<div id="workExperience-EeJP0Tq6jq2iJROqxCdrIA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Production</p>
<div class="work_company">
<span class="bold">Olympic Food</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Spokane, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 1999 to March 2004</p>
<p class="work_description">I worked in a production line. Stood for long periods able to meet physical requirements of jobs. Worked well with others and performed duties in a fast, safe manner.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeJP0V8wDe2iJROqxCdrIA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Dimploma</p>
<div class="edu_school">
<span class="bold" itemprop="name">Rogers High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Spokane, WA</span></div>
</div>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F5db8597d0e32437a/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/2d072f850fb9c5e1?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>c65b7a043d335efad3cb5ad271854c87</uniq_id>
      <url>http://www.indeed.com/r/2d072f850fb9c5e1?hl=en&amp;co=US</url>
      <zipcode>99003</zipcode>
      <name>WEB DEVELOPER</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">WEB DEVELOPER</h1>
<h2 id="headline" itemprop="jobTitle">WEB DEVELOPER - TRIPLE E TECHNOLOGIES</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Spokane, WA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">I am a full-stack web developer with almost a decade of experience in both the design and <br>development of modern web applications. I have been in charge of projects ranging from <br>internal metrics dashboards to online banking software used by thousands of credit union <br>members every day.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebBARWG6eK4yXQQHNWEsA" class="work-experience-section "><div class="data_display">
<p class="work_title title">WEB DEVELOPER</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">TRIPLE E TECHNOLOGIES</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Post Falls, ID</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2015 to Present</p>
<p class="work_description">• Used ASP.NET MVC, Entity Framework, and .NET Identity Framework to develop an on- line client portal for managing customers' private fuel card accounts. <br>• Created a video training portal for internal staff and external customer use, which inter- faces with the YouTube API to create a self-hosted, branded interface for company training <br>materials while still leveraging the convenient scale of YouTube's video platform. <br>• Used ASP.NET MVC, jQuery, Bootstrap, and Chartist.js to create a comprehensive, easy- to-use web-based dashboard for internal use by the customer support department, provid- <br>ing insight into a variety of support ticket metrics.</p>
</div></div>
<div id="workExperience-EebBARWHEPW4yXQQHNWEsA" class="work-experience-section "><div class="data_display">
<p class="work_title title">FREELANCE WEB DESIGN &amp; DEVELOPMENT</p>
<div class="separator-hyphen">-</div>
<p class="work_dates">2000 to Present</p>
<p class="work_description">• Play-tested new game content and interacted with developers to report game issues through the internal Mantis bug tracking database. <br>FREELANCE WEB DESIGN &amp; DEVELOPMENT <br>2000 - PRESENT <br>• Used Photoshop, Pixelmator, and Sketch to create design mock-ups and final site graph- ics. Wrote custom PHP code for multiple sites using frameworks such as CakePHP and <br>CMS platforms like WordPress. <br>Additional detail regarding my freelance design and development work can be found on my <br>portfolio site, http://www.revision1.com.</p>
</div></div>
<div id="workExperience-EebBARWG6eO4yXQQHNWEsA" class="work-experience-section "><div class="data_display">
<p class="work_title title">WEB DEVELOPER</p>
<div class="work_company">
<span class="bold">ENHANCED SOFTWARE PRODUCTS</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Spokane, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2007 to 2015</p>
<p class="work_description">Responsible for the development and maintenance of online banking software using <br>ASP.NET WebForms connecting to a Microsoft SQL Server back-end, which served 35 <br>client credit unions and thousands of daily users across the country. Front-end interactions <br>were managed using jQuery and jQuery Mobile. Duties included feature development, <br>interface design and development, and back-end implementation. <br>• Worked with supplied assets to create branded experiences and ad materials for client on- line banking, mobile banking, and standalone websites. <br>• Worked with the core processing system's development team to integrate online banking <br>configuration, targeted marketing, and enrollment management tools into the core desktop <br>application and SQL Server back-end. <br>• Led efforts to modernize bug tracking and internal workflow tools using JIRA.</p>
</div></div>
<div id="workExperience-EebBARWG6eS4yXQQHNWEsA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">CUSTOMER SUPPORT REPRESENTATIVE</p>
<div class="work_company">
<span class="bold">GAME MASTER, CYAN WORLDS, INC</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Mead, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2007 to 2007</p>
<p class="work_description">Provided live player support for Myst Online: Uru Live via DeskPro text chat and in-game <br>communication. <br>• Performed public, in-character orientation classes for new players at regular intervals throughout the day.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebBARWHEPe4yXQQHNWEsA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">AAS in IT Multimedia</p>
<div class="edu_school">
<span class="bold" itemprop="name">ITT Technical Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Norwood, OH</span></div>
</div>
<p class="edu_dates">2003 to 2005</p>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Microsoft Office (10+ years)</span>, <span class="skill-text">Asp.Net (9 years)</span>, <span class="skill-text">JQuery (10+ years)</span>, <span class="skill-text">Bootstrap (3 years)</span>, <span class="skill-text">Ajax (9 years)</span>, <span class="skill-text">Javascript (10+ years)</span>, <span class="skill-text">HTML 5 (10+ years)</span>, <span class="skill-text">CSS3 (10+ years)</span>, <span class="skill-text">JSON (9 years)</span>, <span class="skill-text">SQL (10+ years)</span>, <span class="skill-text">SVN (7 years)</span>, <span class="skill-text">Sketch (5 years)</span>, <span class="skill-text">Photoshop (10+ years)</span>, <span class="skill-text">Windows (10+ years)</span>, <span class="skill-text">Mac OS X (10+ years)</span>, <span class="skill-text">Visual Studio (9 years)</span>, <span class="skill-text">Web Design (10+ years)</span>, <span class="skill-text">PHP (10+ years)</span>
</div></div></div>
</div>
<div class="section-item links-content">
<div><div class="section_title"><h2>Links</h2></div></div>
<div id="link-items" class="items-container">
<div id="link-EebBAVUhQ6mfXllI6yVEOA" class="link-section "><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=http%3A%2F%2Fcodepen.io%2Fgbuddell%2F&amp;h=24a2c401">http://codepen.io/gbuddell/</a></p></div></div>
<div id="link-EebBAZhO1bKLuNx7tzCqKQ" class="link-section "><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=http%3A%2F%2Fstackoverflow.com%2Fusers%2F4584994%2Falahmnat%3Ftab%3Dprofile&amp;h=31c26f84">http://stackoverflow.com/users/4584994/alahmnat?tab=profile</a></p></div></div>
<div id="link-EebBAXzgvRiLuNx7tzCqKQ" class="link-section last"><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=https%3A%2F%2Fgithub.com%2Falahmnat&amp;h=d35b9ef3">https://github.com/alahmnat</a></p></div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F2d072f850fb9c5e1/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/dfa189ae595a1d9c?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>ce9d1c2352b763ab4f873d6033c230cb</uniq_id>
      <url>http://www.indeed.com/r/dfa189ae595a1d9c?hl=en&amp;co=US</url>
      <zipcode>99003</zipcode>
      <name>Job Seeker</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn anonymous" itemprop="name">Job Seeker</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Spokane, WA</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-Eea_RSbhMo299Vn7OsNiYg" class="work-experience-section last"><div class="data_display"><p class="work_description">GROUP DENTAL CLAIMS <br>PlanHolder: P O BOX 2459 <br>SPOKANE WA 99210-2459 <br>FRY-WAGNER SYSTEMS, INC. <br>PROVIDER SELECTION: You are free to decide which provider to use at any time. However, you <br>Subscriber: can generally reduce your out-of-pocket expenses if you use a DentalGuard Preferred PPO network <br>NICHOLAS L BLUMENTHAL                               provider. To find PPO network providers in your area, consult your directory, visit <br>05506000834 <br> <br>www.GuardianAnytime.com or call the toll free number. <br>dependents, if enrolled <br>Guardian DentalGuard         Member ID: 925024610 <br>Plan Number: G-00529012 <br> <br>See your benefits booklet for a description of benefits, terms and conditions, limitations and <br>exclusions of coverage. <br>This card is for identification purposes only and does not guarantee eligibility to receive services. <br> <br>[Data_Value]_CARD1+925024610+00529012+DEN+1+M+20160706+++CARD</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fdfa189ae595a1d9c/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/d4db01e7028fbe3a?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>80cd35da9e62ae8d188bb93b8cad6002</uniq_id>
      <url>http://www.indeed.com/r/d4db01e7028fbe3a?hl=en&amp;co=US</url>
      <zipcode>60626</zipcode>
      <name>Site Coordinator</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Site Coordinator</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Glenview, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeRSnaPDXMmXVuIbfZYckw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Site Coordinator</p>
<div class="work_company">
<span class="bold">Gilbert Tile</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Dyer, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2013 to August 2014</p>
<p class="work_description">Responsibilities <br>Each day at the assigned property, I would conduct site observations of my employees and contractors ensuring employees met safety policies and followed all rules and regulations. <br> <br>Accomplishments <br>working for this company showed me the importance of team work, if each person on the job including myself did not contribute to the task at hand, the job would not be done efficiently and would reflect our company. <br> <br>Skills Used <br>Team work, Orginization, Leader Ship, and  Task management.</p>
</div></div>
<div id="workExperience-EeRSmsiQ7febwRkqK6c2tA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Cashier/Customer Service</p>
<div class="work_company">
<span class="bold">Menchies</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Highland Park, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2012 to January 2013</p>
<p class="work_description">Responsibilities <br>Each day I opened and closed the store. Since it is a ice cream shop I served customers the finest ice cream in  highland park. I would Handel cash and clean the shop after customers would leave. I Worked very closely with children and enjoyed seeing the smiling faces of children each day.  <br> <br>Accomplishments <br>working with not only people but children helped me to understand and value the importance of respect. Showing customers respect not only reflects you but also reflects your company. <br> <br>Skills Used <br>100 Percent customer service, Team work, organization, and efficiency.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeRSnff4hcSXVuIbfZYckw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">G.E.D in General education</p>
<div class="edu_school">
<span class="bold" itemprop="name">Grays Lake CLC</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Grays Lake, IL</span></div>
</div>
<p class="edu_dates">2011 to 2012</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fd4db01e7028fbe3a/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/0e21cff7b72e3365?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>334b5cf540454016e8ae4dc0f6246bc5</uniq_id>
      <url>http://www.indeed.com/r/0e21cff7b72e3365?hl=en&amp;co=US</url>
      <zipcode>45620</zipcode>
      <name>millwright</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">millwright</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Ripley, WV</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeI_TfPUiYaMofHkoT2skA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">millwright</p>
<div class="work_company">
<span class="bold">electronic automation corporation</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Columbus, OH</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 2008 to December 2012</p>
<p class="work_description">I am currently a millwright for electronics automation corporation. At my current job i assembly and maintence automated storage and retreival systems. (asrs) My job duties consist of floor layout, reading blueprints, building mezzanines, installing and leveling conveyer, using optical levels, transits, and high rise lifting/ working. I have 4 years experience working for EAC. Im also fluent in mig/arc welding, plasma cutters and oxycetylene torches. High school graduate with 2 years in auto mechanics from Roane- Jackson technical center.   Thank you,   Shane Casto</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeI_TizECOSqLZvrOz8UFQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">Ripley HS, Roane- Jackson technical center</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Ripley, WV</span></div>
</div>
<p class="edu_dates">2003 to 2006</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F0e21cff7b72e3365/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/99520b1ebdafe711?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7b162be051acaf11a70e351966d7b582</uniq_id>
      <url>http://www.indeed.com/r/99520b1ebdafe711?hl=en&amp;co=US</url>
      <zipcode>28533</zipcode>
      <name>Customer Service and Problem Resolution Representative</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Customer Service and Problem Resolution Representative</h1>
<h2 id="headline" itemprop="jobTitle">Customer Service and Problem Resolution Representative - Convergys Corporation</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Camp Lejeune, NC</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeVtA-_G8TqBzRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer Service and Problem Resolution Representative</p>
<div class="work_company"><span class="bold">Convergys Corporation</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2014 to October 2016</p>
<p class="work_description">(Work from home position) <br>• Interacts with customers via inbound or outbound calls or the Internet for the purpose of resolving routine problems with products or services. <br>• Confirm customer understanding of the solution and provide additional customer education as needed <br>• Solve problems that are sometimes unstructured and that may require reliance on conceptual thinking <br>• Maintain broad knowledge of client products and services</p>
</div></div>
<div id="workExperience-EeVtA-_G8TuBzRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">COM Coordinator</p>
<div class="work_company">
<span class="bold">Atlantic Marine Corps Communities</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Camp Lejeune, NC</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2013 to September 2014</p>
<p class="work_description">• Scheduling &amp; budgeting <br>• Generates routine quality reports on a timely basis and assists maintenance managers in their absence <br>• Manage YARDI database <br>• Responsible for the "turnover" assignments of vacant houses in an expeditious manner in order to avoid the loss of any rental income <br>• Promote and practice incident-injury free (IIF) and sustainability <br>• Maintains a close and highly responsive relationship to the day-to-day activities of the supervisor, staff and contractors.</p>
</div></div>
<div id="workExperience-EeVtA-_G8TyBzRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Account Team Leader</p>
<div class="work_company">
<span class="bold">American Diabetes Association</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Alexandria, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2005 to May 2008</p>
<p class="work_description">• Handles scheduling and operations.  Monitoring the team's performance floor, and  make sure that the team stays on schedule and meets their performance metrics. <br>• Generates routine quality reports on a timely basis.</p>
</div></div>
<div id="workExperience-EeVtA-_HGE2BzRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Account Executive</p>
<div class="work_company"><span class="bold">Cyber City Teleservices</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2004 to 2005</p>
<p class="work_description">• Served as a liaison between company and clients; interacting with client representatives at all levels. Including senior management. <br>• Provided Call Center trainers with new account information and tools to coordinate training efforts <br>• Responsible for distributing daily call statistic reports to the client and account management team.</p>
</div></div>
<div id="workExperience-EeVtA-_HGE-BzRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Team Manager</p>
<div class="work_company"><span class="bold">RMH Teleservices</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2003 to February 2004</p>
<p class="work_description">• Oversees the administration and coordination of specific client programs <br>• Responsible for leading a team of Customer Care Associates and improving team performance through analysis, coaching and mentoring.</p>
</div></div>
<div id="workExperience-EeVtA-_HGE6BzRkqK6c2tA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Call Center Agent II</p>
<div class="work_company"><span class="bold">Cyber City Teleservices</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2001 to May 2003</p>
<p class="work_description">Takes booth inbound and outbound calls regarding orders for corporate products and services.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeVtA-_HGFGBzRkqK6c2tA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in Business Management</p>
<div class="edu_school"><span class="bold" itemprop="name">Xavier University</span></div>
<p class="edu_dates">1998 to 2002</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>• Highly motivated team player with strong leadership skills <br>• Values continuous learning and personal/professional development <br>• Excellent communication and presentation skills <br>• Can easily relate with people from different level of organization &amp; different cultural background <br>• High level of flexibility and adaptability</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F99520b1ebdafe711/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/3b3ab57b494d8c08?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>c85a681ede7f8766db74a4ecf98b188e</uniq_id>
      <url>http://www.indeed.com/r/3b3ab57b494d8c08?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Dialysis Technician</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Dialysis Technician</h1>
<h2 id="headline" itemprop="jobTitle">Dialysis Technician</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Bellwood, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">A Professional, seeking a position where I will develop and excel while giving my best to the employer.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeawLkMeugixrln7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Dialysis Technician</p>
<div class="work_company">
<span class="bold">Fresenius Medical Care</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2014 to October 2016</p>
<p class="work_description">• Provided dialysis treatments <br>• Managed patients vital signs and progress on dialysis <br>• Responsible for maintenance of water treatment for dialysis machine <br>• Trained new employees</p>
</div></div>
<div id="workExperience-EeawLkMeugWxrln7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Dialysis Technician</p>
<div class="work_company">
<span class="bold">Circle Medical Management</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2013 to March 2014</p>
<p class="work_description">• Provided dialysis treatments <br>• Managed patients vital signs and progress on dialysis <br>• Responsible for maintenance of water treatment for dialysis machine <br>• Trained new employees <br>• Nx stage</p>
</div></div>
<div id="workExperience-EeawLkMeugaxrln7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">CCT.EMT-B</p>
<div class="work_company">
<span class="bold">Superior Ambulance Service</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elmhurst, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2002 to October 2011</p>
<p class="work_description">Critical Care EMT <br>• Provide Emergency Medical Transports <br>• Provide  high quality specialized medical care, comparable to that available in the hospital setting <br>• Utilize high tech equipment to stabilize and maintain the needs of the most critical patients <br>• Scope of practice supported by The St. Francis EMS System and is IDPH approved <br>• Critical Care Scope Of Practice <br>• Access ventilator dependant patients <br>• Responsible for trauma patients I.V medications, blood products, all pacing modalities, and chest tubes</p>
</div></div>
<div id="workExperience-EeawLkMeugmxrln7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Training Coordinator</p>
<div class="work_company">
<span class="bold">Wal-Mart</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 1996 to October 2002</p>
<p class="work_description">• Trained new employees and managed the workflow and training for new employees <br>• Managed a department and placed orders for customers <br>• Managed money for cashiers and processed inventory adjustments <br>• Prepared payroll reports using MS Excel spreadsheets <br>• Handled personal / confidential documents for employees</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeawLkMeug2xrln7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">ASSOCIATES in NURSING RN</p>
<div class="edu_school">
<span class="bold" itemprop="name">Triton College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">River Grove, IL</span></div>
</div>
<p class="edu_dates">October 1998 to October 2017</p>
</div></div>
<div id="education-EeawLkMeuguxrln7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Applied Science in Criminal Justice</p>
<div class="edu_school">
<span class="bold" itemprop="name">Westwood College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">March 2012</p>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F3b3ab57b494d8c08/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/171013c3195c3cca?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>baa55217ab51d8d4ab68b1f74856e677</uniq_id>
      <url>http://www.indeed.com/r/171013c3195c3cca?hl=en&amp;co=US</url>
      <zipcode>60628</zipcode>
      <name>Account Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Account Manager</h1>
<h2 id="headline" itemprop="jobTitle">Account Manager at UBS, GLOBAL ASSET MANAGEMENT</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeF4TATfNW6B3_HkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Account Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">UBS, GLOBAL ASSET MANAGEMENT</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2010 to Present</p>
<p class="work_description">* Work on the Public Pension Fund Team to market to and service institutional clients; the seven member <br>team manages 35 client relationships with $10.4 billion invested with UBS Global Asset Management <br>* Responsible for the administration of top tier institutional client portfolios totaling $5.1 billion in assets: report strategy changes, holdings, performance, attribution, benchmark analysis, and customized presentations <br>* Support Client Advisor business development activity by conducting due diligence on key prospective client portfolios to identify opportunities for UBS investment strategies <br>* Work with over 50 investment strategies including domestic and global fixed income, equities, balanced, real estate, and infrastructure <br>* Prepare presentation materials and participate in portfolio review meetings with institutional clients <br>* Communicate with internal equity and fixed income operations and custodian banks to resolve portfolio discrepancies <br>* Monitor institutional client investment portfolios ensuring compliance with stated investment guidelines</p>
</div></div>
<div id="workExperience-EeF4TATfNW-B3_HkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Fixed Income Trade Support Analyst</p>
<div class="work_company"><span class="bold">UBS, GLOBAL ASSET MANAGEMENT - Chicago, IL</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2008 to July 2010</p>
<p class="work_description">* Liaison between clients and prime brokers/counterparties <br>* Providing support for all trade activity from the portfolio managers <br>* Established working relationships with external and internal clients <br>* Continue to build upon existing procedures while also creating support guides for new procedures <br>* Investigate trade settlements, review transactions, reconcile trade discrepancies, while working in conjunction with custodians, fund accounting, brokers and internal business groups</p>
</div></div>
<div id="workExperience-EeF4TATfNXCB3_HkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Portfolio Implementation Administrator/Portfolio Monitoring Analyst</p>
<div class="work_company"><span class="bold">UBS, GLOBAL ASSET MANAGEMENT - Chicago, IL</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2007 to August 2008</p>
<p class="work_description">* Reconciliation and forecasting of cash balances to portfolios managers, cash rebalancing, and miscellaneous internal reporting as needed <br>* Monitor assigned investment portfolios, identifying potential compliance risks to ensure that <br>Client and/or regulatory guidelines are followed <br>* Daily advising and thorough interaction with the proper internal investment groups, Client Advisors, and Legal &amp; Compliance</p>
</div></div>
<div id="workExperience-EeF4TATfNXGB3_HkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Trader Trainee</p>
<div class="work_company">
<span class="bold">THE LEAGUE CORPORATION</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2006 to June 2007</p>
<p class="work_description">Chicago Board of Trade <br>* Continued education on Option Theory <br>* Maintained Trader's overall trading portfolio <br>* Followed market movement so that it accurately corresponded with trade execution prices <br>* Prepared sheets, input trades, adjusted volatility and spread prices according to market movement <br>* Identified trading strategies and associated portfolio risk <br>* Worked closely with brokers to settle price and quantity out trades</p>
</div></div>
<div id="workExperience-EeF4TATfNXKB3_HkoT2skA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Office Manager</p>
<div class="work_company">
<span class="bold">KZG CONCEPTS</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Lake Forest, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2004 to August 2006</p>
<p class="work_description">* Assisted in 80% of all aspects of setting up KZG Concepts <br>* Coordinated all manufacturing and sample production with our factories in China <br>* Managed and provided product information to client base <br>* Implemented QuickBooks and EDI systems <br>* Displayed leadership by managing two employees <br>* Balanced inventory levels by forecasting future sales</p>
</div></div>
<div id="workExperience-EeF4TATfXIOB3_HkoT2skA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Floor Book Clerk</p>
<div class="work_company">
<span class="bold">KNIGHT FINANCIAL</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2000 to August 2002</p>
<p class="work_description">Chicago Board of Options <br>* Introduction to Option Theory of Volatility Pricing &amp; Screen Based Derivative Trading Models <br>* Executed client orders with the DPM, Market Makers, and Brokers <br>* Reconciled morning trades and those during expiration <br>* Monitored several competitive exchange markets in order to provide the best fill for clients</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeF4TATfXIWB3_HkoT2skA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in Finance</p>
<div class="edu_school">
<span class="bold" itemprop="name">DEPAUL UNIVERSITY</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">November 2006</p>
</div></div>
<div id="education-EeF4TATfXIeB3_HkoT2skA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">International Business Seminar</p>
<div class="edu_school"><span class="bold" itemprop="name">DEPAUL UNIVERSTIY</span></div>
<p class="edu_dates">March 2006</p>
</div></div>
<div id="education-EeF4TATfXImB3_HkoT2skA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Oak Lawn Marketing</p>
<div class="edu_school"><span class="bold" itemprop="name">Osaka University Robotics</span></div>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F171013c3195c3cca/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ec278222d057a695?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>9e72614e1a4d6e3d1dc4c8b9b573d415</uniq_id>
      <url>http://www.indeed.com/r/ec278222d057a695?hl=en&amp;co=US</url>
      <zipcode>99003</zipcode>
      <name>Respiratory Therapist</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Respiratory Therapist</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Nine Mile Falls, WA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeTson2931K6RyTELe7S5A" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Respiratory Therapist</p>
<div class="work_company"><span class="bold">Deaconess medical center</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">1987 to 2005</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeTsor9xBoy6RyTELe7S5A" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">AA</p>
<div class="edu_school"><span class="bold" itemprop="name">SCC</span></div>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fec278222d057a695/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/9797b0cc2093303c?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>c7046637704113139420a6723281b08f</uniq_id>
      <url>http://www.indeed.com/r/9797b0cc2093303c?hl=en&amp;co=US</url>
      <zipcode>60657</zipcode>
      <name>Administrative Assistant/Secretary</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Administrative Assistant/Secretary</h1>
<h2 id="headline" itemprop="jobTitle">Administrative Assistant/Secretary - ACG</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EealGfzwcBK8zhuvKUiAOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant/Secretary</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">ACG: The al Chalabi Group, Ltd.</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 1999 to Present</p>
<p class="work_description">Provide administrative/secretarial support. Assist with the creation and editing of reports, spreadsheets, and PowerPoint presentations reflecting socio-economic forecasts and impacts of major transportation projects – highways, toll roads, rail, bridges, and airports. Generate Excel charts from data. Convert and merge files to Adobe PDF format. Research and collect information for proposals, contracts, and certifications. Create pamphlets, booklets, and newsletters for clients. Assist with accounts payable/receivable, memos, letters, Emails, and Adobe PDF forms. Import, scan, edit and restore pictures, graphs and maps for distribution.  Other duties include:  Maintaining the electronic and paper filing system, and the office supply inventory log and reorder.  Troubleshoot computer software and hardware problems, switchboard, copying, faxing, printing, and binding.</p>
</div></div>
<div id="workExperience-EealGfzwlyO8zhuvKUiAOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company">
<span class="bold">Olsten Staffing - Staffing Services</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 1999 to August 1999</p>
<p class="work_description">On assignment at ACG: The al Chalabi Group, Ltd.</p>
</div></div>
<div id="workExperience-EealGfzwlyS8zhuvKUiAOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Menu Development Assistant/Office Clerk</p>
<div class="work_company">
<span class="bold">Sodexho Marriott - Food Services</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 1998 to May 1999</p>
<p class="work_description">Responsibilities included the data entry and printing of fourteen food menus in English and Spanish. Prepared Nutritional Analysis and Production Records of the menus, for monthly distribution, to the schools. Other duties included: customer service and switchboard assistance, as needed.</p>
</div></div>
<div id="workExperience-EealGfzwvjW8zhuvKUiAOA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Accounts Receivable Clerk</p>
<div class="work_company">
<span class="bold">Capitol News Agency - Magazine Distributors</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 1992 to February 1998</p>
<p class="work_description">Responsibilities included: accounts payable/receivable, daily cash balancing, inventory, budget, customer service and data entry.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EealGvMo-KSNTgxatb0mLA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Computerized Accounting (Certified)</p>
<div class="edu_school">
<span class="bold" itemprop="name">The College of Office Technology</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">1992 to 1992</p>
</div></div>
<div id="education-EealGxx3KHud1Vn7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Office Technology (Certified)</p>
<div class="edu_school">
<span class="bold" itemprop="name">Spanish Coalition for Jobs, Inc.</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">1990 to 1990</p>
</div></div>
<div id="education-EealG19V_jed1Vn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">General Education Courses, ESL</p>
<div class="edu_school">
<span class="bold" itemprop="name">Richard J. Daley College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">1987 to 1988</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Microsoft Office Applications: Word, Excel, PowerPoint, Outlook, Picture It, Publisher, and OneNote.  Adobe Acrobat, PDF Form Creator, and Photoshop.  Word Perfect, Lotus 123, PageMaker, and OmniForm.  Typing 45 wpm, Numeric Key Pad.  Bilingual:  English/Spanish. (10+ years)</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F9797b0cc2093303c/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/af730703ccae081e?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>20172d7309b79d03de21c88d02305d29</uniq_id>
      <url>http://www.indeed.com/r/af730703ccae081e?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Bike/walking courier</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Bike/walking courier</h1>
<h2 id="headline" itemprop="jobTitle">Bike/walking courier - Postmates</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To obtain employment with an organization that is in need of a highly energized and dedicated employee.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebAwQFfkK-5_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Bike/walking courier</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Postmates</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2016 to Present</p>
<p class="work_description">• Secure handling/delivering of orders <br>• Understanding of ins/outs of city and quickest routes <br>• Customer service</p>
</div></div>
<div id="workExperience-EebAwQFfkLC5_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer service</p>
<div class="work_company"><span class="bold">Sprig</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2015 to June 2016</p>
<p class="work_description">• Cooking/packaging of food <br>• Heavy lifting (up to 50lbs.; up/down stairs) <br>• Tech heavy (word + excel documents) <br>• Inventory/maintenance of company supplies <br>• Close relationship to delivery drivers &amp; driver routes</p>
</div></div>
<div id="workExperience-EebAwQFfkLG5_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Server</p>
<div class="work_company">
<span class="bold">Po Boy Jim</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Washington, DC</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2014 to June 2015</p>
<p class="work_description">• Food runner <br>• Handled register/cashier (call in orders + POS) <br>• Close customer service (many regulars) <br>• Booked parties <br>• Side jobs/organized and cleaned restaurant</p>
</div></div>
<div id="workExperience-EebAwQFfkLK5_llI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer service</p>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2014 to September 2014</p>
<p class="work_description">Inventory</p>
</div></div>
<div id="workExperience-EebAwQFfkLO5_llI6yVEOA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Parent customer service</p>
<div class="work_company"><span class="bold">Rockwell Baseball Academy</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2013 to March 2014</p>
<p class="work_description">Fall 2013 - Spring 2014 <br>• Supported training of High School Baseball Teams <br>• Trained Youth in Baseball Clinics ages 5-12 <br>• Cleaned equipment and the facilities <br>• Parent customer service</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebAwQFfkLW5_llI6yVEOA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts</p>
<div class="edu_school">
<span class="bold" itemprop="name">North Park University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">September 2013 to March 2014</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Skills <br>• Microsoft Word, PowerPoint, Adobe <br> <br>• Organizational Skills <br>• Customer Service</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Faf730703ccae081e/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/1c2f32acf25e2170?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7ffb92f77b4af349ae047a7a464492fc</uniq_id>
      <url>http://www.indeed.com/r/1c2f32acf25e2170?hl=en&amp;co=US</url>
      <zipcode>60652</zipcode>
      <name>Various Temporary Assignments</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Various Temporary Assignments</h1>
<h2 id="headline" itemprop="jobTitle">Various Temporary Assignments - Adecco Staffing NA</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Full time employment in Customer Service, Addictions Counseling, Marriage and Family Therapy</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeZtm15ZylKqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Various Temporary Assignments</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Adecco Staffing NA</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2004 to Present</p>
<p class="work_description">to include</p>
</div></div>
<div id="workExperience-EeZtm15ZylOqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer Service</p>
<div class="work_company">
<span class="bold">Adecco Staffing NA</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2014 to June 2015</p>
</div></div>
<div id="workExperience-EeZtm15ZylqqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">Affiliated Physicians</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2007 to August 2007</p>
</div></div>
<div id="workExperience-EeZtm15ZyluqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">Oce NA</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2004 to May 2007</p>
</div></div>
<div id="workExperience-EeZtm15ZylyqHVn7OsNiYg" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">ABN/AMRO Mortgage</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2004 to April 2004</p>
</div></div>
<div id="workExperience-EeZtm15Zyl2qHVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Travel Consultant</p>
<div class="work_company">
<span class="bold">American Express</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Rolling Meadows, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 1996 to December 2001</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeZtm15ZymGqHVn7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Master's in Counseling/Marriage and Family</p>
<div class="edu_school">
<span class="bold" itemprop="name">Capella University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Minneapolis, MN</span></div>
</div>
<p class="edu_dates">August 2017</p>
</div></div>
<div id="education-EeZtm15Zyl-qHVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelors in Psychology/Substance Abuse</p>
<div class="edu_school">
<span class="bold" itemprop="name">Kaplan University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">August 2012</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">MS Office (10+ years)</span>, <span class="skill-text">MS Word (10+ years)</span>, <span class="skill-text">MS Powerpoint (5 years)</span>, <span class="skill-text">SAP (8 years)</span>, <span class="skill-text">Salesforce (9 years)</span>, <span class="skill-text">OMSI (8 years)</span>, <span class="skill-text">Brunet (8 years)</span>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>SUMMARY OF SKILLS: <br>Organizational <br>• Analyze verification of documentation, negotiate billing issues and managed correction of billing issues as well as arrange payments and discuss resolution options in accordance with company collection policies. Skilled in Excel, SAP, Sales Force, ICOMMS, Brunet and QuickenBooks. <br>• Preparation of travel itineraries for corporate accounts. Researched and calculated air fares, and provided price quotes for domestic and international destinations. Skilled in Sabre, Apollo, and World span reservation systems. <br>• Assisted customers on mortgage issues such as insurance matters, resolve escrow, taxes and corrected billing problems. Skilled in Info Image. <br> <br>Secretarial <br>• Experienced in document formatting, proofreading, and administrative assistant duties. <br>• Skilled in MS Works, MS Office, Excel, PowerPoint, and Lotus, SAP, SalesForce, Team Approach, Convio <br>• Ability to type 60 words per minute with no errors. <br> <br>Communication <br>• Conversed with clients either on the phone or through email. <br>• Worked on Call Center Management Project as well as on Galileo Project, Magellan, and DISC Personality</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F1c2f32acf25e2170/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/e546c3995c71f4ed?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>a7b9b5e4a64c96e6ec8b26611ba675e5</uniq_id>
      <url>http://www.indeed.com/r/e546c3995c71f4ed?hl=en&amp;co=US</url>
      <zipcode>60621</zipcode>
      <name>Home Caregiver</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Home Caregiver</h1>
<h2 id="headline" itemprop="jobTitle">Loving person and has a good personality</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">I love learning new thing and is a very hard worker.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeVXgMSjE12teyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Home Caregiver</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Nanny for Toddler</span></div>
</div></div>
<div id="workExperience-EeVXgOc-U1iteyLhBGsw1Q" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Volunteer</p>
<div class="work_company"><span class="bold">Bouquet store</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2012 to 2012</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeVXgUqXERit8Fn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization"><div class="edu_school"><span class="bold" itemprop="name">Cornerstone Christian Correspondence</span></div></div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">inventory/cashier (Less than 1 year)</span></div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Am a people person and i have a great personality. Am a very hard worker and love learn new skills and improving my old ones</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fe546c3995c71f4ed/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/9a85011600af913d?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>9a6de2f34404b1457def582865600062</uniq_id>
      <url>http://www.indeed.com/r/9a85011600af913d?hl=en&amp;co=US</url>
      <zipcode>60621</zipcode>
      <name>Senior Property Management Analyst</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Senior Property Management Analyst</h1>
<h2 id="headline" itemprop="jobTitle">Senior Property Management Analyst - FORESITE REALTY PARTNERS, LLC</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Naperville, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeUujh_9-au4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Senior Property Management Analyst</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">FORESITE REALTY PARTNERS, LLC</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Rosemont, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2014 to Present</p>
<p class="work_description">Hold leadership accountability for the timely processing of the organization's CAM, Real Estate Tax and <br>Insurance Reconciliations for all commercial (retail, office and industrial) properties. Oversee daily lease <br>portfolio administration functions, with a core focus on ensuring Foresite captures and properly distributes all <br>expenses to tenants based on square footage of individual units. Monitor rent payment processes to verify <br>adherence to timelines; develop lease abstracts for interdepartmental utilization. Conduct lease audits to recover all expenses that occur throughout year, ensure expenses are in strict compliance with lease agreements and interpretations and the execution of proper accounting practices. Manage all real estate functions for various internal business units: develop/implement applicable standards, operations and objectives across all <br>inter-company procedures in relation to maintenance, real estate tax and risk management for each property. <br>Work closely with property owners/tenants on billing/collections. Maintain all property accountability <br>documentation. <br>• Educate Foresite personnel and ensure accuracy regarding property business system applications and cost/allocation processes, as well as lease interpretation and compliance requirements (tenant's dispute <br>resolution). <br>• Assess internal properties and perform analysis of results for financial/operational objectives. <br>• Serve as Subject Matter Expert (SME) for property life cycle management, including contracts/regulations. <br>• Orchestrate the development and implementation of property management policies. <br>• Prepare special reports pertaining to issues, including taxes, leasing, insurance and facility utilization. <br>• Communicate frequently with vendors, government agencies and internal staff to investigate and rectify <br>issues regarding regulatory compliance, ownership accountability and tenant relations/responsibility. <br>• Support Asset Management Department in preparing annual budgets/asset plans, reviewing property reports for accuracy/variances and performing due diligence for acquisitions/dispositions.</p>
</div></div>
<div id="workExperience-EeUujh_9-a24oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Senior Lease Analyst</p>
<div class="work_company"><span class="bold">OFFICEMAX</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2013 to 2014</p>
<p class="work_description">Performed comprehensive review and administered approvals of payments for non-escrowed lease-related <br>expenses, ensuring strict payment compliance with lease requirements and identifying areas of concern. <br>Consistently enhanced Lease Administration software functionality by populating lease/payment data within system to streamline the recovery process. Verified the timely/accurate modification of lease data; developed <br>mission-critical statistical reports for reporting, financial projection, comparison and analysis functions. <br> <br>-- OfficeMax; Senior Lease Analyst;  on Second Page -- <br> <br>OfficeMax; Senior Lease Analyst, <br>Managed all facets of the third party auditing process to achieve overpaid rent/expense recovery. Oversaw <br>departmental functions of Lease Analysts; participated in the recruitment and training/development of new <br>personnel. <br>• Worked closely with third party auditors throughout auditing process to efficiently process claims <br>pertaining to payment recoveries. <br>• Promptly identified/rectified escalated landlord issues.</p>
</div></div>
<div id="workExperience-EeUujh_9-ay4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<div class="work_company">
<span class="bold">OFFICEMAX</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Naperville, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2006 to 2014</p>
</div></div>
<div id="workExperience-EeUujh_9-a64oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Financial Analyst</p>
<div class="work_company"><span class="bold">OFFICEMAX</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2006 to 2013</p>
<p class="work_description">Direct accountability for the daily financial oversight of OfficeMax locations including negotiating leases, <br>auditing contracts for compliance, sales/site evaluations, accounts payable, financial compliance, tax, square <br>footage reporting, etc. Coordinated and negotiated with property owners, vendors and local and state <br>government officials. Conducted site evaluations ensuring lease profitability; analyzed store P&amp;L statements and sales data. Responsible for negotiating with owners/management companies to ensure lease/contract <br>compliance. Ensured integrity of financial and management reporting of lease related rents and subtenant <br>income. <br>• Reviewed past expenses, tax and leases to identify potential cost savings, payment oversights and missed <br>incentives; created comprehensive contract analyses and negotiated with property owners to secure savings.</p>
</div></div>
<div id="workExperience-EeUujh_9-a-4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<div class="work_company">
<span class="bold">MCI</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Downers Grove, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2005 to 2006</p>
<p class="work_description">1997 to 2002</p>
</div></div>
<div id="workExperience-EeUujh_9-bC4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Financial Analyst</p>
<div class="work_company"><span class="bold">MCI</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2005 to 2006</p>
<p class="work_description">Direct responsibility for creating, ensuring accuracy and monitoring contracts for all new telecommunication <br>technology sales for global accounts. Analyzed all facets of contracts, invoices and billing; assessed contracts in areas of daily operations, revenue reporting and project specs to ensure revenue viability. Coordinated with <br>sales, business development, credit and clients to analyze contracts. Aided in developing contracts. <br>• Effectively employed proficiencies in technical systems, operations and corporate policies to ensure project <br>profitability.</p>
</div></div>
<div id="workExperience-EeUujh_9-bO4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Regional Operations Manager</p>
<div class="work_company">
<span class="bold">CAPITOL NEWS AGENCY</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2002 to 2005</p>
<p class="work_description">Held total operational and planning responsibility for a $20M retail and distribution territory. Managed and directed all facets of operations, encompassing sales, budgets, finance, inventory, human resources, etc. <br>• Developed and implemented new procedures to significantly reduce shrink and employee turnover; yielded <br>strong gross profit growth numbers annually.</p>
</div></div>
<div id="workExperience-EeUujh_9-bG4oVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Supervisor</p>
<div class="work_company"><span class="bold">Operations Service Center</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2000 to 2002</p>
</div></div>
<div id="workExperience-EeUujh_9-bK4oVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Telecommunications Technician</p>
<div class="separator-hyphen">-</div>
<p class="work_dates">1997 to 2000</p>
<p class="work_description">Orchestrated and directed central region's network operations maintenance group. Managed ten maintenance <br>technicians responsible for rectifying service issues remotely and on-site. Trained, mentored and distributed <br>technicians. Held full oversight responsibility for installation projects from contract close through fulfillment. <br>Coordinated between customers, vendors, local exchange carriers and multiple internal teams to complete <br>projects on time and on budget. <br>• Recipient, "Ameritech/SBC Peer Recognition" award; "Peak Achiever" award; and two "Network All Star" <br>awards.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeUujh_9-bW4oVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in International Relations &amp; Economics</p>
<div class="edu_school">
<span class="bold" itemprop="name">NORTHERN ILLINOIS UNIVERSITY DeKalb</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">DeKalb, IL</span></div>
</div>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>" Demonstrated success in ensuring full compliance with contract terms and conditions through internal audit of <br>contractual agreements; proven ability to collaborate with key stakeholders to ensure contract obligations are <br>timely met. <br>" Identify possible performance discrepancies and loss revenue; communicate and manage risk with key <br>stakeholders to identify a solution. Major strength in planning and organizing detailed projects. Possess <br>strong deductive reasoning, analytical and problem-solving skills. <br>" Exceptional project leader and facilitator possessing refined communication and collaboration skills to <br>coordinate effectively with multiple internal and external teams, in order to advance projects and meet <br>overall strategic and business development goals, while ensuring consistent growth in competitive markets <br>and long-term prosperity. <br>" Focused planner and effective time manager; manage projects from concept to completion, including developing <br>concept, strategy, launch plan and metrics measurement; identify objectives, establish priorities and achieve <br>results under diverse and competitive market conditions. <br>" Principal strengths: organizational, leadership, oration, motivational and strategic planning.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F9a85011600af913d/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/43737bbbcdefb316?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>76618c3742f58e81ba1a5e09093080c6</uniq_id>
      <url>http://www.indeed.com/r/43737bbbcdefb316?hl=en&amp;co=US</url>
      <zipcode>60621</zipcode>
      <name>Executive Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Executive Assistant</h1>
<h2 id="headline" itemprop="jobTitle">Executive Assistant</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To obtain a position with a reputable organization that will provide an environment where I can develop my professional, leadership and communication skills thus providing  opportunities for growth</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeCHJI3Uynixy-cA9mm8_A" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Executive Assistant</p>
<div class="work_company"><span class="bold">Swish Dreams Sports and Educational Foundation</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2009 to August 2009</p>
<p class="work_description">* Assisted Swish Dreams staff and employees with research and data projects geared toward advancing the company brand and marketing. <br>* Assisted Student staff and board members in developing new ideas for students to participate in Swish Dreams Sports Camp.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeCHJI3Uynmxy-cA9mm8_A" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">King College Prep High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">September 2008 to Present</p>
</div></div>
<div id="education-EeCHJI3Uynqxy-cA9mm8_A" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization"><div class="edu_school"><span class="bold" itemprop="name">King College Prep Basketball Team</span></div></div></div>
<div id="education-EeCHJI3Uynyxy-cA9mm8_A" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">LEADERSHIP/EXTRACURRICULAR ACTIVITIES</p>
<div class="edu_school"><span class="bold" itemprop="name">King College Prep Junior</span></div>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F43737bbbcdefb316/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/1d14939df17567f3?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>898664a035e0ab893a9e7b9a4529320a</uniq_id>
      <url>http://www.indeed.com/r/1d14939df17567f3?hl=en&amp;co=US</url>
      <zipcode>23325</zipcode>
      <name>Kitchen Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Kitchen Manager</h1>
<h2 id="headline" itemprop="jobTitle">Kitchen Manager - Morale Welfare Recreation (MWR)</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chesapeake, VA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To secure a job as a cook and continue my commitment to provide excellent customer service and promote leadership while utilizing my 16 years of culinary skills and managerial experiences.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebA4q2sl-W-f87c3iCmnw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Kitchen Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Morale Welfare Recreation (MWR)</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Virginia Beach, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2014 to Present</p>
<p class="work_description">• Managed Kitchen staff by training, assigning, and delegating positions to insure efficient work flow <br>• Maintained a skilled kitchen staff <br>• Overseeing daily product inventory purchases and cost control <br>• Regularly interacted with guests <br>• Properly labeled and stored food in appropriate storage rooms and walk-in refrigerators <br>• Organize, prepare, set up, and cater for large events and golf tournaments</p>
</div></div>
<div id="workExperience-EebA4q2sl-S-f87c3iCmnw" class="work-experience-section "><div class="data_display">
<p class="work_title title">server</p>
<div class="work_company">
<span class="bold">Virginia Beach Funny Bone Comedy Club</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Virginia Beach, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2014 to November 2016</p>
<p class="work_description">Server <br>• Providing excellent customer service at all times <br>• Deliver exceptional, friendly, and fast service in a very fast pace atmosphere <br>• Proven ability to up sell alcohol, appetizers, entrees, and desserts to customers</p>
</div></div>
<div id="workExperience-EebA4q2sl-O-f87c3iCmnw" class="work-experience-section "><div class="data_display">
<p class="work_title title">General Manager</p>
<div class="work_company">
<span class="bold">Unlimited Imports</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chesapeake, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2012 to February 2014</p>
<p class="work_description">• Handle merchandising, inventory control, ordering and cash handling <br>• Direct sales to include customer concerns <br>• Input data to prepare daily reports and trends in sales <br>• Interviewed potential employees <br>• Trained and supervised new staff <br>• Oversight of store opening and closing <br>• Tally daily receipts and deposits <br>• Unpacking and stocking merchandise</p>
</div></div>
<div id="workExperience-EebA4q2sl-K-f87c3iCmnw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Server</p>
<div class="work_company">
<span class="bold">Roger Browns Sports Bar</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Portsmouth, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2007 to May 2012</p>
<p class="work_description">Server <br>• Process payments <br>• Greeting customers <br>• Setting tables and cleaning dining area <br>• Organizing large corporate banquet parties <br>• Train new servers on menu knowledge and steps to achieve quality customer service</p>
</div></div>
<div id="workExperience-EebA4q2sl-G-f87c3iCmnw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Smoker/Line Cook</p>
<div class="work_company">
<span class="bold">Smokey Bones</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chesapeake, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2006 to September 2007</p>
<p class="work_description">• Ensure preventative and general maintenance of smoker and other kitchen equipment <br>• Label food items appropriately <br>• Maintain proper food portion, follow recipes, and presentation set by restaurant <br>• Maintained a well-managed kitchen</p>
</div></div>
<div id="workExperience-EebA4q2sl-C-f87c3iCmnw" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Manager</p>
<div class="work_company">
<span class="bold">IHOP International House of Pancakes</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chesapeake, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2001 to April 2007</p>
<p class="work_description">• Oversee employee work to ensure compliance with company policies, regulations, and food safety guidelines <br>• Responsible for inventory <br>• Customer complaint resolution <br>• Produce monthly reports detailing sales <br>• Hiring and training <br>• Motivational leader <br> <br>Cook <br>• Prepared and served food promptly and courteously <br>• Operate fryers, grills, ovens <br>• Carve meats and prepare vegetables <br>• Maintained clean and sanitary work area at all times <br>Server <br>• Outstanding customer service skills <br>• Fulfilled customer needs with timely and accurate service <br>• Provide ultimate customer experience to multicultural populations</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebA4q2sl-e-f87c3iCmnw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High School Diploma</p>
<div class="edu_school">
<span class="bold" itemprop="name">Lakeland High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Suffolk, VA</span></div>
</div>
<p class="edu_dates">2001</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F1d14939df17567f3/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/733930d1a5a533f1?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>1db5e450b8c87a75366d3450ec8f3b3a</uniq_id>
      <url>http://www.indeed.com/r/733930d1a5a533f1?hl=en&amp;co=US</url>
      <zipcode>60632</zipcode>
      <name>ILLINOIS PARALEGAL</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">ILLINOIS PARALEGAL</h1>
<h2 id="headline" itemprop="jobTitle">ILLINOIS PARALEGAL - JOHNSON, BLUMBERG, AND ASSOCIATES, CHICAG, IL</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To obtain and maintain a position to best utilize my years' experience in the legal field</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeYyZHxWLuGUKxJ-cpi6yQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">ILLINOIS PARALEGAL</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">JOHNSON, BLUMBERG, AND ASSOCIATES, CHICAG, IL</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 2014 to Present</p>
<p class="work_description">Provide exceptional customer service by meeting client's time efficiency needs without undermining the quality of the product; maintain good communication with people within the firm along with the firm's clients. Prepared and present judicial sale litigation matters including daily responsibilities. Assure that defendants are not active in the military. Drafted and reviewed documents to be filed in court and counties in a timely manner. Handle the drafting and filing of prejudgment packages. Review all documents before the preparing prejudgments to ensure that we are ready to schedule a judgment hearing. Assure that AOIs are sent for filing with their corresponding counties. Expert in using LPS Desktop along with other sites. Expertise in the foreclosure process.</p>
</div></div>
<div id="workExperience-EeYyZHxWVfKUKxJ-cpi6yQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">SERVER</p>
<div class="work_company">
<span class="bold">PIZANO'S PIZZA &amp; PASTA</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2013 to 2014</p>
<p class="work_description">Provide exceptional customer service by meeting customer's time efficiency needs without undermining the quality of the product, maintain work space hygiene and order over fellow team members. Waited tables five nights a week and provided great customer service.</p>
</div></div>
<div id="workExperience-EeYyZHxWVfOUKxJ-cpi6yQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">ASSIGNMENT CLERK COORDINATOR</p>
<div class="work_company">
<span class="bold">PIERCE AND ASSOCIATES</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2009 to 2014</p>
<p class="work_description">Provide exceptional customer service by meeting client's time efficiency needs without undermining the quality of the product; maintain good communication with people within the firm along with the firm's clients. Prepare and present judicial foreclosure litigation matters (foreclosures) including daily responsibilities for 500+ file caseload. Expertise in the beginning stages of a foreclosure. Assisted employees, as I was the LPS desktop expert in the department.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeYyZHxWfQSUKxJ-cpi6yQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">bachelor's</p>
<div class="edu_school"><span class="bold" itemprop="name">state university</span></div>
</div></div>
<div id="education-EeYyZHxWfQaUKxJ-cpi6yQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">associate's in Liberal Arts</p>
<div class="edu_school">
<span class="bold" itemprop="name">Chicago State University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">August 2015</p>
</div></div>
<div id="education-EeYyZHxWpBiUKxJ-cpi6yQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associates in Human Science</p>
<div class="edu_school">
<span class="bold" itemprop="name">Olive Harvey College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2015</p>
</div></div>
<div id="education-EeYyZHxWpBqUKxJ-cpi6yQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">College Preparatory in Advanced Placement Spanish</p>
<div class="edu_school">
<span class="bold" itemprop="name">Gwendolyn Brooks College Preparatory Academy</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">September 2005 to March 2009</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>SKILLS <br>Fluent in Spanish, Customer Service, Microsoft Word, Microsoft Excel, Microsoft, PowerPoint, Calculation, Telephone, Email, Serving</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F733930d1a5a533f1/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/821db55097ba107b?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>c36baa8ebe2a16dd5878f5251b2fac33</uniq_id>
      <url>http://www.indeed.com/r/821db55097ba107b?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Baby-sitter</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Baby-sitter</h1>
<h2 id="headline" itemprop="jobTitle">Baby-sitter - Self-Employed</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeXKs1oAZHy0XyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Baby-sitter</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Self-Employed</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2012 to Present</p>
<p class="work_description">Supervise and care for two children on the weekends.</p>
</div></div>
<div id="workExperience-EeXKs1oAZH20XyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">Groundskeeper</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Self-Employed</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2012 to Present</p>
<p class="work_description">Approach homeowners in my local neighborhood to offer my services. <br>Shovel snow during winter months; rake leaves during the fall. <br>Negotiate prices for size of lawn and the amount of snow/leaves to be removed.</p>
</div></div>
<div id="workExperience-EeXKs1oAZHu0XyLhBGsw1Q" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Counselor</p>
<div class="work_company"><span class="bold">After School Matters</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2015 to August 2015</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Skills Summary <br> <br>Leadership   Reliable   Flexible   Quick Learner   Coachable   Hard Worker   Team Player   Computer Skills   Negotiation</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F821db55097ba107b/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/6e31a2de3cb1cabe?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>32ea289e101fb653bd0583185f74e962</uniq_id>
      <url>http://www.indeed.com/r/6e31a2de3cb1cabe?hl=en&amp;co=US</url>
      <zipcode>60602</zipcode>
      <name>Team Associate</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Team Associate</h1>
<h2 id="headline" itemprop="jobTitle">Team Associate - Ikea</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Bolingbrook, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeI88ERiMuWqLZvrOz8UFQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Team Associate</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Ikea</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Bolingbrook, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2012 to Present</p>
<p class="work_description">Help plan and organize work spaces and orders for business and home offices <br>• Maintain professional engagement with customers <br>•  Ensure showroom is kept updated with pricing, correct product descriptions and inventory</p>
</div></div>
<div id="workExperience-EeI88ERiMuaqLZvrOz8UFQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Bartender</p>
<div class="work_company">
<span class="bold">Hollywood Blvd Theatres</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Woodridge, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2007 to May 2012</p>
<p class="work_description">Completed an Illinois liquor training class (B.A.S.S.E.T.) <br>•  Prepared all drink orders <br>• Maintain professional, friendly work place while keeping bar stocked, clean and organized during peak hours</p>
</div></div>
<div id="workExperience-EeI88ERiMueqLZvrOz8UFQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Server-Head Server</p>
<div class="work_company">
<span class="bold">Hollywood Blvd Theatres</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Woodridge, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2004 to October 2007</p>
<p class="work_description">Promoted to Head Server in June 2005 <br>• Received Most Important Leadership Award <br>• Selected to attend an eight-week management class <br>• Developed server training program and prepared weekly work schedule <br>• Supervised and evaluated work performance including disciplinary action <br>•  Assisted with hiring, guest complaints and server comps or voids</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeI88ERiMumqLZvrOz8UFQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Interior Design</p>
<div class="edu_school">
<span class="bold" itemprop="name">College Of Dupage Downers Grove</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Downers Grove, IL</span></div>
</div>
<p class="edu_dates">1995 to 1998</p>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Experience with some Auto C.A.D. able to read drawings and plans i.e. Architectual and Interior Design plans.</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F6e31a2de3cb1cabe/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/58d9dfb15a44462b?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>ceb935a2cba29edba0fc96c7f05fd75b</uniq_id>
      <url>http://www.indeed.com/r/58d9dfb15a44462b?hl=en&amp;co=US</url>
      <zipcode>60610</zipcode>
      <name>Lead Family Services Specialist</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Lead Family Services Specialist</h1>
<h2 id="headline" itemprop="jobTitle">Lead Family Services Specialist - New Moms, Inc</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Motivated professional with family services, administrative support and customer service experience is seeking entry level criminal justice position.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeYWTNaqnkarXAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Lead Family Services Specialist</p>
<div class="work_company"><span class="bold">New Moms, Inc</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2015 to May 2016</p>
<p class="work_description">• Conducts supervision and shadows and assists home visits with family services specialists. <br>• Implements the PAT quality assurance blueprint. <br>• Monthly data reviews for completion and alignment with goal benchmarks. <br>• Monthly data accuracy checks and file reviews. <br>• Assists new staff members with coaching and support. <br>• Create and implement trainings for new staff members.</p>
</div></div>
<div id="workExperience-EeYWTNaqnkerXAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Family Services Specialist</p>
<div class="work_company"><span class="bold">New Moms, Inc</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2012 to August 2015</p>
<p class="work_description">• Maintained a case load of up to 20 participants and their children. <br>• Conducted thorough assessments of participant families to determine their needs, and develop written service plans for each case within thirty days of intake. <br>• Created and maintained case files, and recorded data in appropriate information systems. <br>• Conducted home based visits with participants to observe approved age appropriate activities intended to enhance the parent/child relationship. Discuss needs, referrals and goal progression during the visits. <br>• Coordinated discharge planning, resources and referrals. Assisted with weekly parent support groups.</p>
</div></div>
<div id="workExperience-EeYWTNaqnkirXAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Non-Residential Program Aide</p>
<div class="work_company"><span class="bold">New Moms, Inc</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2010 to March 2012</p>
<p class="work_description">• Assisted with recruitment, intake enrollment, and the maintenance of participant records for the non-residential program. <br>• Assisted with parent support groups plans, provided needed equipment, and assisted in the recruitment of support group members. <br>• Planned and implemented client incentive programs, and monitored inventory reporting needs for the supervisor. <br>• Shopped for groceries and assisted with meal preparation. <br>• Coordinated incoming/outgoing donations, and administrated emergency services program.</p>
</div></div>
<div id="workExperience-EeYWTNaqnkmrXAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">New Kids Aide</p>
<div class="work_company"><span class="bold">New Moms, Inc</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2008 to April 2010</p>
<p class="work_description">• Ensured that children's basic needs were met while in the classroom. <br>• Assisted children with projects and assignments to reinforce material being presented in class. <br>• Obtained and set up needed equipment, materials and supplies for assignments. Organized classroom. <br>• Performed a variety of administrative tasks including answering telephone calls, photocopying and filing. <br>• Coordinated incoming/outgoing donations.</p>
</div></div>
<div id="workExperience-EeYWTNaqxVqrXAxatb0mLA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Internship</p>
<div class="work_company">
<span class="bold">McCormick Tribune YMCA</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2006 to February 2007</p>
<p class="work_description">• Assisted the Program Director. <br>• Created and posted flyers for upcoming events and classes. <br>• Created and organized members' hardcopy and electronic files.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeYWTNaqxVyrXAxatb0mLA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in Criminal Justice</p>
<div class="edu_school"><span class="bold" itemprop="name">Kaplan University</span></div>
<p class="edu_dates">November 2015</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>COMPUTER SKILLS <br>• Proficient in Microsoft Word, PowerPoint and Excel</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F58d9dfb15a44462b/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ca42b90aab249f41?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>e78b9ffff97f27e78f5bdebe841cdaf7</uniq_id>
      <url>http://www.indeed.com/r/ca42b90aab249f41?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Part-time Instructor</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Part-time Instructor</h1>
<h2 id="headline" itemprop="jobTitle">Part-time Instructor - DuPage Training Academy</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Carol Stream, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Dedicated college graduate seeking a position with a dynamic sports organization. Goals: to advance my knowledge, gain professional experience and add value to the organization</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeYBIREmTOuEBAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Part-time Instructor</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">DuPage Training Academy</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2009 to Present</p>
<p class="work_description">Instructed participants in basketball -ran instructional camps <br>◦ Assistant Coach for DTA Travel Basketball High School Team <br>◦ Worked tournaments and worked with umpires and coaches</p>
</div></div>
<div id="workExperience-EeYBIREmJdmEBAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Internship</p>
<div class="work_company"><span class="bold">Champaign Park District</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2016</p>
<p class="work_description">Marketing of Programs <br>◦ In charge of obtaining sponsorships <br>◦ Representing at career fairs <br>◦ Operations and office work <br>◦ Work with all age groups in different programs</p>
</div></div>
<div id="workExperience-EeYBIREmTOqEBAxatb0mLA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Internship</p>
<div class="work_company"><span class="bold">DB Sterlin Consultants, INC</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2015 to August 2015</p>
<p class="work_description">2015 Summer) <br>◦ Marketed companies services <br>◦ General office work - answered phones, filing, <br>◦ Worked with various software applications/Excel spreadsheet work on contracts</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeYBIREmTO2EBAxatb0mLA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Sports Management</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Illinois</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Champaign, IL</span></div>
</div>
<p class="edu_dates">2016</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Motivated University of Illinois Student of Recreation, Sport and Tourism with proven success in supporting organized sports activities <br>◦ Two years' experience playing collegiate men's basketball at a Division III level with both academic and all conference mention <br>◦ Practical background in coaching, basketball instruction and building relations with teams and officials <br>◦ Promotional Volunteer for Ronald McDonald Playhouse Foundation <br>◦ Worked two jobs simultaneously to help pay for school and maintained positions of responsibility with both positions <br>◦ Dedicated, adaptable and energetic with a strong work ethic</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fca42b90aab249f41/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ba37f1e7f7deba2a?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>cc17e14f0c9387d06bf970cbb7d65813</uniq_id>
      <url>http://www.indeed.com/r/ba37f1e7f7deba2a?hl=en&amp;co=US</url>
      <zipcode>23324</zipcode>
      <name>Shift Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Shift Manager</h1>
<h2 id="headline" itemprop="jobTitle">Customer Based</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Hampton, VA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Passion to obatin job in medical field.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeY1F7pA_a2T9eIbfZYckw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Shift Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">McDonald's</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Hampton, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2015 to Present</p>
<p class="work_description">Assist in accomplishing daily targets, controlling food cost, paper cost, customer complaints, assist in scheduling,  assist in hiring process, anything with flow of business. Maintaing proper equipment, delivering  excellent customer service.</p>
</div></div>
<div id="workExperience-EeKx_RinrEyS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Cashier</p>
<div class="work_company">
<span class="bold">Burlington Coat Factory</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hampton, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2012 to May 2015</p>
<p class="work_description">Provide excellent  customer service while assisting in merchandise purchase using a cash register. Money handling and credit/debit transactions.  Assist in providing protection to merchandise as fitting room attendant.</p>
</div></div>
<div id="workExperience-EeMj3z6POr2AmrPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Personal Care Assistant</p>
<div class="work_company">
<span class="bold">Hope in Home</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Newport News, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2013 to May 2014</p>
<p class="work_description">Assist elderly and disabled individuals in daily living activities such as bathing, getting dress, eating, shopping, medication reminders.</p>
</div></div>
<div id="workExperience-EeKx_coQsUmS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Cashier</p>
<div class="work_company">
<span class="bold">Kentucky Fried Chicken</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hampton, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2007 to August 2012</p>
<p class="work_description">Providing excellent customer service while assisting customer in ordering process. Processing cash credit/debit transactions,  food preparation, maintaing an orgainzied  stocked area, assist in leading team to accomplish daily goals.</p>
</div></div>
<div id="workExperience-EeKx_-EhxnGS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Medical Assistant</p>
<div class="work_company">
<span class="bold">Dr. Chinnery</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hampton, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2010 to August 2010</p>
<p class="work_description">Assist docotor with obtaining patients vitals, answering telephone to help in patient questions, refferals, bookkeeping filing maintaing an orgainzied office, scheduling appointments.</p>
</div></div>
<div id="workExperience-EeKx_ipL2zSx17PItWPLJQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Dental Assistant</p>
<div class="work_company">
<span class="bold">Dr. Gallop and Richardson</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hampton, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2008 to June 2008</p>
<p class="work_description">Sterilize and disinfect equipment and instruments, update patient's dental records, suctioning the patient's mouth, preparing materials for dental impressions and restorations, applying topical anesthetics, polishing teeth, making stone and plaster teeth and mouth casts, fabricating temporary crowns bridges and mouth guards, and exposing and developing X-rays.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeY1F0xGY1-T9eIbfZYckw" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certificate in Armed Security</p>
<div class="edu_school">
<span class="bold" itemprop="name">Alliance Security Academy</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Hampton, VA</span></div>
</div>
<p class="edu_dates">2013 to 2013</p>
</div></div>
<div id="education-EeKx_naCz4Cx17PItWPLJQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certification in Dental Assistant</p>
<div class="edu_school">
<span class="bold" itemprop="name">New Horizon Technical Center</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Hampton, VA</span></div>
</div>
<p class="edu_dates">2007 to 2012</p>
</div></div>
<div id="education-EeKx_lFkGCWx17PItWPLJQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certificate in Medical Assistant</p>
<div class="edu_school">
<span class="bold" itemprop="name">Everest</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Newport News, VA</span></div>
</div>
<p class="edu_dates">2009 to 2010</p>
</div></div>
<div id="education-EeKx_fuAfEmS1L9oLXgqwg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High School Diploma in General Studies</p>
<div class="edu_school">
<span class="bold" itemprop="name">Phoebus High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Hampton, VA</span></div>
</div>
<p class="edu_dates">2004 to 2008</p>
</div></div>
<div id="education-EebAze1M8T2-Fc7c3iCmnw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certification in Serv Safe</p>
<div class="edu_school"><div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Hampton, VA</span></div></div>
<p class="edu_dates">September 2015 </p>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fba37f1e7f7deba2a/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/941fef31bc0f7960?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>6384bdd758b9050ef2f38e63cd16322f</uniq_id>
      <url>http://www.indeed.com/r/941fef31bc0f7960?hl=en&amp;co=US</url>
      <zipcode>60626</zipcode>
      <name>management</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">management</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeQXpV0a1l6eTSpAlk2Pyg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">management</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">7eleven</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2011 to Present</p>
<p class="work_description">Responsibilities <br>Register operating,cooler fill up,store clean,cdc fill up,maclane fill up.ordering.food right offs.expired food take off &amp; others. <br> <br>Accomplishments <br>I cant explain it. <br> <br>Skills Used <br>I can be a woner</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeQXohlwel6iPjFyFaRtIQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">bs in bangla,english,higher math,general math,geography,social science,statistic,phychology,biology,chemistry,physics.</p>
<div class="edu_school">
<span class="bold" itemprop="name">kpbhs</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dhaka</span></div>
</div>
<p class="edu_dates">1989 to 2000</p>
</div></div>
<div id="education-EeQXohlwelyiPjFyFaRtIQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization"><div class="edu_school">
<span class="bold" itemprop="name">KPB School &amp; College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dhaka</span></div>
</div></div></div>
<div id="education-EeQXohlwoXCiPjFyFaRtIQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization"><div class="edu_school">
<span class="bold" itemprop="name">kadomtola high school &amp; college</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dhaka</span></div>
</div></div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Speaks: English, Hindi &amp; bangla</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F941fef31bc0f7960/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/3f42d5963143c28a?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>e79071addd266d91cac7b596ee3dd336</uniq_id>
      <url>http://www.indeed.com/r/3f42d5963143c28a?hl=en&amp;co=US</url>
      <zipcode>60611</zipcode>
      <name>Patient Care Support Department Administrative Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Patient Care Support Department Administrative Assistant</h1>
<h2 id="headline" itemprop="jobTitle">Patient Care Support Department Administrative Assistant - Northwest Community Hospital</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Roselle, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">• Administrative Assistant is experienced in Microsoft Word, Excel, Access, PowerPoint, Publisher, and Outlook. <br>• Experience includes scheduling meetings and conference calls, coordinating travel arrangements, providing phone coverage, creating and editing Word and Excel reports, updating databases, and scanning, emailing, and faxing documents.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeRL2fjLWMGbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Patient Care Support Department Administrative Assistant</p>
<div class="work_company"><span class="bold">Northwest Community Hospital</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2013 to January 2015</p>
<p class="work_description">Schedule meetings, order catering, create Excel and Word reports, create banners in Publisher, order and inventory supplies, create department newsletter, provide phone coverage, document meeting minutes, scan, fax and email documents, update director's calendar, and update team intranet webpages.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMObwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Advanced Resources Staffing Agency –  Administrative Assistant</p>
<div class="work_company">
<span class="bold">American Academy of Pediatrics</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elk Grove Village, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2013 to October 2013</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, entered medical student applications for membership into NetForum database and processed membership payments, emailed applicants for additional information, processed batch payments, scanned documents and filed electronically, ran reports to create membership invoices, updated membership information and monitored email to process members' requests.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMabwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Client Asset Manager</p>
<div class="work_company">
<span class="bold">Xerox, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Rosemont, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2013 to July 2013</p>
<p class="work_description">Working as a contractor for Advantage Staffing, managed client print services accounts handling supply orders, tracking progress for new accounts going live, opened tickets with the helpdesk for product repairs and maintenance and tracked to resolution, resolved meter reading issues for accurate billing, and created Excel spreadsheets to track client devices on and off the network for supply and repair coverage.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMKbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company">
<span class="bold">Northwest Community Hospital</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Arlington Heights, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2013 to May 2013</p>
<p class="work_description">May 2013 - May 2013 <br>Spiritual Care Services <br>Working as a contractor for Advanced Resources Staffing, provided phone coverage for chaplains on call and Spiritual Care manager, created banners in MS Publisher, ordered supplies, responded to email requests, and maintained manual files.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMebwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Property Assistant/Receptionist</p>
<div class="work_company">
<span class="bold">Willow Crossing Apartments</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elk Grove Village, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">April 2013 to April 2013</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, answered phones obtaining prospect information and directing calls to the appropriate person, entering resident and guest information into Yardi database software, processes invoices, assists with SODA preparation, creates work orders, assembles resident files, enters utility invoices into Excel spreadsheet, inspects apartments for move-in readiness, and responses to email inquiries.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMibwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Renewal Specialist</p>
<div class="work_company">
<span class="bold">Wheels, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Des Plaines, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2013 to March 2013</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, provided assistance to Compliance Services and Driver Fulfillment Center staff by matching letters to state vehicle stickers, stuffing envelopes, entering vehicle information on state websites for sticker renewal, pulling vehicle titles and matching with files, and updating sticker expiration information in database.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMSbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company">
<span class="bold">American Academy of Pediatrics</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elk Grove Village, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2013 to March 2013</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, provided administrative support to division staff, scheduled conference calls, coordinated meeting arrangements, group dinners, guest hotel and airport transportation, created Adobe PDF portfolios/agenda books, processed check requests, created surveys in Survey Monkey, scanned documents, provided phone coverage, and created Excel spreadsheets.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMmbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Data Entry Clerk</p>
<div class="work_company">
<span class="bold">Wheels, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Des Plaines, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2012 to December 2012</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, entered the company's Truck fleet products and services into a Microsoft Access database and into an intranet environment to create the company's first online catalog of custom truck upfitting options. Duties also included using copy and paste to enter parts and services listed in quotes received from service providers and suppliers for Wheels, Inc. and assigning catalog numbers to each part, tracking each number assigned in an Excel spreadsheet and creating packages of parts and services.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMWbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Executive Administrative Assistant</p>
<div class="work_company">
<span class="bold">American Academy of Pediatrics</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Elk Grove Village, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2012 to October 2012</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, managed calendar for Associate Executive Director as well as coordinated travel arrangements, scanned documents, provided phone coverage, created Excel spreadsheets, provided administrative support to division staff, and distributed mail.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMqbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Data Entry Clerk</p>
<div class="work_company">
<span class="bold">Wheels, Inc</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Des Plaines, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2012 to August 2012</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, entering the company's Truck fleet products and services into a Microsoft Access database and into an internet application to create the company's first online catalog. Duties included using copy and paste to enter parts and services listed in quotes received from installation companies and suppliers for Wheels, Inc. and assigning catalog numbers to each part, tracking each number assigned in an Excel spreadsheet and creating packages of parts and services.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMubwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Marketing Assistant</p>
<div class="work_company">
<span class="bold">Emirates Airlines</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Rosemont, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2012 to June 2012</p>
<p class="work_description">Working as a contractor for Kelly Services, reated prospective client files for sales team by gathering company information from internet searches, airline travel numbers from database, and creating annual cumulative reports and pivot tables from Excel spreadsheets. Also created new client files in Access database and updated existing files with point of sale and travel agency relationship information.</p>
</div></div>
<div id="workExperience-EeRL2fjLWMybwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company"><span class="bold">Pepsico</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2009 to May 2012</p>
<p class="work_description">Maintained project databases, performed searches in Spec 2000 (a Citrix Xen application) to locate and edit ingredient specifications against Product Vision 5.0 ingredient descriptions; managed project to burn CDs of ingredient specifications and performed MS Word mail merge to mail CDs to suppliers; maintained Access database for offsite storage and laboratory notebooks issued to researchers in Records Management Services; submitted work orders for building repairs, reconciled invoices from vendors, scanned documents, electronically filed, edited, and updated documents in Documentum, Spec 2000, and Product Vision 5.0, and performed follow-up phone calls and emails.</p>
</div></div>
<div id="workExperience-EeRL2fjLWM2bwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company">
<span class="bold">Pepsico</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Barrington, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2011 to December 2011</p>
<p class="work_description">Working as a contractor for Advanced Resources Staffing, created Excel spreadsheets and file system to track grant applications and evaluations; scheduled conference calls, made travel preparations, scheduled meetings with catering and technical setup requirements, solicited and followed-up with institutions for program updates, shipped large packages to meeting destinations, created check requests, filled supply requested, and performed data entry in an Excel spreadsheet.</p>
</div></div>
<div id="workExperience-EeRL2fjLWM6bwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Lead Qualification Representative</p>
<div class="work_company">
<span class="bold">CDW</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Vernon Hills, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2009 to July 2009</p>
<p class="work_description">Working as a contractor for Kelly Services, qualified sales lead with existing and new clients to initiate sales calls for CDW account managers. Made outbound calls, processed customer orders, forwarded sales leads with client's contact information and product interests for account managers and sales managers.</p>
</div></div>
<div id="workExperience-EeRL2fjLf9-bwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Qualification Center Representative</p>
<div class="work_company">
<span class="bold">DeVry Online University</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Addison, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2009 to April 2009</p>
<p class="work_description">Working as a contractor for Kelly Services in a college environment, directed inbound student calls to appropriate department; and initiated outbound calls to prospective students directing interested individuals to admissions advisors for enrollment. Used Oracle database to locate student information in directing student calls to the appropriate department or advisor; provided a daily report of outbound calls.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-CbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Call Center Representative</p>
<div class="work_company">
<span class="bold">Allstate Insurance</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Barrington, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2008 to December 2008</p>
<p class="work_description">Working as a contractor for Kelly Services, provided one-on-one customer service and solutions for homeowners in catastrophe situations; provided follow-up on issues to create customer satisfaction; acting as a single-point-of-contact between the client, contractor, and adjuster to resolve issues for a win-win resolution.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-KbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Project Coordinator/Senior Associate SAN Operations Team</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2006 to January 2008</p>
<p class="work_description">Coordinated the efforts of the SAN Ops team for onsite installation of SAN storage hardware and software enterprise wide. Facilitated project meetings, and provided status reports.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-ObwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Sr. Associate</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2003 to January 2008</p>
<p class="work_description">Performed space allocations for storage space in SAN environment (EMC) and zoned Brocade switches. Wrote SOP (standard operating procedure) documents for SAN Operations team for training and knowledge transfer purposes. Monitored Legato tape library to resolve tape issues.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-GbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">1993 to 2008</p>
</div></div>
<div id="workExperience-EeRL2fjLf-SbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Unix Team Leasing Coordinator</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2002 to October 2003</p>
<p class="work_description">Tracked lease and purchase agreements for IT servers replacing outdated servers with new leased or purchased servers.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-WbwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Disaster Recovery Team Associate</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2000 to December 2001</p>
<p class="work_description">Reviewed backup recovery plans of IT teams for effectiveness and accuracy in an emergency backups situation.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-abwRkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Change Management Team Associate</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 1994 to December 1999</p>
<p class="work_description">Performed testing of mainframe programs for validity and accuracy moving successful programs from the test environment through the Endevor life cycle to the production environment.</p>
</div></div>
<div id="workExperience-EeRL2fjLf-ebwRkqK6c2tA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Administrative Assistant</p>
<div class="work_company"><span class="bold">Discover Financial Services</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 1993 to September 1994</p>
<p class="work_description">Provided phone coverage, travel arrangements, tracked staff attendance, and scheduled IT quarterly meetings.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeRL2fjLf-mbwRkqK6c2tA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts in Business</p>
<div class="edu_school">
<span class="bold" itemprop="name">Trinity International University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Deerfield, IL</span></div>
</div>
</div></div>
<div id="education-EeRL2fjLf-ubwRkqK6c2tA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associate in Technical Writing</p>
<div class="edu_school">
<span class="bold" itemprop="name">College of Lake County</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Grayslake, IL</span></div>
</div>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Technical Skills: <br>Microsoft Word, Excel, Access, PowerPoint, Outlook, and Publisher.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F3f42d5963143c28a/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/d7d83e14cfa41ca7?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>fd00011d46bc53680a5d83abeccf71f6</uniq_id>
      <url>http://www.indeed.com/r/d7d83e14cfa41ca7?hl=en&amp;co=US</url>
      <zipcode>23324</zipcode>
      <name>Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Manager</h1>
<h2 id="headline" itemprop="jobTitle">CEO - Dr. D. Keith Simmons</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Portsmouth, VA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebAz4rieVyw8R6VyWpLnA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Dr. D. Keith Simmons</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chesapeake, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2003 to Present</p>
<p class="work_description">Manage daily operations of the Dental office. Coordinate treatment plans with patients. Discuss and make <br>financial arrangements for treatment. Front desk coordinator, verifying insurance and scheduling <br>appointments. Perform collection activities on delinquent accounts, make bank deposits, run daily and monthly reports. Re branded the office with an official company logo and marketing materials. Increased <br>practice income, lowered office out going expenses. Implemented systems that brings more organization and accountability to daily operations</p>
</div></div>
<div id="workExperience-EebAz4rioG6w8R6VyWpLnA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Assistant Manager</p>
<div class="work_company">
<span class="bold">Wendys</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Portsmouth, VA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2000 to December 2007</p>
<p class="work_description">Manage daily store operations, customer service, train employees, deposits, ordering supplies/food, <br>maintain store cleanliness</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EebAz4rioG-w8R6VyWpLnA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associate in Arts in marketing</p>
<div class="edu_school">
<span class="bold" itemprop="name">Strayer University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chesapeake, VA</span></div>
</div>
<p class="edu_dates">2013 to 2015</p>
</div></div>
<div id="education-EebAz4rix4Gw8R6VyWpLnA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Liberal Arts</p>
<div class="edu_school">
<span class="bold" itemprop="name">Brooklyn College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Brooklyn, NY</span></div>
</div>
<p class="edu_dates">1987 to 1989</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Management (10+ years)</span>, <span class="skill-text">marketing (10+ years)</span>, <span class="skill-text">Customer Service (10+ years)</span>, <span class="skill-text">Event Planning (10+ years)</span>, <span class="skill-text">Receptionist (10+ years)</span>
</div></div></div>
</div>
<div class="section-item military-content">
<div><div class="section_title">
<div id="add_military"></div>
<h2>Military Service</h2>
</div></div>
<div id="military-items" class="items-container"><div id="military-EebA0lpgYmKdow2SBVVUiQ" class="military-section last"><div class="data_display">
<p class="military_country"><span class="bold">Service Country:</span> United States</p>
<p class="military_branch"><span class="bold">Branch:</span> Army</p>
<p class="military_rank"><span class="bold">Rank:</span> e-4</p>
<p class="military_date">October 1990 to October 1993</p>
<p class="military_description">trained in the army as a dental assistant and expanded functions dental assistant</p>
<p class="military_commendations_title">Commendations:</p>
<p class="military_commendations">Army Commendation Medal</p>
</div></div></div>
</div>
<div class="section-item certification-content">
<div><div class="section_title"><h2>Certifications</h2></div></div>
<div id="certification-items" class="items-container"><div id="certification-EebA0HJLC3K6Lhs0yVzk5Q" class="certification-section last"><div class="data_display">
<p class="certification_title">CPR</p>
<p class="certification_date">September 2018</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Skills <br>Management, marketing, promotions, accounts receivable, collections, customer service, social media, <br>Microsoft word, fax, copy, type 35 wpm.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fd7d83e14cfa41ca7/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ca20e42533cf6b48?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>361b91a64d88ffecbbd1053de2b71b10</uniq_id>
      <url>http://www.indeed.com/r/ca20e42533cf6b48?hl=en&amp;co=US</url>
      <zipcode>19017</zipcode>
      <name>Server/Waiter</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Server/Waiter</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Woodbury Heights, NJ</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebAvuiUvY2n1Q2SBVVUiQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Server/Waiter</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Capn Cats Clam Bar</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2016 to Present</p>
</div></div>
<div id="workExperience-EebAvtJ7Fn6n1Q2SBVVUiQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Gym assistant</p>
<div class="work_company"><span class="bold">Riverwinds community center</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2015 to January 2016</p>
</div></div>
<div id="workExperience-EebAvqmD38mn1Q2SBVVUiQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">USPS</p>
<div class="work_company"><span class="bold">Bellmawr post office</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2014 to July 2015</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebAvoO1OT6mys7c3iCmnw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Associate</p>
<div class="edu_school"><span class="bold" itemprop="name">Gloucester County College</span></div>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Studying at RCGC hoping to continue at Camden County College for dental hygienist program.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fca20e42533cf6b48/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/e66093504a3ada94?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>af24fdc3d246ff790a48c3d691af7471</uniq_id>
      <url>http://www.indeed.com/r/e66093504a3ada94?hl=en&amp;co=US</url>
      <zipcode>60632</zipcode>
      <name>Teachers Assitant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Teachers Assitant</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Burnham, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeZGqqZ-73qeb1lI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Teachers Assitant</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Kingdom Kids Daycare</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dolton, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2015 to Present</p>
</div></div>
<div id="workExperience-EeZGqlyKIhCeb1lI6yVEOA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Unloader</p>
<div class="work_company">
<span class="bold">FedEx Ground</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Bedford Park, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2015 to March 2016</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeZGqbinLjKeb1lI6yVEOA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Early Childhood Education</p>
<div class="edu_school">
<span class="bold" itemprop="name">South Suburban College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">South Holland, IL</span></div>
</div>
<p class="edu_dates">2012 to 2016</p>
</div></div>
<div id="education-EeZGqcjEslieb1lI6yVEOA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High school or equivalent</p>
<div class="edu_school">
<span class="bold" itemprop="name">Thornridge High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dolton, IL</span></div>
</div>
<p class="edu_dates">2008 to 2012</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Word</span>, <span class="skill-text">Excel</span>, <span class="skill-text">Powerpoint</span>, <span class="skill-text">Cooking</span>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fe66093504a3ada94/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/a8e9290bb0a4ff4b?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>e0cce891720d0a1f1ece536686e9dec1</uniq_id>
      <url>http://www.indeed.com/r/a8e9290bb0a4ff4b?hl=en&amp;co=US</url>
      <zipcode>60626</zipcode>
      <name>warehousing</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">warehousing</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Waukegan, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EeR6nGqSMTKXVuIbfZYckw" class="work-experience-section last"><div class="data_display">
<p class="work_title title">warehousing</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">lil ladys food</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Gurnee, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2013 to Present</p>
<p class="work_description">Responsibilities <br>Prepare the line for out going shipments.prepare for recieving .making sure the orders were filled right before they went out. Checking the products for cuts.or damage. <br> <br>Accomplishments <br>Kept the working community focused and full of energy. Made sure jobs were done and made sire the line was moving properly. <br> <br>Skills Used <br>Focuse,attention, preparation, time,team management.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeR6nPATSUObwRkqK6c2tA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">GED</p>
<div class="edu_school">
<span class="bold" itemprop="name">waukegan high school</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Waukegan, IL</span></div>
</div>
<p class="edu_dates">1999 to 2004</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>I'm a very good worker,a fast pace learner,get along good with other's. I'm very good with my hands very passionate about my jobs I take learning new things very serious I'm very positive.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fa8e9290bb0a4ff4b/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/4e8d141e30cd4db5?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>9384f511b53a9ddca81874e25625b5ab</uniq_id>
      <url>http://www.indeed.com/r/4e8d141e30cd4db5?hl=en&amp;co=US</url>
      <zipcode>60611</zipcode>
      <name>Marketing &amp; Insights Analyst</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Marketing &amp; Insights Analyst</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeUJibf2J6uCnVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Marketing &amp; Insights Analyst</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Everspring Partners</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Evanston, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2015 to Present</p>
<p class="work_description">Responsibilities <br>Higher education consultancy that provides full-service, customized online-education solutions <br>• Conduct market research to investigate viability of programs with our existing university partners as well as new clients/programs <br>• Pull and analyze student and industry data and deliver insights in support of management, sales and marketing priorities <br>• Conduct strong data analysis and think creatively around consumer data to generate actionable recommendations <br>• Create digital content geared towards online brand-building for universities <br>• Assist Design team with creative/design work, writing and managing university social media assets, syndicating content via social networks, assisting on digital production products, and editorial development</p>
</div></div>
<div id="workExperience-EeUKBMO3EzeNAwxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Part-time Freelance Web Designer</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Self-employed</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2014 to Present</p>
<p class="work_description">Responsibilities <br>• Designed and built websites for small businesses using Squarespace or Wordpress content management systems <br>• Conducted thorough market research to understand best practices in field <br>• Worked with business owners to understand their vision. Created user-friendly websites based on their expectations, taking into account user demographics and SEO <br>• Created graphics and took photographs as necessary. Created written content for all webpages</p>
</div></div>
<div id="workExperience-EeUKBPuPpFuCnVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">University Relations</p>
<div class="work_company">
<span class="bold">Everspring Partners</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Evanston, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2014 to February 2015</p>
<p class="work_description">Responsibilities <br>• Wrote, edited and designed marketing materials for university clients. These included customized e-mail communications, program and landing pages, advertisements and all other literature required of individual programs <br>• Conducted in-depth research and analyses of market trends, best practices and competition. Performed thorough analysis of data using Tableau software. Used data conclusions to create effective market strategies <br>• Built presentations for clients, and ensured organization and meeting of deadlines</p>
</div></div>
<div id="workExperience-EeNz7QvG-9iEnr9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Copywriting Intern</p>
<div class="work_company">
<span class="bold">mealsharing.com</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2013 to February 2014</p>
<p class="work_description">Tech start-up that aims to bring its members the ability to share a home-cooked meal anywhere in the world <br>• In charge of creating copy for mealsharing.com website, e-mail blasts with site members and official <br>correspondence with media and new partners. <br>• Responsible for editing and re-vamping website copy.</p>
</div></div>
<div id="workExperience-EeNz7QvHIu-Enr9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Head of Marketing &amp; Advertising</p>
<div class="work_company"><span class="bold">IshkBibel, Janabiyah, Bahrain</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2008 to December 2013</p>
<p class="work_description">Independent boutique specializing in Indian haute couture and home decor <br>• Designed the masthead, logo and brand name. Created all means of advertising including printed and online <br>media. <br>• Organized promotional events and exhibitions. Increased IshkBibel's exposure by communicating with local <br>businesses.</p>
</div></div>
<div id="workExperience-EeNz7QvHIuqEnr9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Program Operations Intern</p>
<div class="work_company">
<span class="bold">A Curated Wall</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2013 to August 2013</p>
<p class="work_description">Start-up gallery and art production company, specializing in customizable works <br>• In charge of setting up comprehensive library of artwork, as well as providing background information (legal, <br>source, materials, pricing, etc.) for each piece. This project was designed and carried out independently. <br>• Researched extensively and helped acquire pieces from a variety of sources. <br>• Responsible for writing descriptions for various art pieces and collections.</p>
</div></div>
<div id="workExperience-EeNz7QvHIuuEnr9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Marketing &amp; Promotion Intern</p>
<div class="work_company">
<span class="bold">Concrete Music Group LLC</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2012 to August 2012</p>
<p class="work_description">Fledgling independent music label focusing on local artists; goal of branching into myriad genres of world music <br>• In charge of creating content for website and social media pages. This included taking initiative to regularly <br>update all media with information about the label. <br>• Communicated with artists on a regular basis to better gauge their ideas for promotion.</p>
</div></div>
<div id="workExperience-EeNz7QvHIuyEnr9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Marketing &amp; Advertising Intern</p>
<div class="work_company">
<span class="bold">Marsh &amp; McLennan</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Dubai</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2011 to August 2011</p>
<p class="work_description">Insurance brokerage firm that covers projects in multiple industries, including construction and energy <br>• Created new materials as a part of company-wide rebranding. Generated materials from scratch about <br>Marsh's six Middle East offices, all of which had little to no prior marketing. <br>• Designed materials to present the six Middle East offices as a cohesive whole, while maintaining their <br>individual region- and industry-specific strengths and qualities. <br>• Received company-wide award for best rebranding within Marsh &amp; McLennan.</p>
</div></div>
<div id="workExperience-EeNz7QvHIu6Enr9oLXgqwg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Program Operations Intern</p>
<div class="work_company">
<span class="bold">QED Group, LLC</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Washington, DC</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2010 to August 2010</p>
<p class="work_description">Public-interest oriented consulting firm focused on public, private, non-profit, and international clients <br>• Worked on two USAID projects, "Knowledge-Driven Microenterprise Development" and "Global Health <br>Technology." <br>• Used media to create effective explanation of USAID's knowledge-driven strategy. <br>• Researched and wrote content for USAID's "Microlinks" website. <br>• Created extensive financial reports and performed needed administrative tasks to bring in new health-related <br>projects.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeNz7QvHIvGEnr9oLXgqwg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Arts in English</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Chicago - The College of Arts and Sciences</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">June 2013</p>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Illustrator, Photoshop, UX Design, Photography, Copywriting, Copy Editing, MS Office Suite, Tableau, HTML, Hindi, CSS</span></div></div></div>
</div>
<div class="section-item links-content">
<div><div class="section_title"><h2>Links</h2></div></div>
<div id="link-items" class="items-container"><div id="link-EeUUWYsBIbWgSFlI6yVEOA" class="link-section last"><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=http%3A%2F%2Fpreankanarula.squarespace.com&amp;h=cfdd4c34">http://preankanarula.squarespace.com</a></p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F4e8d141e30cd4db5/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/5c6801f8a27d5d14?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>246ab04f696dae8f4ec066f9463f6117</uniq_id>
      <url>http://www.indeed.com/r/5c6801f8a27d5d14?hl=en&amp;co=US</url>
      <zipcode>60657</zipcode>
      <name>Interactive Marketing Intern</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Interactive Marketing Intern</h1>
<h2 id="headline" itemprop="jobTitle">Interactive Marketing Intern - Indiana Blood Center</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="relocation_status">Willing to relocate</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeXleqUKiU-gLVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Interactive Marketing Intern</p>
<div class="work_company">
<span class="bold">Indiana Blood Center</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Indianapolis, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2016 to April 2016</p>
<p class="work_description">• Strategically planned and created all content on all social media platforms, using Illustrator, InDesign, Photoshop, Canva, Excel editorial calendar, RSS Feeds, and Google Alerts. <br>• Wrote copy and controlled all social media platforms, managing upkeep, paid ad campaigns and content creation.  <br>• Measured monthly social media analytics using analytical tools from Facebook, Twitter and Hootsuite.  <br>• Produced average organic Twitter engagement rate of 1.4% out of 9k+ followers and Facebook engagement rate of 6% out of 11k+ followers.  <br>• Launched new company blog raiseyoursleeve.org using Wordpress.  <br>• Compiled reports and analyzed all key email metrics, paid ad campaigns within social channels, website data and donor portal goal conversions using Google Analytics.  <br>• Lead marketing team in social media strategy and analytical reporting and made recommendations for organizational growth through strategic marketing communications.</p>
</div></div>
<div id="workExperience-EeXleqUKiVCgLVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Digital Marketing Intern</p>
<div class="work_company">
<span class="bold">Mr. Delivery</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Indianapolis, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2015 to December 2015</p>
<p class="work_description">• Managed all public relations matters regarding sponsorships, food sampling, and tabling events. <br>• Designed and implemented all Indianapolis advertising through Adobe Illustrator software that is used in social media campaigns, blogs, and distribution. <br>• Administered all advertising campaigns, organic post and data analysis from all Indianapolis social media accounts for Facebook, Twitter, and Instagram. Utilizing such tools as Hootsuite, Google AdWords and Google Trends to sustain effective position in the food delivery market. <br>• Increased Facebook fan base 45% from 121-175, Instagram followers from 0 to 40 and Twitter followers from 0 to 38 in 2-month span by engaging with the community and creating/delivering valuable evergreen/promotional content.</p>
</div></div>
<div id="workExperience-EeXleqUKiVGgLVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Business Development Intern</p>
<div class="work_company">
<span class="bold">Indiana Members Credit Union</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Indianapolis, IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2015 to October 2015</p>
<p class="work_description">• Promoted credit union product and services at all sponsored events, including such company sponsors as Indy Eleven, Indiana University Health network, Indianapolis Symphony Orchestra, IUPUI and various others within the sponsored credit union network. <br>• Handled the execution and display of all appropriate credit union media marketing within the 24 branch locations, located throughout the central Indiana market, as well as the ATM machines located within the sponsored company's establishments. <br>• Scheduled and assisted all three corporate Business Development Officers in all new potential sponsor company signings.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeXleqUKiVOgLVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in Business</p>
<div class="edu_school">
<span class="bold" itemprop="name">Indiana University, Kelley School of Business Indianapolis</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Indianapolis, IN</span></div>
</div>
<p class="edu_dates">2012 to 2016</p>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Google Analytics (1 year)</span>, <span class="skill-text">Google Adwords (1 year)</span>, <span class="skill-text">Illustrator (2 years)</span>, <span class="skill-text">Photoshop (2 years)</span>, <span class="skill-text">Indesign (2 years)</span>, <span class="skill-text">Social Media Marketing (2 years)</span>, <span class="skill-text">Hootsuite (2 years)</span>, <span class="skill-text">Microsoft Excel (10+ years)</span>, <span class="skill-text">Microsoft office (10+ years)</span>
</div></div></div>
</div>
<div class="section-item links-content">
<div><div class="section_title"><h2>Links</h2></div></div>
<div id="link-items" class="items-container"><div id="link-EeXleqUKYj6gLVn7OsNiYg" class="link-section last"><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=http%3A%2F%2Fwww.linkedin.com%2Fin%2Fzthieken&amp;h=f49ad0e1">http://www.linkedin.com/in/zthieken</a></p></div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Technical Skills <br> <br>OS: Microsoft Windows, Mac <br> <br>Software: Microsoft Office (Excel, Outlook, PowerPoint, Word); Adobe Creative Suites (InDesign, Illustrator, Photoshop) <br> <br>Social Media/Blogging: Facebook, Instagram, LinkedIn, Twitter, Pinterest, Hootsuite, Blogger, Wordpress <br> <br>Tools: Google AdWords, Google Analytics, Mail Chimp, Optimizely</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F5c6801f8a27d5d14/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ac2a5351b18a040c?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>51e8d9563224856e8674cdba36c892f1</uniq_id>
      <url>http://www.indeed.com/r/ac2a5351b18a040c?hl=en&amp;co=US</url>
      <zipcode>19319</zipcode>
      <name>Stower</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Stower</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Newark, DE</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebA0elEPai9h6mUQsuaQg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Stower</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Integrity Staffing Solutions</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Middletown, DE</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2016 to Present</p>
<p class="work_description">Scan items and bins, put items in bins in a safe manner.</p>
</div></div>
<div id="workExperience-EeWs8b_Ef_WQxpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Packer</p>
<div class="work_company">
<span class="bold">Integrity Staffing Solutions</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Middletown, DE</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2015 to December 2015</p>
<p class="work_description">Responsibilities <br>Packing items in boxes to be shipped to customers.</p>
</div></div>
<div id="workExperience-EeWs8YPLkKWQxpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Janitor</p>
<div class="work_company">
<span class="bold">Gemini Janitorial Services</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Wilmington, DE</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2015 to November 2015</p>
<p class="work_description">Responsibilities <br>Vacuum, mop, disinfect commercial trailers. <br> <br>Accomplishments <br>I made sure everyone's work space was clean.  <br> <br>Skills Used <br>N/A</p>
</div></div>
<div id="workExperience-EeVX8K1yYWq2A-IbfZYckw" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Babysitter</p>
<div class="work_company">
<span class="bold">Self Opportunity</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Newark, DE</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2008 to 2014</p>
<p class="work_description">Responsibilities <br>Feed, bathe, take care of children.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeVX8LkjZJe2A-IbfZYckw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High school or equivalent</p>
<div class="edu_school"><span class="bold" itemprop="name">Christiana High School</span></div>
</div></div></div>
</div>
<div class="section-item certification-content">
<div><div class="section_title"><h2>Certifications</h2></div></div>
<div id="certification-items" class="items-container"><div id="certification-EeYaM--trmelLCpAlk2Pyg" class="certification-section last"><div class="data_display">
<p class="certification_title">CPR/AED</p>
<p class="certification_date">July 2015 to July 2017</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fac2a5351b18a040c/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/e747aa192f0d2ab9?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>7ab65b9e502d2a9e679edecd442e2084</uniq_id>
      <url>http://www.indeed.com/r/e747aa192f0d2ab9?hl=en&amp;co=US</url>
      <zipcode>30502</zipcode>
      <name>Emergency/ICU Veterinary Technician</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Emergency/ICU Veterinary Technician</h1>
<h2 id="headline" itemprop="jobTitle">Masters in Public Health Student</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Doraville, GA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Obtain a position in the field of public health, where my experience can be fully utilized to improve the nation’s overall health care status, one community at a time.</p>
<p id="relocation_status">Willing to relocate: Anywhere</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeYpNBsd4ZuDMipAlk2Pyg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Emergency/ICU Veterinary Technician</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">BluePearl Veterinary Partners</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Lawrenceville, GA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">September 2015 to Present</p>
<p class="work_description">Responsibilities <br>-    Triage and stabilize critical patients in an emergency setting <br>-    Operated diagnostic radiographic and ultrasound equipment <br>-    Collected tissue, feces, blood and urine samples for examination and analysis. <br>-    Placement of peripheral and central line catheters <br>-    Administering treatments, medications, and performing procedures on all hospitalized patients <br>-    Admitting patients, preparing charts, processing invoices for clients and answering calls <br>-    Administer medications, fluid therapy and use pharmaceutical knowledge independently for the benefit of critical patients <br>-    Assist in procedures, including laceration repairs, centesis, foreign body and GDV surgery, cesarean and whelping <br>-    Placement of peripheral and central line catheters</p>
</div></div>
<div id="workExperience-EeYpNHQWrNGNfBkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Assistant Practice Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Banfield Pet Hospital</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Norcross, GA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2011 to Present</p>
<p class="work_description">Responsibilities <br>o    Act as communications liaison, including holding routine hospital team meetings, sharing business information and messaging to all hospital associates. <br>o    Oversee and train associates on professional, efficient and exceptional client service, to include client education. <br>o    Partner with the Medical Director, Field Director, and Chief of Staff (if applicable) to interview, select, train, develop, coach, and mentor. <br>o    Identify potential “bottlenecks” and formulate solutions to remove barriers. <br>o    Effectively create and maintain paraprofessional schedules and work with Chief of Staff to schedule associates. <br>o    Regularly supervise the team of paraprofessionals in the hospital. <br>o    Provide inspirational leadership to the team by creating a positive professional relationship with the Chief of Staff, PetSmart associates, adoption center agencies, clients, field leadership and Central Team Support. <br>Ensure compliance with all practice policies and procedures and with all local, state and federal laws</p>
</div></div>
<div id="workExperience-EeYpNEbjK9CNfBkqK6c2tA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Vet Technician/Administrative Assistant</p>
<div class="work_company">
<span class="bold">Tallahassee Animal Service Center</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Tallahassee, FL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2012 to August 2015</p>
<p class="work_description">Responsibilities <br>o    Conduct tests and help in diagnosing illnesses and injuries of animals.  <br>o    Take laboratory tests, prepare tissue samples for testing and perform urinalysis and blood counts.  <br>o    Advise owners on safety/health practices to ensure no disease is transmitted from animals to humans.  <br>o    Give medications orally and topically to animals with injuries, diseases and contaminations.  <br>o    Document pain and stress level tests of animals used for research and development, as well as medical histories.  <br>o    Schedule and coordinate meetings, appointments, and travel arrangements for supervisors and managers <br>o    Trained over 10 administrative assistants during a period of company expansion to ensure attention to detail and adherence to company policy <br>o    Developed new filing and organizational practices, saving the company labor expenses <br>o    Maintain utmost discretion when dealing with sensitive topics <br>o    Manage travel and expense reports for department team members</p>
</div></div>
<div id="workExperience-EeYpNKEiqAaFHOIbfZYckw" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Aftercare Teacher</p>
<div class="work_company">
<span class="bold">The G.L.O.B.E. Academy</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Atlanta, GA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2014 to June 2015</p>
<p class="work_description">Responsibilities <br>o    Prepare materials and equipment for after school instructional activities. · Create and lead a weekly enrichment classes.  <br>o    Lead small group activities that are age appropriate in an atmosphere where students are actively engaged in meaningful learning experiences enhancing the social, physical, and academic advancement of children in his/her care.  <br>o    Assist students with homework assignments and provide Maintain a daily attendance log.  <br>o    Be able to create a fun and safe learning environment.  <br>o    Be prepared to implement the lesson plans and activities for each day.  <br>o    Be punctual. The after school program follows the school site calendar and is closed during normal school closure days.  <br>o     Welcome and supervise the work of volunteers &amp; service learning students assigned to your group.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeYpNMdvf3aFHOIbfZYckw" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Master's in Public Health</p>
<div class="edu_school">
<span class="bold" itemprop="name">Capella University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Minneapolis, MN</span></div>
</div>
<p class="edu_dates">2015 to 2017</p>
</div></div>
<div id="education-EeKo-gDnh8yx17PItWPLJQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">B.S. in Animal Science</p>
<div class="edu_school"><span class="bold" itemprop="name">Florida A&amp;M University</span></div>
<p class="edu_dates">August 2012</p>
</div></div>
<div id="education-EeKo-gDnh82x17PItWPLJQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school"><span class="bold" itemprop="name">Mainland High School</span></div>
<p class="edu_dates">August 2003 to May 2007</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">o    Knowledge of human resources policies, rules and regulations. o    Maintaining confidentiality regarding Human Resources related issues. o    Ability to interface effectively with all levels of staff. o    Ability to prepare and maintain accurate records. o    Creating and updating personnel, payroll and accounting information in spreadsheets. o    Writing reports, business correspondence, and procedure manuals. o    Proficient with MS Word &amp; Excel.</span></div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>¥    Florida A&amp;M University Marching “100” Band <br>¥    Verizon Wireless Fashion Member-Community <br>¥    Volunteer Halifax Medical Center <br>¥    Volunteer Tallahassee Animal Service Center <br>¥    Magazine Publication( vogue.it, Rolling Out, Fenuex Magazine, DNA Magazine, The Storm Magazine and Jute Fashion Magazine)</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fe747aa192f0d2ab9/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/4e57e6f982204e73?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>1724b493a70a2a27699964907e44fcdc</uniq_id>
      <url>http://www.indeed.com/r/4e57e6f982204e73?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Bus Driver</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Bus Driver</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeY9p8a-HxOv8Fn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Bus Driver</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Sunrise Transportation</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2015 to Present</p>
<p class="work_description">Drive Children to and from school i also do charters</p>
</div></div>
<div id="workExperience-EeTxRz1HHJiyyllI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Home Care Aide</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Lutheran Social Services</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2009 to Present</p>
<p class="work_description">Responsibilities <br>I help elderly citizens in there homes. i cook,clean,wash garments,run arrands make Dr. Appt and sometimes sit down and have long talks with my clients. <br> <br>Accomplishments <br>I recieved an award for being a staff for over five years. <br> <br>Skills Used <br>good communication skills,listen,be patience and have respect for your clients.</p>
</div></div>
<div id="workExperience-EeTxRnOZK46fTln7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Direct Support Person</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Southwest Disability Servics</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2007 to Present</p>
<p class="work_description">Responsibilities <br>there are eight indiviual i take care of i cook,clean,wash garments,assist with bathes,help indiviual with putting there clothes out for workshop monitor at all time <br> <br>Accomplishments <br>being a good worker and a friend to my clients. <br> <br>Skills Used <br>you have to be able too have patience for this job, willing to listen,be calm,talk clearly and in some situation you just might  have to handle some things that might require two staff and set a time for everything if possible.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeY9p_v7k6yv8Fn7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Certificate in Medical Assistant</p>
<div class="edu_school">
<span class="bold" itemprop="name">Everett Community College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2007 to 2008</p>
</div></div>
<div id="education-EeTxQ_0mDnafTln7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Diploma in General</p>
<div class="edu_school">
<span class="bold" itemprop="name">Midwest Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">1996 to 2000</p>
</div></div>
</div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F4e57e6f982204e73/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/d9bff0c3ad5ed4dd?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>db4b90fc0ba0686804c77789c27e3403</uniq_id>
      <url>http://www.indeed.com/r/d9bff0c3ad5ed4dd?hl=en&amp;co=US</url>
      <zipcode>60631</zipcode>
      <name>Prepared Foods Team Member</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Prepared Foods Team Member</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Berwyn, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeKVySg32gmS1L9oLXgqwg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Prepared Foods Team Member</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Whole Foods market</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="work_description">This job requires to help guest at all time even by welcoming them to we're they are shopping this is part of the job , most of this is kitchen work so have to make sure the food we cook is on temp which ever u want wether is cold food has to be under 40'  if u want hot food to be ready has to be over 160 I started of as a dishwasher but now I work in da same department but a venue called fresh pack were everything has to be under 40' to be packed out part of the job is taking temperatures making sure we change the sanitation water when needed initial all your work making sure everything we packaged is FIFO in our cases</p>
</div></div>
<div id="workExperience-EeKVydRGe4SS1L9oLXgqwg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Food Service Worker</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Target</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="work_description">I work at the Pizza Hut inside the target in elston. Here we have to welcome guest all times because it is part of the job. Also have to pay attention to temperatures and FiFo everything that needs to b FIFO I also know how to work the cash registers because they have giving me and opportunity to do so also facing is and important task here just like it is at whole foods market</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeKVyfmnRGyS1L9oLXgqwg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">High school diploma</p>
<div class="edu_school">
<span class="bold" itemprop="name">Wells Community Academy High School</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2005 to 2009</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fd9bff0c3ad5ed4dd/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/3c6e72b23a66a83e?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>6cea3c3f698db3eca642166cccab79a1</uniq_id>
      <url>http://www.indeed.com/r/3c6e72b23a66a83e?hl=en&amp;co=US</url>
      <zipcode>60610</zipcode>
      <name>CNA - Certified Nursing Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">CNA - Certified Nursing Assistant</h1>
<h2 id="headline" itemprop="jobTitle">Customer Service</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To obtain a position where I can utilize my practical experience to provide excellent and effective customer service.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeZfs1ip2nmqxAxatb0mLA" class="work-experience-section "><div class="data_display">
<p class="work_title title">CNA - Certified Nursing Assistant</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Alden Manor Heather home</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Harvey, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2016 to Present</p>
<p class="work_description">Answer call lights,assist residents with Adl's,report behaviors to charge nurse</p>
</div></div>
<div id="workExperience-EeW9WBR-R7GAuyLhBGsw1Q" class="work-experience-section "><div class="data_display">
<p class="work_title title">CNA - Certified Nursing Assistant</p>
<div class="work_company">
<span class="bold">Peterson Park Health Care Center</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">October 2014 to September 2016</p>
<p class="work_description">Responsibilities <br>Help residents with ADL's. Report changes within the resident to the charge nurse. Took vital signs, bathed,</p>
</div></div>
<div id="workExperience-EeXAIhCHH5W7AFlI6yVEOA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Packer</p>
<div class="work_company">
<span class="bold">Graham Packaging</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2011 to September 2014</p>
<p class="work_description">Responsibilities <br>I packed and inspected plastic bottles for defects <br>Followed OSHA rules and regulations  <br>Kept my work space clean and neat <br>Build and labeled boxes</p>
</div></div>
<div id="workExperience-EeHNJGjBIsKfRhOqxCdrIA" class="work-experience-section "><div class="data_display">
<p class="work_title title">CNA</p>
<div class="work_company">
<span class="bold">HICKORY NURSING PAVILLION</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Hickory Hills, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2010 to September 2011</p>
<p class="work_description">Assisting residents with ADL's <br>Report any changes with rhe residents to the charge  <br>Feed the resident, give the residents showers and take their vitals signs</p>
</div></div>
<div id="workExperience-EeHNJB1BSZ-fRhOqxCdrIA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Cashier</p>
<div class="work_company">
<span class="bold">Wal-Mart</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2007 to November 2009</p>
<p class="work_description">floor clean <br>Demonstrating initiative <br>Worked with a team <br>Stocked shelves <br>Cash handling</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeRAFFIHlxibwRkqK6c2tA" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">CNA</p>
<div class="edu_school">
<span class="bold" itemprop="name">South Central College</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Mankato, MN</span></div>
</div>
<p class="edu_dates">2011 to 2011</p>
</div></div>
<div id="education-EeRAFCDHZvWbwRkqK6c2tA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">GED</p>
<div class="edu_school">
<span class="bold" itemprop="name">Lincoln Community Center</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Mankato, MN</span></div>
</div>
<p class="edu_dates">2010 to 2011</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Medical Skills: <br>CPR Certified for Adults, Infants, and Children <br>Vital Signs</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F3c6e72b23a66a83e/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/0dc12b75b8daba24?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>d4e9e30610fe14e13b52b42a93b9ce74</uniq_id>
      <url>http://www.indeed.com/r/0dc12b75b8daba24?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Route Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Route Manager</h1>
<h2 id="headline" itemprop="jobTitle">Customer Service Representative</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Berwyn, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeXE4tegB0WmJVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Route Manager</p>
<div class="work_company">
<span class="bold">Namco Cybertainment</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Bensenville, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2008 to 2009</p>
<p class="work_description">• Diagnose and repair video arcade equipment <br>• Collect revenue from equipment <br>• Respond to customer concerns</p>
</div></div>
<div id="workExperience-EeXE4tegB0amJVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Customer Service Representative</p>
<div class="work_company">
<span class="bold">Today's Staffing</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Downers Grove, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2005 to 2008</p>
<p class="work_description">• Answer incoming calls in call center <br>• Respond to customer concerns <br>• Input data into system</p>
</div></div>
<div id="workExperience-EeXE4tegB0emJVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Shipping &amp; Receiving Mgr</p>
<div class="work_company">
<span class="bold">EM Coating Servces</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Lombard, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2003 to 2005</p>
<p class="work_description">• Maintained customer contact on job status <br>• Respond to customer concerns and discrepancies <br>• Route all inbound &amp; outbound freight</p>
</div></div>
<div id="workExperience-EeXE4tegLlimJVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Warehouse Clerk</p>
<div class="work_company">
<span class="bold">KYB America LLC</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Addison, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2001 to 2002</p>
<p class="work_description">• Pick orders &amp; replenish stock on daily basis <br>• Load and unload trucks using forklift <br>• Daily operation of UPS manifest</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeXE4tegLlmmJVn7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">The Chubb Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Villa Park, IL</span></div>
</div>
<p class="edu_dates">2000 to 2001</p>
</div></div>
<div id="education-EeXE4tegLlumJVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Automotive Technology II</p>
<div class="edu_school">
<span class="bold" itemprop="name">Universal Technical Institute</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Glendale Heights, IL</span></div>
</div>
<p class="edu_dates">1993 to 1995</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element">
<span class="skill-text">Microsoft Office (10+ years)</span>, <span class="skill-text">Warehouse Management (3 years)</span>, <span class="skill-text">Forklift Operator (10+ years)</span>, <span class="skill-text">Shipping And Receiving (10+ years)</span>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F0dc12b75b8daba24/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/dc4ba4f052ec74ca?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>a83d440940a7b8ff1403e9dfee73c331</uniq_id>
      <url>http://www.indeed.com/r/dc4ba4f052ec74ca?hl=en&amp;co=US</url>
      <zipcode>60626</zipcode>
      <name>Contractor</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Contractor</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="relocation_status">Willing to relocate: Anywhere</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeQCs2eEQvy3VbPItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Contractor</p>
<div class="work_company">
<span class="bold">Motorola solutions</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Schaumburg, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2013 to July 2014</p>
<p class="work_description">Responsibilities <br>Worked on assembly line assemble radio test radios pack radios <br> <br>Accomplishments <br>Be able to train other employees, able to work at a faster pace <br> <br>Skills Used <br> Assemble, more hands on at the computer</p>
</div></div>
<div id="workExperience-EeQCsihmFAK_TAIvSgncRA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Cashier/Customer Service</p>
<div class="work_company">
<span class="bold">Walmart</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2011 to July 2013</p>
<p class="work_description">Responsibilities <br>Check out customer,make sure front end was clean,cash checks,returns,money gram  <br> <br>Accomplishments <br>Knowing that I had a lot of customers that looked up to me they would always come to my line  <br> <br>Skills Used <br>Cash register</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeQCsjasGQi_TAIvSgncRA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">Midwest</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Evergreen Park, IL</span></div>
</div>
<p class="edu_dates">2011 to 2011</p>
</div></div></div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Microsoft word Microsoft PowerPoint</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fdc4ba4f052ec74ca/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/f547482d83611468?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>16e8607110eb949a62b6685ce01dc20b</uniq_id>
      <url>http://www.indeed.com/r/f547482d83611468?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Teaching Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Teaching Assistant</h1>
<h2 id="headline" itemprop="jobTitle">Teaching Assistant</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Seeking employment as an Entry Level Software Engineer to use experiences in designing software <br>applications and developing testing procedures for your Company</p>
<p id="relocation_status">Willing to relocate: Anywhere</p>
<p id="employment_eligibility">Sponsorship required to work in the US</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container"><div id="workExperience-EebAxg1XleS99Vn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Teaching Assistant</p>
<div class="work_company">
<span class="bold">Illinois Institute of Technology</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2015 to December 2015</p>
<p class="work_description">Assisted the professor in helping the students learn technologies for developing enterprise web <br>applications, software management concepts. <br>Have successfully held learning sessions for over a hundred students in Microsoft Project <br>Professional 2013 to draft effective project plans. <br>Have been the point of contact for all the queries/questions pertaining to the subject. <br>Have also assisted the professor in grading and organizing projects for the students. <br>Projects <br>Sign Language Translator <br>Sign Language Translator is a windows application that translates hand signs to text as well as <br>Speech. <br>Skin detection and background subtraction was used to get accurate results. <br>The system built was successfully able to recognize American sign language hand signs for alphabets and gestures.Tool used was Microsoft Visual Studio with OpenCV library <br>Professor Pedia (Android Application) <br>Professor Pedia is an Android Application which helps new students as well as current students to know about the subjects they are going to register for at Illinois Tech. <br>This application helps the students in deciding whether to take that course or not based on the student's reviews and ratings given by other students. <br>CPU, Disk and Memory Benchmarking(Amazon AWS, t2.micro, C++ ) <br>Benchmarked CPU performance in GFLOPS and IOPS with varying concurrency levels to evaluate the speed of the processor. <br>Evaluated the throughput and latency of disk for sequential and random read/write IO, varying <br>block size and degree of concurrency. <br>Tourist Destination Advisor (Python) <br>Tourist Destination Advisor system assists the tourists to decide their tourist destination based on the experiences and views of the tourists who already visited that place. <br>This system classify comments gathered from data available on Twitter and further analyze <br>comments into positive and negative sentiments.</p>
</div></div></div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EebAxg1Xlea99Vn7OsNiYg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Computer Science</p>
<div class="edu_school">
<span class="bold" itemprop="name">Illinois Institute of Technology － Chicago</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2016</p>
</div></div>
<div id="education-EebAxg1Xlei99Vn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Engineering in Information Technology</p>
<div class="edu_school">
<span class="bold" itemprop="name">Dayananda Sagar College of Engineering</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Bangalore, Karnataka</span></div>
</div>
<p class="edu_dates">2014</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Key Skills <br>Proficiency in C, C++, Java          Selenium IDE/RC tests <br>Novice in Python, JavaScript        Quick learner <br>Testing                            Proficient in HTML</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Ff547482d83611468/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/3d91952f983af0ee?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>61c7b4fcc5e901a1c3b20c42f0146033</uniq_id>
      <url>http://www.indeed.com/r/3d91952f983af0ee?hl=en&amp;co=US</url>
      <zipcode>98532</zipcode>
      <name>Baker/Opener/Fryer</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Baker/Opener/Fryer</h1>
<h2 id="headline" itemprop="jobTitle">Baker/Opener/Fryer</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Centralia, WA</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To achieve a part or full time job in a healthy, local work environment, while building skills for future success.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EebA3JgnAXi2CIeEKom3Nw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Baker/Opener/Fryer</p>
<div class="work_company">
<span class="bold">FULLERS BAKERY</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Centralia, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2015 to 2016</p>
</div></div>
<div id="workExperience-EebA3JgnAXm2CIeEKom3Nw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Warehouse Worker and Food handler</p>
<div class="work_company">
<span class="bold">FULLERS BAKERY</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Olympia, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2015 to 2015</p>
</div></div>
<div id="workExperience-EebA3JgnAXq2CIeEKom3Nw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Baker Assistant</p>
<div class="work_company"><span class="bold">Market St. Bakery</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2013 to 2014</p>
<p class="work_description">AMAZING pastries. we would take our time and make a fine product, out of every job working with food this was my favorite.</p>
</div></div>
<div id="workExperience-EebA3JgnKIu2CIeEKom3Nw" class="work-experience-section "><div class="data_display">
<p class="work_title title">Child Supervisor and Cook</p>
<div class="work_company">
<span class="bold">Happy Hive Childcare</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Centralia, WA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2013 to 2014</p>
<p class="work_description">I would come in and work full time supervising children, I cooked meals, in charge of naps, and other extracurricular activities, such as outside time, playing, and seeing the children home.  <br>On a side note I also had to learn infant CPR.</p>
</div></div>
<div id="workExperience-EebA3XxsqU-LuNx7tzCqKQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Cashier/Cook</p>
<div class="work_company">
<span class="bold">Greenwood foodmart</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Greenwood, MA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2011 to May 2012</p>
<p class="work_description">Kept the store clean, made subs, pizzas, worked the register and the lottery machine.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EebA3JgnKIy2CIeEKom3Nw" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school"><span class="bold" itemprop="name">Centralia College</span></div>
<p class="edu_dates">2009</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>SKILLS <br>Food Preparation and handling <br>Time management <br>Cash handling <br>Can lift 50 lbs. + <br>People skills</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F3d91952f983af0ee/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/860f970a2bb58546?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>b6751fe97704a75f0e6679fdd1abf726</uniq_id>
      <url>http://www.indeed.com/r/860f970a2bb58546?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Marketing Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Marketing Assistant</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeIHMIfalU6l9BOqxCdrIA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Marketing Assistant</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Integrated Audiology Care</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Downers Grove, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">July 2011 to Present</p>
<p class="work_description">Implement marketing and advertising campaigns by assembling and preparing marketing and advertising strategies, plans, and objectives; planning and organizing promotional presentations; <br>updating calendars. <br>●Prepare marketing reports by collecting, analyzing, and summarizing sales data. <br>●Track product line sales and costs by analyzing and entering sales, expense, and new <br>business data.</p>
</div></div>
<div id="workExperience-EeIHMIfalU-l9BOqxCdrIA" class="work-experience-section "><div class="data_display">
<p class="work_title title">Sales Merchandiser</p>
<div class="work_company">
<span class="bold">Advantage Sales and Marketing</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2008 to April 2011</p>
<p class="work_description">Sales Merchandiser: <br>●Serviced 150 retail accounts in the Chicago territory for GlaxoSmithKline products. <br>●Developed promotional presentations to market, sell and facilitate a successful launch of new <br>products in addition to verifying existing merchandise distribution. <br>●Developed working relationships with retail managers to maximize sales and increase sales <br>volume for store as well as GlaxoSmithKline.</p>
</div></div>
<div id="workExperience-EeIHMIfalVCl9BOqxCdrIA" class="work-experience-section "><div class="data_display">
<p class="work_title title">National Sales and Marketing Intern</p>
<div class="work_company">
<span class="bold">Hostelling International</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2007 to August 2007</p>
<p class="work_description">Designed and created promotional advertisements for our services in the national market. <br>●Identified new target markets and segmented existing ones to increase hostel revenue. <br>●Budgeted, planned and implemented fundraising events for international exchange program.</p>
</div></div>
<div id="workExperience-EeIHMIfalVGl9BOqxCdrIA" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Emergency Medical Technician</p>
<div class="work_company">
<span class="bold">Oglesby Ambulance</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Oglesby, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2006 to December 2006</p>
<p class="work_description">Provided emergency care to the sick and injured and managed patient care levels during transport to hospital for a population of 15,000. <br>●Inventoried and restocked ambulance equipment and supplies as needed. <br>●Prepared and submitted patient care reports.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeIHMIfalVOl9BOqxCdrIA" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelors of Science in Business</p>
<div class="edu_school"><span class="bold" itemprop="name">Eastern Illinois University</span></div>
<p class="edu_dates">December 2007</p>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Computer Skills: Microsoft Word, Excel, Access, PowerPoint, Outlook</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F860f970a2bb58546/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/6266cf414d1d1150?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>1812b4203becd7089fcbc86f2f0b3380</uniq_id>
      <url>http://www.indeed.com/r/6266cf414d1d1150?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Loader/Unloader</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Loader/Unloader</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeRB1pxgIaSeTSpAlk2Pyg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Loader/Unloader</p>
<div class="work_company">
<span class="bold">Bradford exchange</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">November 2012 to April 2014</p>
<p class="work_description">Responsibilities <br>Loader/unloader picker,scaner,inventory.. <br> <br>Skills Used <br>Professional and hard working,always their earily and willing to stay late.Perfection</p>
</div></div>
<div id="workExperience-EeRB1VcuArOXCpTgI3RMKQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Mechanic</p>
<div class="work_company">
<span class="bold">dave's auto</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2005 to April 2012</p>
<p class="work_description">Responsibilities <br>Work and repaired various types of cars... <br> <br>Skills Used <br>maintaned perfect and professional skills.</p>
</div></div>
<div id="workExperience-EeRB2hmSRq-XCpTgI3RMKQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Janitorial Worker</p>
<div class="work_company">
<span class="bold">Sears building</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 1999 to June 2004</p>
<p class="work_description">Responsibilities <br>Maintaining and cleaning of over 60 offices,and bathrooms,removing garbage ect.. <br> <br>Skills Used <br>positive,hardworking,ability to produce perfection..</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeRB1vX5JHeXVuIbfZYckw" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Food and sanitation</p>
<div class="edu_school">
<span class="bold" itemprop="name">Phaylnx</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2009 to 2009</p>
</div></div>
<div id="education-EeRB11303QmeTSpAlk2Pyg" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">CNC</p>
<div class="edu_school">
<span class="bold" itemprop="name">Wright college</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">2008 to 2009</p>
</div></div>
<div id="education-EeRB17-2zkqeTSpAlk2Pyg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">automotive tech.</p>
<div class="edu_school">
<span class="bold" itemprop="name">Illinois central college</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Springfield, IL</span></div>
</div>
<p class="edu_dates">1993 to 1995</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Cnc,Basic and advanced automotive,Food and sanitation,Building construction,custodial maintanace</span></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F6266cf414d1d1150/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/0d2c5bc8ebecb155?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>e594c6d9c799b8cc6a47b9b4840b187b</uniq_id>
      <url>http://www.indeed.com/r/0d2c5bc8ebecb155?hl=en&amp;co=US</url>
      <zipcode>60657</zipcode>
      <name>RN Staff</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">RN Staff</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Morton Grove, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">●    To work full time as a Registered Nurse in various hospital settings.</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeVGErJSHt-SkhJ-cpi6yQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">RN Staff</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Swedish Covenant Hospital</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2006 to Present</p>
<p class="work_description">Provide close monitoring of acute and chronic cardiac disease for arrhythmias, MI screening, <br>anticoagulation therapy, cardiac and peripheral diagnostic procedures <br>●    Work collaboratively with surgeons, cardiologists and interns to achieve optimum patient care <br>●    Utilized the nursing process to assess, plan, implement and evaluate telemetry patient care <br>●    Demonstrate critical thinking and use clinical judgment to meet patient care needs including establishing <br>priorities <br>●    Assess signs and symptoms indicating physiologic and psychosocial changes in the patient's conditions</p>
</div></div>
<div id="workExperience-EeVGErJSuyCSkhJ-cpi6yQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">RN</p>
<div class="work_company">
<span class="bold">Central Nursing Home</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2005 to November 2005</p>
<p class="work_description">Assess signs and symptoms indicating physiologic and psychosocial changes in the geriatric patient <br>population <br>●    Perform tasks related to the functioning of the unit and the provision of optimum patient care</p>
</div></div>
<div id="workExperience-EeVGErJS4jGSkhJ-cpi6yQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">faculty member</p>
<div class="work_company"><span class="bold">Medical Trust Hospital</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">January 2003 to August 2004</p>
<p class="work_description">for International Language Testing skills for Undergraduate Nursing Students,</p>
</div></div>
<div id="workExperience-EeVGErJS4jKSkhJ-cpi6yQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Teacher RN</p>
<div class="work_company"><span class="bold">Bhagirathi Neotia Women's and Children's Hospital</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2001 to May 2002</p>
<p class="work_description">Utilized the nursing process to assess, plan, implement and evaluate patients in the Pediatric Intensive <br>Care and Neonatal Intensive Care. <br>●    Perform tasks related to the functioning of the unit and provision of patient care.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeVGErJTCUOSkhJ-cpi6yQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelors of Nursing</p>
<div class="edu_school">
<span class="bold" itemprop="name">Griffith University</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Brisbane QLD</span></div>
</div>
<p class="edu_dates">July 1999</p>
</div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F0d2c5bc8ebecb155/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/2e3c79a9dca7a643?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>8da372a381906827479a690a73e2ed9c</uniq_id>
      <url>http://www.indeed.com/r/2e3c79a9dca7a643?hl=en&amp;co=US</url>
      <zipcode>30502</zipcode>
      <name>Property Manager</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Property Manager</h1>
<h2 id="headline" itemprop="jobTitle">Property Manager</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Lawrenceville, GA</p></div>
<div class="separator-hyphen">-</div>
</div>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeLIY-9zicCx17PItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Property Manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Casas Unicas Apts</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dallas, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2009 to Present</p>
<p class="work_description">53 unit property. I have firm knowledge of the area and competitive rental properties in my area. As property manager I recommend marketing programs, special promotions and other advertising strategies to the owner in order to maximize occupancy and rental rates. As the property manager I am the owner's partner in maximizing the return on investment of the property through efficient performance of these four functional areas of responsibility. Marketing and Financial, Tenant and Occupancy, Facility Administration &amp; Risk Management. The unwelcome task of eviction for violations or non-payment is part of this function also.</p>
</div></div>
<div id="workExperience-EeLIY-9zicGx17PItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">sales manager</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span class="bold" itemprop="name">Casas Unicas Apts</span></div>
<p class="work_description">day in store operations. To ensure that the store is safe and properly maintained, routinely inspect the inventory, parking facilities, and equipment to determine if repairs or maintenance is needed. Meet with current customers when handling requests for auto parts or trying to resolve complaints. Also keeping accurate, up-to-date records of income and expenditures from store operations and submitting regular expense reports to supervisors and district manager.</p>
</div></div>
<div id="workExperience-EeLIY-9zicKx17PItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Administrative Assistant/Leasing Agent</p>
<div class="work_company">
<span class="bold">CNC LLC</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Dallas, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">March 2007 to August 2009</p>
<p class="work_description">1300 units. Leasing, understanding the needs of the tenants is important for this function. Getting them to move in is only the beginning. As a leasing agent I must then respond to their requests, monitor their activities as regards the lease requirements, collect rent in a timely manner, and continually assess the tenants' satisfaction as regards the property's amenities versus those of competing rental properties in the area. I served as information and communication managers for an office, plan and schedule meetings and appointments; organize and maintain paper and electronic files; manage projects; conduct research; and disseminate information by using the telephone, mail services, Web sites, and e-mail.</p>
</div></div>
<div id="workExperience-EeLIY-9zicOx17PItWPLJQ" class="work-experience-section "><div class="data_display">
<p class="work_title title">Sales Manager AutoZone</p>
<div class="work_company"><div class="inline-block"><span>Dallas, TX</span></div></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">August 2002 to March 2007</p>
</div></div>
<div id="workExperience-EeLIY-9zicSx17PItWPLJQ" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Leasing Agent Green Hills</p>
<div class="work_company">
<span class="bold">SNL Management</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Grand Prairie, TX</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">May 2000 to June 2002</p>
<p class="work_description">I showed and leased apartments to prospective tenants, Interviewed prospective tenants and recorded information to ascertain needs and qualifications. Accompanied prospects to model partments and discuses size and layout of rooms, available facilities, such as swimming pool, location of shopping centers, services available, and terms of lease. Complete lease form or agreement and collect rental deposits. Inspect condition of premises periodically and arrange for necessary maintenance. Compile listings of available rental property. Compose newspaper advertisements, contact credit bureau to obtain credit report on prospective tenant.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeLIY-9zsNax17PItWPLJQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Diploma</p>
<div class="edu_school">
<span class="bold" itemprop="name">Bryan Adams High School Dallas</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dallas, TX</span></div>
</div>
<p class="edu_dates">2002 to 2004</p>
</div></div>
<div id="education-EeLIY-9zsNex17PItWPLJQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<div class="edu_school">
<span class="bold" itemprop="name">Skyline High School Dallas</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Dallas, TX</span></div>
</div>
<p class="edu_dates">2000 to 2002</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Key Skills <br>Office Management <br>Teambuilding &amp; Supervision <br>Resident Retention &amp; Marketing <br>Excellent Customer Service &amp; Communication Skills <br> <br>Reporting <br>Spreadsheet Creation <br>Budget Creation/Utilization <br>Bookkeeping &amp; Payroll <br>Bilingual- Spanish/English <br> <br>Extremely Knowledgeable in TAA Leases, Forms, &amp; Procedures <br>Property Billing <br>Maintenance Management <br>Vendor Relations</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F2e3c79a9dca7a643/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/957e00c249cbd9fa?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>134d5b07e00dafc15d2601ec7450896c</uniq_id>
      <url>http://www.indeed.com/r/957e00c249cbd9fa?hl=en&amp;co=US</url>
      <zipcode>60611</zipcode>
      <name>Paralegal/Legal Adminstrative Assistant</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Paralegal/Legal Adminstrative Assistant</h1>
<h2 id="headline" itemprop="jobTitle">Paralegal/Legal Adminstrative Assistant - Meyer &amp; Njus, P.A Attorney At Law</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">To secure a responsible position that will utilize my previous work <br>Experience and have an opportunity to learn new skills which will allow me to become a part of a <br>progressive growth oriented company. Over 5 years of extensive experience as an Administrative <br>assistant with professional experience in customer service and the Legal field. A skilled computer user; <br>with a goal driven mentality. I consider myself an enthusiastic and avid learner, with a keen attention to <br>detail.</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeWYt1pEmDuGuVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Paralegal/Legal Adminstrative Assistant</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Meyer &amp; Njus, P.A Attorney At Law</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2010 to Present</p>
<p class="work_description">Prepare legal documents and correspondence (briefs, pleadings, appeals, <br>wills, contracts, ect). Identify process improvements to ensure company <br>compliance with Legal requirements or to increase department efficiency. <br>Check Legal forms for accuracy. Prepare correspondence. Draft a variety of legal documents. Call Courts to follow up on files. Prepare Summons and <br>Complaint's/Garnishment. Administrative Duties such as Scanning/Imaging <br>/Filing/Mailing/Telephone. Performs other duties and assignments as required with acceptable results. Knowledge of civil proceedings for states of Illinois and Indiana. Knowledge of legal terminology and principles.</p>
</div></div>
<div id="workExperience-EeWYt1pEmDyGuVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Digital Printer Machine Operator</p>
<div class="work_company">
<span class="bold">SportsAwards Company</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2010 to 2010</p>
<p class="work_description">Operates a Fuji machine in accordance with established procedures and guidelines. Read and interprets corps and diagrams to select. Fuji machine <br>helps provide recognition, award and promotional products. Making sure <br>all orders are taken care of on a timely fashion. Reviewing all orders for errors and make appropriate adjustments and corrections.</p>
</div></div>
<div id="workExperience-EeWYt1pEmD2GuVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Cashier</p>
<div class="work_company">
<span class="bold">Aldi Grocery Store</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Niles, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2009 to 2010</p>
<p class="work_description">Responsible to provide customer and personnel assistance. <br>Handle cash intake. <br>Inventory control and maintenance.</p>
</div></div>
<div id="workExperience-EeWYt1pEmD6GuVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Judge's Clerk</p>
<div class="work_company">
<span class="bold">Will County Court House</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Joliet, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2006 to 2008</p>
<p class="work_description">Provide clerical support preparing legal document, responsible for telephone and written notification of witnesses, filing, creating files, data <br>entry and operating standard office machines. Responsible for maintaining <br>upkeep of daily court call and data base. Summary entries of pleadings, <br>motions, minute order, entering judgments and orders. Fax over police <br>reports. Perform any and all other duties that were assigned to me.</p>
</div></div>
<div id="workExperience-EeWYt1pEmD-GuVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Loan Processor</p>
<div class="work_company">
<span class="bold">Advantage Mortgage</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2004 to 2006</p>
<p class="work_description">Process all purchase and refinance loan (conventional, non-conforming) <br>data entry, collecting of proper documentation needed to complete the final <br>approval. Ordering full credit reports, appraisal reports, final inspection, and title polices, helping client find appropriate homeowners insurance, <br>setting and approving closing dates desktop underwriting for Conventional <br>Loans, comparing premiums between lenders and locking rates, conversing with Account Executives before submitting loans to get a faster approvals.</p>
</div></div>
<div id="workExperience-EeWYt1pEmECGuVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">Closer/Funder/Secretary</p>
<div class="work_company">
<span class="bold">Law Title</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Naperville, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2002 to 2004</p>
<p class="work_description">Ability to function under stress and with short time frames. Preparing <br>Huds, closing documentation, clearing Title. Making sure to have all <br>documentation to fund a file. Responsible to payoff existing Mortgage. <br>Responsible to cut all checks. Returning original documents to the lender. <br>Most important satisfying our clients and customers.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container"><div id="education-EeWYt1pEmEKGuVn7OsNiYg" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Paralegal</p>
<div class="edu_school">
<span class="bold" itemprop="name">Northwestern COLLEGE</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
</div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>SKILLS <br>•   Elevated grammatical and excellent verbal and written skills <br>•   Highly focused with strong organizational, and time management skills <br>•   Great customer service skills <br>•   Bilingual/Bicultural, completely fluent in English and Spanish <br>•    65+ WPM skills with complete accuracy <br>•   Extremely detail oriented. <br>•   Ability to understand and carry out oral and written instructions and request clarification when <br>needed <br>•   Proficiency with advanced Microsoft Office, Word, Excel, PowerPoint, Outlook <br>•   Ability to multitask in a demanding client facing role <br>•   Strong Database management skills</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2F957e00c249cbd9fa/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/addc0a73570845e2?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>afe22d5606f3700b57944b3d8b5cb0ce</uniq_id>
      <url>http://www.indeed.com/r/addc0a73570845e2?hl=en&amp;co=US</url>
      <zipcode>60631</zipcode>
      <name>Tableau Developer</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn " itemprop="name">Tableau Developer</h1>
<h2 id="headline" itemprop="jobTitle">Tableau Developer - Cars.com</h2>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Arlington Heights, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">• Over 5+ years of experience in Software Design, Development, Implementation, Maintenance and production Support of Business Intelligence and Database Solutions and platforms. <br>• In-depth knowledge on Tableau Desktop, Tableau Server and Tableau Viewer. <br>• End to end experience in designing and deploying data visualizations using Tableau <br>• Mastered the ability to design and deploy rich Graphic visualizations with Filters, Drill Downs and Parameters. <br>• Created side by side bars, Scatter Plots, Stacked Bars, Heat Maps, Filled Maps and Symbol Maps according to deliverable specifications. <br>• Provided technical and analytical expertise in responding to complex, specialized report requests requiring complex business logics and detail-level data analysis. <br>• Extensive experience in troubleshooting, performance tuning and supporting business applications and reporting solutions. <br>• Experience in Tableau software Installation in Single and clustered environments. <br>• Hands on experience in configuration and administration of Tableau Server <br>• Involved in Tableau platform upgrades <br>• Provided 24X7 Operational/Application support for business critical applications. <br>• Established Standards and Best Practices for Enterprise Tableau Environment, application onboarding and development processes. <br>• Extensive working experience in writing Packages, Procedures and Functions. <br>• Strong team member with good communication and analytical skills. <br>• In-depth knowledge in formal Systems analysis design, implementation of Operational/Database systems, utilizing Oracle RDBMS Methodologies. <br>• Excellent problem solving, troubleshooting and decision-making skills.</p>
<p id="relocation_status">Willing to relocate: Anywhere</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeZShLkG8-ikkVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Tableau Developer</p>
<div class="work_company" itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
<span class="bold" itemprop="name">Cars.com</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2015 to Present</p>
<p class="work_description">Cars.com is a website which was launched in June 1998. It is the second largest automotive classified site, second to only Autotrader.com. Its headquarters are located in Chicago, Illinois.As a Consultant to IBM US GBS team my work involved creating daily, weekly &amp; monthly reports based on product type, product family, costs, consumer, geographic region etc. <br> <br>Responsibilities: <br>• Interact with business users to identify dashboard strategies, report layouts, and analysis presentations <br>• Deliver reports to business teams in a timely manner <br>• Customized Dashboards &amp;Stories based on cities, counties &amp; geographic regions in parts of North America, Europe &amp;Australia to analyze market trends. <br>• Applied Tableau Server &amp; Tableau Desktop integration to Dashboards &amp; Stories. <br>• Created variable sales targets, overhead costs &amp; fixed costs as parameters to be used by End User. <br>• Created User friendly Quick Filters such as radio buttons for new and used, drop down menus for months etc. on the dashboard. <br>• Using combination Histograms, Waterfall charts Box Plots etc. for better visual analysis. <br>• Worked with Business Partners when developing technical requirements and design. <br>• Created mockups, prototypes, design, interface, system, and user documentation. <br>• Updating Tableau from version 8 to version 9. <br>• Used research and business feedback to iteratively refine, and evolve the dashboard. <br>• Create and maintain software documentation <br>• Fix bugs, defect resolution and triaging. <br> <br>Environment: Tableau 8/9, Tableau Server, Tableau Desktop, JavaScript, Microsoft Xcel, Notepad++.</p>
</div></div>
<div id="workExperience-EeZShLkG8-mkkVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Tableau Developer</p>
<div class="work_company">
<span class="bold">State Street</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>Boston, MA</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">December 2013 to January 2015</p>
<p class="work_description">State Street Bank is the investment banking division and it provides financing and risk management services to large companies, institutions and government clients. State Street is one of the world's largest managers of institutional assets; Global Advisors manages more than $2 trillion in assets worldwide. <br> <br>Responsibilities: <br>• Worked in Global Markets repository team and created BI solutions to business users. <br>• Crated Dashboards/Reports for different investment portfolios. <br>• Involved in creating dashboards by extracting data from different sources (Data Blending). <br>• Created interactive dashboard and applied filter, highlight and URL actions to the dashboard. <br>• Responsible for creating calculated fields, LOD Calculations, bins, sets, geo coding and hierarchies. <br>• Performed type conversion functions when connected to relational data sources. <br>• Ability to authoring on Tableau server by changing mark types and adding filters. <br>• Administered user, user groups, and scheduled instances for reports in Tableau. <br>• Monitored and maintained incremental refreshes for data sources on Tableau server. <br>• Configured Cache settings which improved the performance of the reports and Dashboards. <br>• Participated in meetings, reviews, and user group discussions as well as communicating with stakeholders and business groups. <br>• Designed and developed ETL processes using SSIS to receive customer files from data warehouses and load them into transaction processing systems. <br>• Involved in Installation, Configuration and Deployment of SSRS Reports using different topologies and used Process Explorer for maintenance of stuck process, and to release the handles on files <br>• Developed interactive dashboards in Tableau Desktop and published them on to Tableau Server which allowed end users to understand the data on the fly with the usage of quick filters for on demand needed information. <br>• Scheduled data refresh on Tableau Server for weekly and monthly increments based on business changes to ensure that the views and dashboards displayed the changed data accurately. <br>• Involved in reviewing business requirements and analyzing data sources from Excel/Oracle, SQL Server for design, development, testing, and production rollover of reporting and analysis projects within Tableau Desktop. <br>• Converted charts into Crosstabs for further underlying data analysis in MS Excel. <br>• Reviewed basic SQL queries and altered for better performance in Tableau Desktop. <br>• Tested dashboards to ensure data was matching as per the business requirements and if there were any changes in underlying data. <br> <br>Environment: Tableau Desktop/Server 7,8,9,  MS SQL Server, Oracle, Teradata, Salesforce, Windows 8, MS Excel/Access, SSIS, SSRS.</p>
</div></div>
<div id="workExperience-EeZShLkG8-qkkVn7OsNiYg" class="work-experience-section "><div class="data_display">
<p class="work_title title">Tableau Developer</p>
<div class="work_company"><span class="bold">Cerner Corp</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">June 2013 to November 2013</p>
<p class="work_description">Cerner is intelligent solutions for the health care industry. Cerner connect people and systems at more than 18,000 facilities worldwide, and wide range of services support the clinical, financial and operational needs of organizations of every size. <br> <br>Responsibilities: <br>• Worked and partnered with business and IT teams in various areas to develop or maintained reports and addressed data needs to provide reporting solutions. <br>• Independently worked on owning IT support tasks related to Tableau Reports on Server. <br>• Built and maintained many dashboards. <br>• Conducted Knowledge transfer session to the employees on Tableau Software. <br>• Created dataflow diagram and documented the whole data movement process. <br>• Responsible for writing complete requirement document by interacting with the business directly to ascertain the business rules and logic. Writing high level and detail design documents. <br>• Worked on the requirement gathering of the reports <br>• Analyzed the database tables and created database views based on the columns needed. <br>• Created dashboards displaying sales, variance of sales between planned and actual values. <br>• Implemented calculations and parameters wherever needed. <br>• Published dashboards to Tableau server. <br>• Created Users, Adding Users to a Site, Adding Users to a Group Viewing, Editing &amp; Deleting Users and Activating their Licenses, Distributed Environments, Installing Worker Servers, Maintaining a Distributed Environment, High Availability. <br>• Impersonating with a Run as User Account, Impersonating with Embedded SQL Credentials. <br>• Good Knowledge on TABCMD &amp; TABADMIN, Database Maintenance and Troubleshooting. <br>• Strong ability in defining query for generating drill down reports, handling parameterized reports and creating stylish report layouts in SSRS. <br>• Scheduled Jobs for executing the stored SSIS packages which were developed to update the database on Daily basis. <br> <br>Environment: Tableau Desktop 6,7,8, Tableau Server, MySQL, Microsoft SQL Server, Windows, Microsoft Visual Studio, ERWIN, SSIS and SSRS.</p>
</div></div>
<div id="workExperience-EeZShLkG8-ukkVn7OsNiYg" class="work-experience-section last"><div class="data_display">
<p class="work_title title">BI Developer</p>
<div class="work_company">
<span class="bold">Development Bank Of Singapore</span> <div class="separator-hyphen">-</div> <div class="inline-block"><span>IN</span></div>
</div>
<div class="separator-hyphen">-</div>
<p class="work_dates">February 2011 to April 2013</p>
<p class="work_description">Development Bank Of Singapore provides different financial services. The scope of the project that I worked on was to facilitate in the maintenance and development of the application, which supported personal banking on wealth management, asset management dealing with different types of accounts and services. <br> <br>Responsibilities: <br>• Worked on Full life cycle development (SDLC) involved in all stages of development <br>Created different types of reports in SSRS such as parameterized and cascaded parameterized reports. <br>• Migrated data from Heterogeneous data Sources and legacy system (DB2, Access, Excel) to centralized SQL Server databases using SQL Server Integration Services (SSIS) to overcome transformation constraints. <br>• Worked on different types of reports in SSRS like Drill Down and Drill Through. <br>• Wrote complex queries, CTE's (Common table expressions), stored procedures, functions and views to ensure data integrity, data consistency and to meet the business rules. <br>• Created configuration Tables, Stored Procedure, User Defined Function in SQL Server Management Studio to support the whole SSIS process workflow. <br>• Created OLAP Cubes from the integrated data and generated reports on those cubes. <br>• Designed and developed various Service Management Reports, utilizing Dynamic and Cascading Prompts, sub-reports, charts, parameterized reports, conditional and dynamic reports. <br>• Designed, deployed, and maintained of various SSRS Reports in SQL Server 2008. <br>• Wrote Parameterized Queries for generating Tabular reports using Global variables, Expressions, and Functions, Sorting the data, Defining Data sources and Subtotals for the reports using SSRS 2005. <br>• Designed reports using SQL Server Reporting Services (SSRS) and Excel Pivot table based on OLAP cubes which make use of multiple value selection in parameters pick list, cascading prompts, matrix dynamics reports. <br> <br>Environment: MS SQL Server 2008, T-SQL, SQL Server Reporting Services (SSRS), SQL Server Integration Services (SSIS), MS Access, Windows</p>
</div></div>
</div>
</div>
<div class="section-item skills-content">
<div><div class="section_title"><h2>Skills</h2></div></div>
<div id="skills-items" class="items-container"><div class="data_display"><div class="skill-container resume-element"><span class="skill-text">Tableau (5 years)</span></div></div></div>
</div>
<div class="section-item links-content">
<div><div class="section_title"><h2>Links</h2></div></div>
<div id="link-items" class="items-container"><div id="link-EeZShLkG8-ekkVn7OsNiYg" class="link-section last"><div class="data_display"><p class="link_url"><a rel="nofollow" target="_blank" href="http://www.indeed.com/url?q=http%3A%2F%2FIllinois.As&amp;h=29cb6567">http://Illinois.As</a></p></div></div></div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>TECHNICAL SKILLS: <br> <br>Database           Teradata, MS SQL Server […] DB2, Oracle <br>Languages          SQL, PL/SQL, Teradata-BTEQ, TPT <br>Reporting Tools    Tableau […] Tableau 9.0 <br>BI Tools           Tableau, Business Intelligence Development Studio (SSIS/SSRS) <br>Domains            Financial, Banking, Retail <br>Web Tools          HTML, DHTML, XML <br>SQL Server Tools   SQL server Management Studio,  SQL server Query Analyzer, SQL server mail service , DBCC, BCP , SQL server profiler <br>Others             MS Word, Excel, VISIO 2007</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Faddc0a73570845e2/pdf</pdf_download_link>
</record>
</page>
<page>
<pageurl>http://www.indeed.com/r/ad0097f6278aeab9?hl=en&amp;co=US</pageurl>
<record>
<uniq_id>3a9a083b374c4a57a53fb79aae43c850</uniq_id>
      <url>http://www.indeed.com/r/ad0097f6278aeab9?hl=en&amp;co=US</url>
      <zipcode>60645</zipcode>
      <name>Job Seeker</name>
      <raw_html><![CDATA[<div id="resume_body" class="vcard single_form-content">
<div id="basic_info_row" class="last basicInfo-content"><div id="basic_info_cell" class="data_display">
<h1 id="resume-contact" class="fn anonymous" itemprop="name">Job Seeker</h1>
<div id="contact_info_container">
<div class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><p id="headline_location" class="locality" itemprop="addressLocality">Chicago, IL</p></div>
<div class="separator-hyphen">-</div>
</div>
<p id="res_summary" class="summary">Obtain a faculty position through which I may not only promote academic excellence in the field <br>of Mathematics, but also cultivate positive growth in nonacademic aspects in my students' lives.</p>
<p id="relocation_status">Willing to relocate: Anywhere</p>
<p id="employment_eligibility">Authorized to work in the US for any employer</p>
</div></div>
<div class="section-item workExperience-content">
<div><div class="section_title"><h2>Work Experience</h2></div></div>
<div id="work-experience-items" class="items-container">
<div id="workExperience-EeYriwO_XNuPUcl4KpgZYQ" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">Chicago Waldorf School</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2014 to 2015</p>
<p class="work_description">Taught a lab based physics class recreating Galileo's experiments that lead to the laws of motion <br>culminating with calculations/predictions/experiments involving a 2 dimensional rocket launch. <br>•   Taught a high school level Projective Geometry course. Independently contacted experts in this field to further develop and refine the curriculum. <br>•   Taught standard Math track classes Algebra 1, Precalculus, and Calculus.</p>
</div></div>
<div id="workExperience-EeYriwO_g-yPUcl4KpgZYQ" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">South Loop Elementary</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2013 to 2014</p>
<p class="work_description">2013-2014 <br>•   70% of my students met or exceeded NWEA goal targets <br>•   Performed individualized interventions with small groups of students requiring support based on data. <br>•   Tracked, catalogued, and adjusted interventions based on student outcomes. <br>•   Taught Algebra and Geometry to students in CPS's gifted program.</p>
</div></div>
<div id="workExperience-EeYriwO_qv2PUcl4KpgZYQ" class="work-experience-section "><div class="data_display">
<div class="work_company"><span class="bold">Holden Elementary</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2012 to 2013</p>
<p class="work_description">•   70% of my students met or exceeded NWEA goal targets <br>•   Used data gathered from assessments to effectively group students <br>•   Taught a diverse class grouped by RIT scores focusing on strengthening skills <br>•   Created supplemental materials to align curriculum with common core and to engage/challenge students</p>
</div></div>
<div id="workExperience-EeYriwO_qv6PUcl4KpgZYQ" class="work-experience-section last"><div class="data_display">
<div class="work_company"><span class="bold">Josephinum High School</span></div>
<div class="separator-hyphen">-</div>
<p class="work_dates">2012 to 2012</p>
<p class="work_description">school year <br>•   Taught Algebra 1, Algebra 2, and Precalculus/Trigonometry classes. <br>•   Assessed entire student body and then created a custom curriculum aligned with state standards, school <br>goals that also cultivated critical thinking abilities. <br>•   Planned lessons designed to engage students by targeting a variety of learning styles. <br>•   Ran an after school ACT prep course centered on general test taking strategies as well as Mathematics, <br>Science, and English specific content.</p>
</div></div>
</div>
</div>
<div class="section-item education-content">
<div><div class="section_title"><h2>Education</h2></div></div>
<div id="education-items" class="items-container">
<div id="education-EeYriwO_0g-PUcl4KpgZYQ" class="education-section "><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Master of Arts in Distinction</p>
<div class="edu_school"><span class="bold" itemprop="name">DePaul University</span></div>
<p class="edu_dates">March 2012</p>
</div></div>
<div id="education-EeYriwO_0hGPUcl4KpgZYQ" class="education-section last"><div class="data_display" itemprop="alumniOf" itemscope itemtype="http://schema.org/EducationalOrganization">
<p class="edu_title">Bachelor of Science in Mathematics</p>
<div class="edu_school">
<span class="bold" itemprop="name">University of Illinois at Chicago</span> <div class="separator-hyphen">-</div> <div class="inline-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Chicago, IL</span></div>
</div>
<p class="edu_dates">April 2007</p>
</div></div>
</div>
</div>
<div class="section-item additionalInfo-content">
<div><div class="section_title"><h2>Additional Information</h2></div></div>
<div id="additionalinfo-items" class="items-container"><div id="additionalinfo-section" class="last"><div class="data_display"><p>Special Skills and Interests <br>●   I have been a lifelong basketball player and fan. I enjoy most major sports, but basketball is my favorite <br>and I still currently play it. I understand the intrinsic emotional benefits as well as the obvious physical <br>benefits of athletics and would enjoy the chance to coach any sport. <br>●   I embrace the opportunity to positively impact students by leading clubs and extracurricular activities. I <br>personally have experience with chess, forensics, cultural, technology, and current events. I have a broad <br>enough background that i feel comfortable in any recreational themed club that may arise.</p></div></div></div>
</div>
</div>]]></raw_html>
      <pdf_download_link>/resumes/account/login?dest=%2Fr%2Fad0097f6278aeab9/pdf</pdf_download_link>
</record>
</page>
</root>`;

export default CVdata;
