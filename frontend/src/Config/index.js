const config = [
  {
    value: "skill_extraction",
    label: "Skill extraction",
    tasks: [
      {
        value: "phrase",
        label: "Phrase",
        isFixed: true,
        annotation_labels: [
          {
            name: "Skill core",
            colorTheme: {
              fontColor: "rgb(40, 40, 40)",
              background: "rgba(253, 192, 134, 1)",
            },
          },
          {
            name: "Experience",
            colorTheme: {
              fontColor: "black",
              background: "#f6e8c3",
            },
          },
        ],
        key_words: [
          { value: "skills", label: "Skills" },
          { value: "technical", label: "Technical" },
        ],
      },
      {
        value: "phrase2",
        label: "Phrase2",
        isFixed: false,
        annotation_labels: [
          {
            name: "Phrase",
            colorTheme: {
              fontColor: "white",
              background: "rgba(166, 189, 219, 1)",
            },
          },
        ],
        key_words: [
          { value: "skills", label: "Skills" },
          { value: "technical", label: "Technical" },
        ],
      },
      {
        value: "core",
        label: "Core",
        isFixed: false,
        annotation_labels: [
          {
            name: "Core",
            colorTheme: {
              fontColor: "white",
              background: "#CC3333",
            },
          },
        ],
        key_words: [
          { value: "skills", label: "Skills" },
          { value: "technical", label: "Technical" },
        ],
      },
    ],
  },
  {
    value: "job_description",
    label: "Job Description",
    tasks: [
      {
        value: "title",
        label: "Title",
        isFixed: true,
        annotation_labels: [
          {
            name: "Title",
            colorTheme: {
              fontColor: "black",
              background: "rgba(253, 192, 134, 1)",
            },
          },
        ],
        key_words: [{ value: "title", label: "Title" }],
      },
      {
        value: "requirements",
        label: "Requirements",
        isFixed: false,
        annotation_labels: [
          {
            name: "Requirements",
            colorTheme: {
              fontColor: "black",
              background: "rgba(166, 189, 219, 1)",
            },
          },
        ],
        key_words: [{ value: "requirements", label: "Requirements" }],
      },
    ],
  },
];

export default config;
