import React from "react";
import { makeStyles } from "@material-ui/core";
import { Button } from "@material-ui/core/";
import setCurrentLabelAction from "../../store/actions/setCurrentLabelAction";
import { useDispatch } from "react-redux";

const useStyles = makeStyles({
  buttonStyle: {
    paddingLeft: "15px",
    paddingRight: "15px",
    letterSpacing: "4px",
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "normal",
    color: (props) =>
      props.index === props.buttonIndex ? props.value.fontColor_hex : "white",
    backgroundColor: (props) =>
      props.index === props.buttonIndex
        ? props.value.background_color_hex
        : "Transparent",
    "&:hover": {
      color: (props) => props.value.fontColor_hex,
      background: (props) => props.value.background_color_hex,
    },
    "&:focus": {
      outline: "none",
    },
    textTransform: "none",
  },
});

export default function ToggleButton({ children, ...props }) {
  const dispatch = useDispatch();

  const onClick = () => {
    props.setButtonIndex(props.index);
    dispatch(setCurrentLabelAction(props.value));
  };

  const { buttonStyle } = useStyles(props);
  return (
    <Button className={buttonStyle} onClick={onClick}>
      {children}
    </Button>
  );
}
