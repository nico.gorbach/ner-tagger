import React, { useState, useRef } from "react";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import {
  ButtonGroup,
  Popper,
  Paper,
  Grow,
  ClickAwayListener,
  Grid,
  MenuList,
} from "@material-ui/core/";
import { MUIMenuItem, MUIMenuButton } from "../../styles/MUI/Menu";
import { MUIButton } from "../../styles/MUI/Buttons";

export default function CustomButtonGroup(props) {
  const { annotationLabels, dispatch, setCurrentLabelAction } = props;
  const options = annotationLabels.map((label) => label.name);
  const optionsColorTheme = annotationLabels.map((label) => ({
    color: label.font_color_hex,
    backgroundColor: label.background_color_hex,
  }));

  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);
  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleMenuItemClick = (_, index) => {
    setSelectedIndex(index);
    dispatch(setCurrentLabelAction(annotationLabels[index]));
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <Grid container direction="column" style={{ marginLeft: "0px" }}>
      <Grid item xs={12}>
        <ButtonGroup
          variant="contained"
          ref={anchorRef}
          aria-label="split button"
        >
          <MUIMenuButton
            backgroundcolor={
              annotationLabels.length > 0
                ? annotationLabels[selectedIndex].background_color_hex
                : null
            }
            color={
              annotationLabels.length > 0
                ? annotationLabels[selectedIndex].font_color_hex
                : null
            }
          >
            {options[selectedIndex]}
          </MUIMenuButton>
          <MUIButton
            // color="secondary"
            size="small"
            aria-controls={open ? "split-button-menu" : undefined}
            aria-expanded={open ? "true" : undefined}
            aria-label="select merge strategy"
            aria-haspopup="menu"
            onClick={handleToggle}
            backgroundcolor={
              annotationLabels.length > 0
                ? annotationLabels[selectedIndex].background_color_hex
                : null
            }
            color={
              annotationLabels.length > 0
                ? annotationLabels[selectedIndex].fontColor_hex
                : null
            }
          >
            <ArrowDropDownIcon style={{ fill: "black" }} />
          </MUIButton>
        </ButtonGroup>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList id="split-button-menu">
                    {options.map((option, index) => (
                      <MUIMenuItem
                        key={index}
                        index={index}
                        option={option}
                        selectedindex={selectedIndex}
                        onClick={(event) => handleMenuItemClick(event, index)}
                        color={optionsColorTheme[index].color}
                        backgroundcolor={
                          optionsColorTheme[index].backgroundColor
                        }
                      >
                        {option}
                      </MUIMenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Grid>
    </Grid>
  );
}
