import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  MainContainer,
  LeftSection,
  RightSection,
  RightSubSection,
} from "../../styles/Header";
import NextDocButtons from "./NextDocButtons";
import AnnotationLabels from "./AnnotationLabels";
import CustomParticles from "../Particles";

export default function Header() {
  return (
    <MainContainer
      className={window.innerWidth < 800 ? "small-window-width" : null}
    >
      <LeftSection />
      <RightSection>
        {/* <CustomParticles
          width={"80%"}
          height={"80px"}
          particleColor={"#ffffff"}
          numParticles={10}
          size={"5"}
        /> */}
        <RightSubSection>
          <AnnotationLabels />
          <NextDocButtons />
        </RightSubSection>
      </RightSection>
    </MainContainer>
  );
}
