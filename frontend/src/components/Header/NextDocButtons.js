import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "react-avatar";
import { MUIIconButton } from "../../styles/MUI/Buttons";
import { withStyles } from "@material-ui/core/styles";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import ExitToAppOutlined from "@material-ui/icons/ExitToAppOutlined";
import AssessmentOutlinedIcon from "@material-ui/icons/AssessmentOutlined";
import { logoutAction } from "../../store/actions/loginAction";
import { useHistory } from "react-router-dom";
import { Tooltip, Zoom } from "@material-ui/core";

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(() => ({
  root: {
    "&:hover": {
      backgroundColor: "rgba(92,184,181,0.2)",
    },
  },
}))(MenuItem);

export default function NextDocButtons() {
  const dispatch = useDispatch();
  const history = useHistory();

  const user = useSelector((state) => state.auth.user);

  const logoutHandler = () => {
    dispatch(logoutAction());
    history.push({
      pathname: "/login",
      state: { from: "/" },
    });
  };

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Tooltip
        title={
          <span style={{ fontSize: "12px", letterSpacing: "3px" }}>Logout</span>
        }
        TransitionComponent={Zoom}
        placement="bottom"
        enterDelay={2000}
        leaveDelay={0}
        enterNextDelay={2000}
      >
        <MUIIconButton onClick={handleClick}>
          <Avatar
            name={
              user
                ? user.first_name !== ""
                  ? `${user.first_name} ${user.last_name}`
                  : user.username
                : null
            }
            size={45}
            textSizeRatio={2.5}
            round="100px"
            color="#8856a7"
            fgColor="rgb(240,240,240)"
          />
        </MUIIconButton>
      </Tooltip>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem onClick={logoutHandler}>
          <ExitToAppOutlined fontSize="small" style={{ marginRight: "15px" }} />
          <ListItemText primary="Logout" />
        </StyledMenuItem>
      </StyledMenu>
    </>
  );
}
