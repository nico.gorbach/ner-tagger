import React from "react";
import { useSelector, useDispatch } from "react-redux";
import setCurrentLabelAction from "../../store/actions/setCurrentLabelAction";
import CustomButtonGroup from "./CustomButtonGroup";
import LocalOfferOutlinedIcon from "@material-ui/icons/LocalOfferOutlined";
import { Tooltip, Zoom } from "@material-ui/core";

export default function AnnotationLabels() {
  const dispatch = useDispatch();

  const annotationLabels = useSelector(
    (state) => state.settings.config.annotationLabels
  );

  return (
    <Tooltip
      title={
        <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
          Annotation labels
        </span>
      }
      TransitionComponent={Zoom}
      placement="right"
      enterDelay={2000}
      leaveDelay={0}
      enterNextDelay={2000}
      disableFocusListener
      disableTouchListener
    >
      <span
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <LocalOfferOutlinedIcon
          fontSize={"default"}
          style={{
            marginLeft: "50px",
            marginRight: "20px",
            fill: "rgb(240,240,240)",
          }}
          data-tip
          data-for="annotation-label-tip"
        />
        <CustomButtonGroup
          annotationLabels={annotationLabels}
          dispatch={dispatch}
          setCurrentLabelAction={setCurrentLabelAction}
          data-tip
          data-for="annotation-label-tip"
        />
      </span>
    </Tooltip>
  );
}
