import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles({
  menuItem: {
    color: (props) => props.color,
    backgroundColor: (props) =>
      props.selected ? `${props.background} !important` : "white ! important",
    "&:hover": { backgroundColor: (props) => `${props.background} !important` },
    letterSpacing: "4px",
    fontFamily: "Roboto",
  },
});

export default function CustomMenuItem(props) {
  const { option, index, selectedIndex, handleMenuItemClick, children } = props;

  const selected = index === selectedIndex;
  const classes = useStyles({ ...props.colorTheme, selected });

  return (
    <MenuItem
      key={option}
      selected={true}
      onClick={(event) => handleMenuItemClick(event, index)}
      className={classes.menuItem}
    >
      {children}
    </MenuItem>
  );
}
