import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { useSelector } from "react-redux";
import Avatar from "react-avatar";
import { ReactComponent as TrophyIcon } from "../../assets/trophy4.svg";
import { ReactComponent as GoldMedalIcon } from "../../assets/gold-medal.svg";
import { ReactComponent as SilverMedalIcon } from "../../assets/silver-medal.svg";
import { ReactComponent as BronzMedalIcon } from "../../assets/bronz-medal.svg";
import Box from "@material-ui/core/Box";
import { Paper, Grow, Tooltip, Zoom } from "@material-ui/core";
import ProgressBar from "react-customizable-progressbar";
import { ProgressIndicator } from "../../styles/ProgressBar";
import { makeStyles } from "@material-ui/styles";

const avatarColors = [
  "#377eb8",
  "#4daf4a",
  "#984ea3",
  "#ff7f00",
  "#a65628",
  "#f781bf",
  "#999999",
  "#e41a1c",
];
function GrowTransition(props) {
  return <Grow {...props} direction="up" />;
}

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: "0px",
    top: "0px",
    color: "white",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const useStyles = makeStyles(() => ({
  tooltip: {
    maxWidth: "150px",
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function Leaderboard(props) {
  const classes = useStyles();
  const { isOpen, setIsOpen } = props;

  const annotDistrib = useSelector((state) => state.annot.annotDistrib);
  const loggedinUsername = useSelector((state) => state.auth.user.username);

  const annotDistrib2 = annotDistrib.map((user) => {
    const percentageSubmitted =
      user.amount_submitted < user.amount_docs
        ? Math.round((user.amount_submitted / user.amount_docs) * 100)
        : 100;

    return { ...user, percentageSubmitted };
  });

  return (
    <div>
      <Dialog
        aria-labelledby="customized-dialog-title"
        open={isOpen}
        TransitionComponent={GrowTransition}
        maxWidth={650}
        disableBackdropClick={false}
        style={{ userSelect: "none" }}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={() => setIsOpen(false)}
          style={{ backgroundColor: "#5cb8b5" }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              padding: "0px 10px",
            }}
          >
            <TrophyIcon height={30} width={30} fill="white" />
            <span
              style={{
                color: "white",
                fontSize: "24px",
                fontWeight: "500",
                letterSpacing: "4px",
                marginLeft: "40px",
              }}
            >
              Leaderboard
            </span>
          </div>
        </DialogTitle>
        <DialogContent
          dividers
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-around",
            maxHeight: "400px",
            overflow: "scroll",
            padding: "0px 0px 0px 0px",
            backgroundColor: "rgb(240,240,240)",
            display: "block",
          }}
        >
          <Paper
            elevation={0}
            style={{
              backgroundColor: "Transparent",
              display: "inline-block",
            }}
          >
            {annotDistrib2
              .sort(
                (user_a, user_b) =>
                  user_b.amount_submitted - user_a.amount_submitted
              )
              .map((user, index) => (
                <Box
                  boxShadow={0}
                  style={{
                    width: "650px",
                    height: "80px",
                    display: "flex",
                    alignItems: "center",
                    padding: "10px 0px",
                    margin: "0px 0px",
                    fontWeight: "500",
                    backgroundColor: index % 2 == 0 ? "#f3f6f9" : "white",
                    letterSpacing: "2px",
                  }}
                >
                  <Tooltip
                    title={
                      <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                        Rank
                      </span>
                    }
                    TransitionComponent={Zoom}
                    placement="bottom"
                    enterDelay={0}
                    leaveDelay={0}
                  >
                    <span
                      style={{
                        color:
                          user.username === loggedinUsername
                            ? "#798687"
                            : "#d0d4d9",
                        fontSize: "20px",
                        marginLeft: "30px",
                        cursor: "default",
                      }}
                    >
                      {index + 1}
                    </span>
                  </Tooltip>
                  <Avatar
                    name={
                      user.first_name !== ""
                        ? `${user.first_name} ${user.last_name}`
                        : user.username
                    }
                    size={45}
                    textSizeRatio={2.2}
                    round="100px"
                    color="#8856a7"
                    color={avatarColors[index]}
                    fgColor="rgb(240,240,240)"
                    style={{ marginLeft: "25px", cursor: "default" }}
                  />
                  <span
                    style={{
                      fontSize: "18px",
                      fontWeight:
                        user.username === loggedinUsername ? "800" : "400",
                      color: "#798687",
                      marginLeft: "20px",
                      cursor: "default",
                    }}
                  >
                    {user.first_name !== "" ? user.first_name : user.username}
                  </span>
                  <span
                    style={{
                      fontSize: "18px",
                      fontWeight:
                        user.username === loggedinUsername ? "800" : "400",
                      color: "#798687",
                      marginLeft: "auto",
                      marginRight: "0px",
                      cursor: "default",
                    }}
                  >
                    {user.amount_text_annot_by_label
                      .sort((a, b) => b.count - a.count)
                      .slice(0, 2)
                      .map((label, index) => (
                        <Tooltip
                          title={
                            <span
                              style={{ fontSize: "12px", letterSpacing: "3px" }}
                            >
                              {label.annotation_label__name}
                            </span>
                          }
                          TransitionComponent={Zoom}
                          placement="bottom"
                          enterDelay={0}
                          leaveDelay={0}
                        >
                          <span
                            key={index}
                            style={{
                              fontSize: "18px",
                              color: "#798687",
                              marginLeft: "20px",
                              cursor: "default",
                            }}
                          >
                            {label.count}
                          </span>
                        </Tooltip>
                      ))}
                  </span>
                  <Tooltip
                    classes={{ tooltip: classes.tooltip }}
                    title={
                      <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                        Unannotated docs submitted
                      </span>
                    }
                    TransitionComponent={Zoom}
                    placement="bottom"
                    enterDelay={0}
                    leaveDelay={0}
                  >
                    <span
                      key={index}
                      style={{
                        fontSize: "18px",
                        color: "#798687",
                        marginLeft: "20px",
                        cursor: "default",
                        fontWeight:
                          user.username === loggedinUsername ? "800" : "400",
                      }}
                    >
                      {user.amount_unannotated_submitted}
                    </span>
                  </Tooltip>
                  <Tooltip
                    classes={{ tooltip: classes.tooltip }}
                    title={
                      <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                        Annotated docs submitted
                      </span>
                    }
                    TransitionComponent={Zoom}
                    placement="bottom"
                    enterDelay={0}
                    leaveDelay={0}
                  >
                    <span
                      key={index}
                      style={{
                        fontSize: "18px",
                        color: "#798687",
                        marginLeft: "20px",
                        marginRight: "30px",
                        cursor: "default",
                        fontWeight:
                          user.username === loggedinUsername ? "800" : "400",
                      }}
                    >
                      {user.amount_submitted -
                        user.amount_unannotated_submitted}
                    </span>
                  </Tooltip>
                  <span
                    style={{
                      marginLeft: "0px",
                      marginRight: "30px",
                    }}
                  >
                    <ProgressBar
                      steps={100}
                      progress={user.percentageSubmitted}
                      radius={20}
                      trackStrokeWidth={5}
                      strokeWidth={5}
                      strokeColor="#798687"
                      trackStrokeColor="#d9d9d9"
                      trackColor={"green"}
                      initialAnimation={true}
                      initialAnimationDelay={1}
                      trackTransition={"0.1s ease"}
                      transition={"0.1s ease"}
                      style={{
                        display: "flex !important",
                        justifyContent: "center !important",
                        alignItems: "center !important",
                        backgroundColor: "purple !important",
                      }}
                    >
                      <Tooltip
                        title={
                          <span
                            style={{ fontSize: "12px", letterSpacing: "3px" }}
                          >
                            Progress
                          </span>
                        }
                        TransitionComponent={Zoom}
                        placement="bottom"
                        enterDelay={0}
                        leaveDelay={0}
                      >
                        <ProgressIndicator
                          className="indicator"
                          style={{
                            fontSize: "13px",
                            letterSpacing: "0.1px",
                            fontWeight:
                              user.username === loggedinUsername
                                ? "800"
                                : "400",
                          }}
                        >
                          {`${user.percentageSubmitted}%`}
                        </ProgressIndicator>
                      </Tooltip>
                    </ProgressBar>
                  </span>
                  {index === 0 ? (
                    <GoldMedalIcon
                      height={"35px"}
                      width={"40px"}
                      style={{ marginLeft: "0px", marginRight: "30px" }}
                    />
                  ) : index === 1 ? (
                    <SilverMedalIcon
                      height={"35px"}
                      width={"40px"}
                      style={{ marginLeft: "0px", marginRight: "30px" }}
                    />
                  ) : index >= 2 ? (
                    <BronzMedalIcon
                      height={"35px"}
                      width={"40px"}
                      style={{
                        marginLeft: "0px",
                        marginRight: "30px",
                        visibility: index > 2 ? "hidden" : "none",
                      }}
                    />
                  ) : null}
                </Box>
              ))}
          </Paper>
        </DialogContent>
      </Dialog>
    </div>
  );
}
