import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { useSelector } from "react-redux";
import { Grow } from "@material-ui/core";
import CanvasJSReact from "./canvasjs.react";
import EqualizerOutlinedIcon from "@material-ui/icons/EqualizerOutlined";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

function toolTipFormatter(e) {
  return `<strong>${e.entries[1].dataPoint.label}</strong> <br/>`.concat(
    e.entries
      .map(
        (entry) =>
          `<span style= "color: +
          ${entry.dataSeries.color} +
          ">
          ${entry.dataSeries.name}
          </span><strong>
          ${Math.round(entry.dataPoint.y, 2)}
          </strong> <br/>`
      )
      .join("")
  );
}

function toggleDataSeries(e) {
  if (typeof e.dataSeries.visible === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
}

function GrowTransition(props) {
  return <Grow {...props} direction="up" />;
}

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    display: "flex",
  },
  closeButton: {
    position: "absolute",
    right: "0px",
    top: "0px",
    color: "white",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <EqualizerOutlinedIcon
        fontSize="large"
        style={{ fill: "rgb(240,240,240)" }}
      />
      <Typography
        variant="h6"
        style={{
          color: "white",
          fontSize: "24px",
          fontWeight: "500",
          letterSpacing: "4px",
          marginLeft: "30px",
        }}
      >
        {children}
      </Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function Chart(props) {
  const { isOpen, setIsOpen } = props;

  let annotDistrib = useSelector((state) => state.annot.annotDistrib).slice();

  annotDistrib = annotDistrib.map((user) => ({
    ...user,
    amount_text_annot_by_label: [
      ...user.amount_text_annot_by_label,
      {
        annotation_label__name: "No label submissions",
        count: user.amount_unannotated_submitted,
      },
    ],
  }));

  const user = useSelector((state) => state.auth.user);
  const loggedinUserIndex = annotDistrib
    .map((item) => item.username)
    .indexOf(user.username);
  let loggedinUserAnnots = annotDistrib.splice(loggedinUserIndex, 1).pop();

  if (loggedinUserAnnots) {
    loggedinUserAnnots = loggedinUserAnnots.amount_text_annot_by_label;
  }

  const annotLabelNames = [
    ...new Set(
      annotDistrib
        .map((user) =>
          user.amount_text_annot_by_label.map(
            (label) => label.annotation_label__name
          )
        )
        .flat()
    ),
  ];

  const annotLabelCounts = annotLabelNames.map((annotation_label__name) => ({
    annotation_label__name,
    count:
      annotDistrib
        .filter((user) => user.amount_submitted !== 0)
        .reduce(
          (countTotal, user) =>
            countTotal +
            user.amount_text_annot_by_label
              .filter(
                (label) =>
                  label.annotation_label__name === annotation_label__name
              )
              .reduce((countLocal, item) => countLocal + item.count, 0) /
              user.amount_submitted,
          0
        ) / annotDistrib.length,
  }));

  const options = {
    animationEnabled: true,
    theme: "light2",
    title: {},
    axisX: {
      reversed: true,
    },
    axisY: {
      title: "Avg amount of annotations per doc",
      includeZero: true,
    },
    legend: {
      cursor: "pointer",
      itemclick: toggleDataSeries,
    },
    toolTip: {
      shared: true,
      content: toolTipFormatter,
    },
    data: [
      {
        type: "bar",
        name: user.first_name !== "" ? user.first_name : user.username,
        showInLegend: true,
        color: "#4393c3",
        dataPoints: loggedinUserAnnots
          ? annotLabelNames.map((label) => ({
              label,
              y: loggedinUserAnnots
                ? loggedinUserAnnots.filter(
                    (annotLabel) => annotLabel.annotation_label__name === label
                  ).length > 0
                  ? loggedinUserAnnots
                      .filter(
                        (annotLabel) =>
                          annotLabel.annotation_label__name === label
                      )
                      .pop().count / 10
                  : 0
                : 0,
            }))
          : null,
      },
      {
        type: "bar",
        name: "Other users (average)",
        showInLegend: true,
        color: "#cab2d6",
        dataPoints: annotLabelNames.map((label) => ({
          label,
          y: annotLabelCounts
            .filter((annotLabel) => annotLabel.annotation_label__name === label)
            .pop().count,
        })),
      },
    ],
  };

  return (
    <div>
      <Dialog
        aria-labelledby="customized-dialog-title"
        open={isOpen}
        TransitionComponent={GrowTransition}
        maxWidth={650}
        style={{ userSelect: "none" }}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={() => setIsOpen(false)}
          style={{
            backgroundColor: "#5cb8b5",
            width: "700px",
            color: "white",
            letterSpacing: "4px !important",
          }}
        >
          Label distribution
        </DialogTitle>
        <DialogContent dividers>
          <div>
            <CanvasJSChart options={options} />
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}
