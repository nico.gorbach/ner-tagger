import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import CustomParticles from "../Particles";
import { Logo } from "../../styles/Logo";
import logo from "../../assets/adeccoLogo.png";
import { makeStyles } from "@material-ui/styles";
import {
  Button,
  IconButton,
  InputAdornment,
  TextField,
} from "@material-ui/core/";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { login } from "../../store/actions/loginAction";
import { Checkbox, FormControlLabel, Snackbar, Slide } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";

const useStyles = makeStyles(() => ({
  notchedOutline: {
    borderWidth: "1px",
    borderColor: "#5cb8b5 !important",
  },
  label: {
    color: "red",
  },
  button: {
    "&:focus": {
      outlineColor: "#2c7fb8",
    },
  },
}));

export default function Login() {
  const dispatch = useDispatch();
  const classes = useStyles();

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");
  const [showpassword, setShowpassword] = useState(null);

  const [serverAlert, setServerAlert] = useState(null);

  const [rememberMe, setRememberMe] = useState(true);

  const is_authenticated = useSelector((state) => state.auth.is_authenticated);

  const LoginHandler = async (e) => {
    e.preventDefault();
    dispatch(login({ email, password, rememberMe })).then((resp) => {
      if (resp.statusText !== "OK") {
        try {
          setServerAlert(resp.statusText);
        } catch (err) {
          console.log(err);
        }
      }
    });
  };

  return is_authenticated ? (
    <Redirect to={{ pathname: "/" }} />
  ) : (
    <>
      <CustomParticles
        particleColor={"#5db3ae"}
        linksColor={"#b5e0de"}
        width={"100%"}
        height={"100%"}
        particleOpacity={"0.5"}
        linkOpacity={"0.5"}
        size={"5"}
        interactive={true}
      />
      <div
        style={{
          width: "100%",
          height: "100vh",
          position: "absolute",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Form
          onSubmit={LoginHandler}
          style={{
            height: "550px",
            width: "420px",
            display: "grid",
            gridTemplateRows: "8fr 0fr 3fr 3fr 0fr 1fr 2fr 1fr 1fr",
            gridTemplateColumns: "1fr 3fr 7fr 1fr",
            justifyItems: "center",
            zIndex: "10",
            borderRadius: "10px",
            border: "solid 0.5px #5cb8b5",
            backgroundColor: "rgb(253,253,253)",
          }}
        >
          <div
            style={{
              zIndex: "2",
              gridRow: "1 / span 1",
              gridColumn: "2 / span 2",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Logo src={logo} />
          </div>
          <Form.Group
            size="lg"
            controlId="email"
            style={{
              width: "100%",
              gridRow: "3 / span 1",
              gridColumn: "2 / span 2",
            }}
          >
            <TextField
              required={true}
              autoFocus={true}
              variant="outlined"
              label="Email address"
              fullWidth
              placeholder="Enter your email address"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              border={"solid 1px #5cb8b5"}
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
              }}
              InputLabelProps={{
                style: {
                  color: "rgb(150,150,150)",
                },
              }}
              style={{ backgroundColor: "white" }}
            />
          </Form.Group>
          <Form.Group
            size="lg"
            controlId="password"
            style={{
              width: "100%",
              gridRow: "4 / span 1",
              gridColumn: "2 / span 2",
              padding: 0,
            }}
          >
            <TextField
              required={true}
              autoFocus={false}
              variant="outlined"
              type={showpassword ? "text" : "password"}
              label="Password"
              value={password}
              fullWidth
              placeholder="Enter your password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onMouseDown={() => setShowpassword(true)}
                      onMouseUp={() => setShowpassword(false)}
                      onTouchDown={() => setShowpassword(true)}
                      onTouchUp={() => setShowpassword(false)}
                    >
                      {showpassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              InputLabelProps={{
                style: {
                  color: "rgb(150,150,150)",
                },
              }}
              style={{ backgroundColor: "white" }}
            />
          </Form.Group>
          <FormControlLabel
            control={
              <Checkbox
                checked={rememberMe}
                onChange={() => setRememberMe(!rememberMe)}
                value="checked1"
                color="primary"
              />
            }
            label="Remember me"
            style={{
              width: "150px",
              gridRow: "6 / span 1",
              gridColumn: "2 / span 1",
              display: "flex",
              justifyContent: "flex-start",
              margin: "0",
              padding: "0",
            }}
          />
          <Snackbar
            open={serverAlert}
            onClose={() => setServerAlert(false)}
            autoHideDuration={10000}
            TransitionComponent={(props) => (
              <Slide {...props} direction="down" />
            )}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <Alert severity="error">
              <AlertTitle>Login error</AlertTitle>
              {serverAlert}
            </Alert>
          </Snackbar>
          <Button
            style={{
              gridRow: "8 / span 1",
              gridColumn: "2 / span 2",
              backgroundColor: "#5cb8b5",
              border: "none",
              height: "55px",
              fontFamily: "Roboto",
              letterSpacing: "4px",
              width: "100%",
              color: "white",
            }}
            type={"submit"}
            className={classes.button}
          >
            Login
          </Button>
        </Form>
      </div>
    </>
  );
}
