import React from "react";
import ProgressBar from "react-customizable-progressbar";
import { ProgressContainer, ProgressIndicator } from "../../styles/ProgressBar";
import { Tooltip } from "../../styles/Tooltip";
import { useSelector } from "react-redux";

export default function Progress() {
  const doc = useSelector((state) => state.annot.doc);

  const {
    amount_assigned_to_loggedin_user,
    amount_submitted_by_loggedin_user,
  } = doc;

  const percentageSubmitted =
    amount_submitted_by_loggedin_user < amount_assigned_to_loggedin_user
      ? Math.round(
          (amount_submitted_by_loggedin_user /
            amount_assigned_to_loggedin_user) *
            100
        )
      : 100;

  return (
    <>
      <ProgressContainer>
        <ProgressBar
          steps={100}
          progress={percentageSubmitted}
          radius={40}
          trackStrokeWidth={10}
          strokeWidth={10}
          strokeColor="#798687"
          trackStrokeColor="#d9d9d9"
          trackColor={"green"}
          initialAnimation={true}
          initialAnimationDelay={1}
          trackTransition={"0.1s ease"}
          transition={"0.1s ease"}
        >
          <ProgressIndicator>{`${percentageSubmitted}%`}</ProgressIndicator>
        </ProgressBar>
      </ProgressContainer>
      <Tooltip
        id="progress-tip"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Progress
      </Tooltip>
    </>
  );
}
