import React from "react";
import { Section, Contents2 } from "../../../styles/Panel";
import SelectProject from "./ProjectAndTaskSettings/SelectProject";
import SelectTasks from "./ProjectAndTaskSettings/SelectTasks";
import SelectedSearchString from "./ProjectAndTaskSettings/SelectSearchString";

export default function Settings() {
  return (
    <Section>
      <Contents2>
        <SelectProject />
        <SelectTasks />
        <SelectedSearchString />
      </Contents2>
    </Section>
  );
}
