import React from "react";
import { Section, Seperator, Contents } from "../../../../styles/Panel";
import {
  ToggleButtonOnOffContainer,
  ToggleButtonOnOffSubContainer,
} from "../../../../styles/Buttons";
import { ReactComponent as SmartHighlightIcon } from "../../../../assets/marker2.svg";
import { ReactComponent as HighlightWhiteSpaceIcon } from "../../../../assets/marker3.svg";
import { Tooltip } from "../../../../styles/Tooltip";
import CustomOnOffTogggleButton from "./CustomToggleButton";
import { useSelector, useDispatch } from "react-redux";
import setDoSmartAnnotateAction from "../../../../store/actions/setDoSmartAnnotateAction";
import setDoSelectWhiteSpaceAction from "../../../../store/actions/setDoSelectWhiteSpaceAction";

export default function HighlightSettings() {
  const dispatch = useDispatch();
  const doSmartAnnotate = useSelector(
    (state) => state.settings.annot.doSmartAnnotate
  );
  const doSelectWhiteSpace = useSelector(
    (state) => state.settings.annot.doSelectWhiteSpace
  );

  return (
    <Section>
      <Seperator />
      <Contents>
        <ToggleButtonOnOffContainer data-tip data-for="smart-highlight-tip">
          <SmartHighlightIcon fill={"white"} height={"35px"} width={"35px"} />
          <ToggleButtonOnOffSubContainer>
            <CustomOnOffTogggleButton
              activeLabel={"smart"}
              inactiveLabel={"regular"}
              value={doSmartAnnotate}
              setValue={(val) => dispatch(setDoSmartAnnotateAction(val))}
            />
          </ToggleButtonOnOffSubContainer>
        </ToggleButtonOnOffContainer>
        <ToggleButtonOnOffContainer
          data-tip
          data-for="highlight-white-space-tip"
        >
          <HighlightWhiteSpaceIcon
            fill={"white"}
            height={"35px"}
            width={"35px"}
          />
          <ToggleButtonOnOffSubContainer>
            <CustomOnOffTogggleButton
              activeLabel={"space"}
              inactiveLabel={"no space"}
              value={doSelectWhiteSpace}
              setValue={(val) => dispatch(setDoSelectWhiteSpaceAction(val))}
            />
          </ToggleButtonOnOffSubContainer>
        </ToggleButtonOnOffContainer>
        <Tooltip
          id="smart-highlight-tip"
          place="right"
          effect="solid"
          multiline
          arrowColor={"Transparent"}
          className={"custom"}
          delayHide={500}
          delayShow={500}
        >
          Smart highlight
        </Tooltip>
        <Tooltip
          id="highlight-white-space-tip"
          place="right"
          effect="solid"
          multiline
          arrowColor={"Transparent"}
          className={"custom"}
          delayHide={500}
          delayShow={500}
        >
          Highlight white space
        </Tooltip>
      </Contents>
    </Section>
  );
}
