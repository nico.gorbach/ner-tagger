import React from "react";
import DropDownMenu from "./DropDownMenu";
import { Tooltip } from "../../../../styles/Tooltip";
import { useSelector, useDispatch } from "react-redux";
import setSelectedSearchStringsAction from "../../../../store/actions/setSelectedSearchStringsAction";

export default function SearchDoc() {
  const dispatch = useDispatch();

  const selectedSearchStrings = useSelector(
    (state) => state.settings.config.selectedSearchStrings
  );
  const searchStringOptions = useSelector(
    (state) => state.settings.config.searchStringOptions
  );

  return (
    <>
      <DropDownMenu
        value={selectedSearchStrings.map((item) => ({
          ...item,
          value: item.label,
        }))}
        options={searchStringOptions.map((item) => ({
          ...item,
          value: item.label,
        }))}
        onChange={(val) => dispatch(setSelectedSearchStringsAction(val ?? []))}
        placeholder={"Search Strings"}
        isMulti
        // isSearchable
        isClearable={
          !searchStringOptions.reduce(
            (acc, currentValue) => acc || !currentValue.optional,
            false
          )
        }
        data-tip
        data-for="search-doc-tip-off"
      />
      <Tooltip
        id="search-doc-tip-off"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Highlight key words
      </Tooltip>
    </>
  );
}
