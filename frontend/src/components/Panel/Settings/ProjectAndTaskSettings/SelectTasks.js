import React from "react";
import DropDownMenu from "./DropDownMenu";
import { Tooltip } from "../../../../styles/Tooltip";
import { useSelector, useDispatch } from "react-redux";
import setSelectedTasksAction from "../../../../store/actions/setSelectedTasksAction";

export default function SelectTasks() {
  const dispatch = useDispatch();

  const selectedTasks = useSelector(
    (state) => state.settings.config.selectedTasks
  );
  const taskOptions = useSelector((state) => state.settings.config.taskOptions);

  return (
    <>
      <DropDownMenu
        value={selectedTasks.map((item) => ({ ...item, value: item.label }))}
        options={taskOptions.map((item) => ({ ...item, value: item.label }))}
        onChange={(val) => dispatch(setSelectedTasksAction(val ?? []))}
        isMulti
        placeholder={"Select task"}
        isClearable={false}
        // noOptionsMessage={"No more tasks available"}
        isClearable={
          !taskOptions.reduce(
            (acc, currentValue) => acc || !currentValue.optional,
            false
          )
        }
        data-tip
        data-for="task-dropdown-tip-off"
      />
      <Tooltip
        id="task-dropdown-tip-off"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Select task
      </Tooltip>
    </>
  );
}
