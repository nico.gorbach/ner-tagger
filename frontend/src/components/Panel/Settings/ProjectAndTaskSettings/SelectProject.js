import React from "react";
import DropDownMenu from "./DropDownMenu";
import { Tooltip } from "../../../../styles/Tooltip";
import { useSelector, useDispatch } from "react-redux";
import setSelectedProjectAction from "../../../../store/actions/setSelectedProjectAction";

export default function SelectProject() {
  const dispatch = useDispatch();

  const selectedProject = useSelector(
    (state) => state.settings.config.selectedProject
  );
  const projectOptions = useSelector(
    (state) => state.settings.config.projectOptions
  );

  return (
    <>
      <DropDownMenu
        title={"Project:"}
        value={selectedProject}
        onChange={(val) => dispatch(setSelectedProjectAction(val))}
        options={projectOptions}
        placeholder={"Select Project"}
        data-tip
        data-for="project-dropdown-tip-off"
      />
      <Tooltip
        id="project-dropdown-tip-off"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Select project
      </Tooltip>
    </>
  );
}
