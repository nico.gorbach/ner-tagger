const customStyles = {
  control: (provided, state) => ({
    ...provided,
    "&:hover": { border: "solid 1px rgb(120,120,120)", color: "rgb(50,50,50)" },
    width: "160px",
    margin: "3px 0px",
    backgroundColor: "rgb(250,250,250)",
    cursor: "pointer",
  }),
  option: (provided, state) => ({
    ...provided,
    color: "black",
    backgroundColor: state.isFocused ? "rgba(92,184,181,0.2)" : "white",
    cursor: "pointer",
  }),
  multiValue: (provided, state) => ({
    ...provided,
    color: state.data.font_color_hex,
    backgroundColor: state.data.background_color_hex,
  }),
  multiValueLabel: (provided, state) => ({
    ...provided,
    color: state.data.font_color_hex,
    paddingRight: "5px",
  }),
  multiValueRemove: (provided, state) =>
    !state.data.optional ? { ...provided, display: "none" } : provided,
  placeholder: (provided, state) => ({
    ...provided,
    "&:hover": { color: "rgb(50,50,50)" },
  }),
  dropdownIndicator: (provided, state) => ({ ...provided, padding: "5px" }),
};

export default customStyles;
