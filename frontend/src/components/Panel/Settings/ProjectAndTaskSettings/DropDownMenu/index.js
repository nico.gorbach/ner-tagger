import React from "react";
import Select from "react-select";
import customStyles from "./customStyles";

export default function DropDownMenu(props) {
  const {
    value,
    onChange,
    options,
    placeholder,
    isMulti,
    isClearable,
    isSearchable,
  } = props;

  return (
    <Select
      className={"custom"}
      value={value}
      onChange={onChange}
      options={options}
      placeholder={placeholder}
      isMulti={isMulti}
      isClearable={isClearable}
      isSearchable={isSearchable}
      // isDisabled={
      //   options
      //     ? (options.length === 1 ?? false) ||
      //       !options.some((option) => (option ? option.optional : true))
      //     : true
      // }
      styles={customStyles}
    />
  );
}
