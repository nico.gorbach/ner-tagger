import React from "react";
import { Tooltip } from "../../../../styles/Tooltip";
import CustomOnOffTogggleButton from "./CustomToggleButton";
import { useSelector, useDispatch } from "react-redux";
import setDoSelectWhiteSpaceAction from "../../../../store/actions/setDoSelectWhiteSpaceAction";
import { MUIIconButton } from "../../../../styles/MUI/Buttons";

export default function HighligghtWhitespace() {
  const dispatch = useDispatch();
  const doSelectWhiteSpace = useSelector(
    (state) => state.settings.annot.doSelectWhiteSpace
  );

  return (
    <>
      <MUIIconButton disableRipple>
        <CustomOnOffTogggleButton
          activeLabel={"space"}
          inactiveLabel={"no space"}
          value={doSelectWhiteSpace}
          setValue={(val) => dispatch(setDoSelectWhiteSpaceAction(val))}
        />
      </MUIIconButton>
      <Tooltip
        id="highlight-white-space-tip"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Highlight white space
      </Tooltip>
    </>
  );
}
