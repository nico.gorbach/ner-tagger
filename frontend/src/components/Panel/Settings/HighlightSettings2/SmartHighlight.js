import React from "react";
import { Tooltip } from "../../../../styles/Tooltip";
import CustomOnOffTogggleButton from "./CustomToggleButton";
import { useSelector, useDispatch } from "react-redux";
import setDoSmartAnnotateAction from "../../../../store/actions/setDoSmartAnnotateAction";
import { MUIIconButton } from "../../../../styles/MUI/Buttons";

export default function SmartHighlight() {
  const dispatch = useDispatch();
  const doSmartAnnotate = useSelector(
    (state) => state.settings.annot.doSmartAnnotate
  );

  return (
    <>
      <MUIIconButton disableRipple>
        <CustomOnOffTogggleButton
          activeLabel={"smart"}
          inactiveLabel={"regular"}
          value={doSmartAnnotate}
          setValue={(val) => dispatch(setDoSmartAnnotateAction(val))}
        />
      </MUIIconButton>
      <Tooltip
        id="smart-highlight-tip"
        place="right"
        effect="solid"
        multiline
        arrowColor={"Transparent"}
        className={"custom"}
        delayHide={500}
        delayShow={2000}
      >
        Smart highlight
      </Tooltip>
    </>
  );
}
