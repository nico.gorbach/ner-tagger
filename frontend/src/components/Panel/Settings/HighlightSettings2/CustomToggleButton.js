import React from "react";
import ToggleOnOffButton from "react-toggle-button";

export default function CustomToggleButton(props) {
  const { activeLabel, inactiveLabel, value, setValue } = props;

  return (
    <ToggleOnOffButton
      value={value}
      onToggle={() => setValue(!value)}
      colors={{ active: { base: "#31a354" } }}
      activeLabel={activeLabel}
      inactiveLabel={inactiveLabel}
      containerStyle={{
        height: "30px",
        width: "110px",
      }}
      trackStyle={{ width: "100%", height: "100%" }}
      thumbStyle={{
        margin: "0",
        height: "28px",
        width: "28px",
      }}
      thumbAnimateRange={[1, 80]}
      activeLabelStyle={{
        fontSize: "15px",
        fontFamily: "Roboto",
        width: "100%",
        marginLeft: "10px",
        marginRight: "-20px",
      }}
      inactiveLabelStyle={{
        fontSize: "15px",
        fontFamily: "Roboto",
        width: "200px",
        marginRight: "0",
      }}
    />
  );
}
