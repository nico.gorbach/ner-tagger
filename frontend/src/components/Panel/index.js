import React from "react";
import logo from "../../assets/adeccoLogo.png";
import { LogoContainer, Logo } from "../../styles/Logo";
import {
  MainContainer,
  SubContainer,
  SubSubContainer,
} from "../../styles/Panel";
import AnnotationProgress from "./AnnotationProgress";
import Settings from "./Settings";
import HighlightSettings from "./Settings/HighlightSettings";
import CustomParticles from "../Particles";

export default function Panel(props) {
  return (
    <MainContainer>
      <CustomParticles
        width={"260px"}
        // height={"calc(100vh - 80px)"}
        height={"100%"}
        particleColor={"#ffffff"}
        numParticles={10}
        size={"5"}
      />
      <LogoContainer
        className={props.displayLogo ? "small-window-width" : null}
      >
        <Logo src={logo} />
      </LogoContainer>
      <SubContainer>
        <SubSubContainer>
          <Settings />
          <AnnotationProgress />
          <HighlightSettings />
        </SubSubContainer>
      </SubContainer>
    </MainContainer>
  );
}
