import { useSelector, useDispatch } from "react-redux";
import setDocAnnotsAction from "../../../../store/actions/setDocAnnotsAction";

export default function useMergeOverlappingAnnotations() {
  const dispatch = useDispatch();

  const docAnnotations = useSelector((state) => state.docAnnotations);

  const currentRange = docAnnotations.slice(-1)[0];

  const overlappingIndex = docAnnotations.findIndex(
    (range) =>
      currentRange.label === range.label &&
      ((currentRange.end > range.start && currentRange.end < range.end) ||
        (currentRange.start > range.start && currentRange.start < range.end))
  );

  if (overlappingIndex !== -1) {
    const { start, end } = docAnnotations[overlappingIndex];
    if (start < docAnnotations[docAnnotations.length - 1].start) {
      docAnnotations[docAnnotations.length - 1].start = start;
    }
    if (end > docAnnotations[docAnnotations.length - 1].end) {
      docAnnotations[docAnnotations.length - 1].end = end;
    }
    docAnnotations.splice(overlappingIndex, 1);
  }

  dispatch(setDocAnnotsAction(docAnnotations));
  return docAnnotations;
}
