import React, { useState } from "react";
import { DocBody, DocContainer } from "../../../styles/Annotation";
import DocAnnotation from "./DocAnnotation";
import { useSelector, useDispatch } from "react-redux";
import setSelectedTextStartAction from "../../../store/actions/setSelectedTextStartAction";
import PublishOutlinedIcon from "@material-ui/icons/PublishOutlined";
import setLoadingDocAction from "../../../store/actions/setLoadingDocAction";
import { MUISubmitButtonIcon } from "../../../styles/MUI/Buttons";
import { Alert, AlertTitle } from "@material-ui/lab";
import { Snackbar, Slide, Tooltip, Zoom } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { setDoc } from "../../../store/actions/setDocAction";
import Axios from "../../../axios/authenticated";

function SlideTransition(props) {
  return <Slide {...props} direction="down" />;
}

const useStyles = makeStyles((theme) => ({
  tooltip: {
    maxWidth: "130px",
  },
}));

export default function Document() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const currentLabel = useSelector(
    (state) => state.settings.config.currentLabel
  );
  const annotationLabels = useSelector(
    (state) => state.settings.config.annotationLabels
  );
  const docAnnotations = useSelector((state) => state.annot.docAnnotations);

  const doc = useSelector((state) => state.annot.doc);

  const selectedProject = useSelector(
    (state) => state.settings.config.selectedProject
  );

  const user = useSelector((state) => state.auth.user);

  const [
    showSuccessfulSubmissionMsg,
    setShowSuccessfulSubmissionMsg,
  ] = useState(null);
  const [showFailedSubmissionMsg, setShowFailedSubmissionMsg] = useState(null);

  const enabled = annotationLabels.includes(currentLabel) ? true : false;

  const handleCloseSubmissionMsg = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setShowSuccessfulSubmissionMsg(false);
  };

  const onFetchNextDoc = () => {
    dispatch(setLoadingDocAction(true));

    let data;
    if (docAnnotations.length !== 0) {
      data = docAnnotations.map((annot) => ({
        user: user.id,
        doc: doc.id,
        project: selectedProject.id,
        annotation_label: annot.label.id,
        content: annot.content,
      }));
    } else {
      data = {
        user: user.id,
        doc: doc.id,
        project: selectedProject.id,
        annotation_label: currentLabel ? currentLabel.id : 1,
        content: "",
      };
    }

    Axios.post("annotations/texts/", data)
      .then(() => {
        dispatch(setDoc());
      })
      .then(() => setTimeout(() => setShowSuccessfulSubmissionMsg(true), 1000))
      .catch((err) => {
        console.log("err", err.response);
        setShowFailedSubmissionMsg(true);
        dispatch(setLoadingDocAction(false));
      });
  };

  return (
    <>
      <DocBody
        className={!enabled ? "highlight-disabled" : null}
        onMouseDown={(e) =>
          dispatch(setSelectedTextStartAction({ X: e.clientX, Y: e.clientY }))
        }
      >
        <DocContainer
          className={"small-window-width"}
          style={{ padding: "20px", backgroundColor: "rgb(250,250,250)" }}
        >
          <DocAnnotation enabled={enabled} />
        </DocContainer>
      </DocBody>
      <Tooltip
        classes={{ tooltip: classes.tooltip }}
        title={
          <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
            Submit and load next doc
          </span>
        }
        TransitionComponent={Zoom}
        placement="bottom"
        enterDelay={2000}
        leaveDelay={0}
        enterNextDelay={2000}
      >
        <MUISubmitButtonIcon
          onClick={onFetchNextDoc}
          data-tip
          data-for="submit-annotations-tip"
        >
          <PublishOutlinedIcon
            fontSize="default"
            style={{
              padding: "10px",
              backgroundColor: "#7ec5c2",
              borderRadius: "100px",
              height: "50px",
              width: "50px",
              fill: "rgb(240,240,240)",
            }}
          />
        </MUISubmitButtonIcon>
      </Tooltip>
      {/* <Snackbar
        open={noAnnotationWarning}
        onClose={() => setTimeout(() => setNoAnnotationWarning(false), 6000)}
        TransitionComponent={(props) => <Slide {...props} direction="down" />}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          severity="warning"
          color="error"
          onClose={() => setNoAnnotationWarning(false)}
          style={{ width: "380px" }}
        >
          <AlertTitle>Warning</AlertTitle>
          There are no annotations in this document. Are you sure you want to
          submit?
        </Alert>
      </Snackbar> */}
      <Snackbar
        open={showSuccessfulSubmissionMsg}
        onClose={handleCloseSubmissionMsg}
        autoHideDuration={1500}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert severity="success" onClose={handleCloseSubmissionMsg}>
          <AlertTitle>Success</AlertTitle>
          Annotations submitted.
        </Alert>
      </Snackbar>
      <Snackbar
        open={showFailedSubmissionMsg}
        onClose={() => setShowFailedSubmissionMsg(false)}
        autoHideDuration={6000}
        TransitionComponent={SlideTransition}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          severity="error"
          onClose={() => setShowFailedSubmissionMsg(false)}
        >
          <AlertTitle>Error</AlertTitle>
          Oops. Annotation submission failed.
        </Alert>
      </Snackbar>
    </>
  );
}
