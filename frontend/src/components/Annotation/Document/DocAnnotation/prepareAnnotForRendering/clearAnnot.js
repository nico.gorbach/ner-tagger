export default function clearAnnot(
  annotatedTextStart,
  docAnnotations,
  dispatch,
  setDocAnnotsAction
) {
  dispatch(
    setDocAnnotsAction(
      docAnnotations.filter((item) => item.start !== annotatedTextStart)
    )
  );
}
