import { useSelector, useDispatch } from "react-redux";
import setDocAnnotsAction from "../../../../../store/actions/setDocAnnotsAction";

export default function useMergeOverlappingAnnots() {
  const dispatch = useDispatch();

  const docAnnotations = useSelector((state) => state.annot.docAnnotations);
  const currentRange = docAnnotations.slice(-1)[0];

  if (docAnnotations.length > 1) {
    docAnnotations.slice(0, -1).forEach((range, index) => {
      const { start, end, label } = range;
      const currentStart = currentRange.start;
      const currentEnd = currentRange.end;
      const currentLabel = currentRange.label;

      let newStart = currentStart;
      let newEnd = currentEnd;

      if (currentLabel === label) {
        newStart =
          currentStart > start && currentStart < end ? start : currentStart;
        newEnd = currentEnd >= start && currentEnd <= end ? end : currentEnd;

        if (newStart !== currentStart || newEnd !== currentEnd) {
          docAnnotations[docAnnotations.length - 1].start = newStart;
          docAnnotations[docAnnotations.length - 1].end = newEnd;
          docAnnotations.splice(index, 1);
        }
      } else {
        newStart =
          currentStart > start && currentStart < end ? end + 1 : currentStart;
        newEnd =
          currentEnd >= start && currentEnd <= end ? start - 1 : currentEnd;
        if (newStart !== currentStart || newEnd !== currentEnd) {
          docAnnotations[docAnnotations.length - 1].start = newStart;
          docAnnotations[docAnnotations.length - 1].end = newEnd;
        }
      }

      if (currentStart <= start && currentEnd >= end) {
        docAnnotations.splice(index, 1);
      }
    });

    dispatch(setDocAnnotsAction(docAnnotations));
  }

  return docAnnotations;
}
