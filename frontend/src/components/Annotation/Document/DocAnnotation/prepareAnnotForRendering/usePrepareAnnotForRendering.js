import annotRenderer from "./annotRenderer";
import { useSelector, useDispatch } from "react-redux";
// import setNodesAction from "../../../../../store/actions/setNodesAction";

export default function usePrepareAnnotationsForRendering() {
  const dispatch = useDispatch();
  const docAnnotations = useSelector((state) => state.annot.docAnnotations);
  const nodes = useSelector((state) => state.annot.nodes);
  const docAnnotationsSorted = docAnnotations.sort((a, b) =>
    a.start > b.start ? 1 : -1
  );

  let newNodes = nodes.slice();
  let l = 0;

  docAnnotationsSorted.forEach((annotatedText) => {
    let { start, end, startPosition } = annotatedText;

    start -= l;
    end -= l;
    startPosition.X -= l * 1.1;

    const annotatedNodes = newNodes.slice(start, end + 1);
    const annotatedNodesRendered = annotRenderer(
      annotatedNodes,
      annotatedText,
      dispatch,
      docAnnotations
    );
    newNodes.splice(start, end - start + 1, annotatedNodesRendered);

    l += end - start;
  });

  // dispatch(setNodesAction(newNodes));

  return newNodes;
}
