import { AnnotatedText } from "../../../../../styles/Annotation";
import clearAnnot from "./clearAnnot";
import setDocAnnotsAction from "../../../../../store/actions/setDocAnnotsAction";
import { Tooltip, Zoom } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

// const hex2rgba = (hex, alpha = 1) => {
//   const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16));
//   return `rgba(${r},${g},${b},${alpha})`;
// };

const styles = {
  tooltip: {
    color: (props) => props.fontColor,
    background: (props) => props.backgroundcolor,
  },
  arrow: {
    color: (props) => props.backgroundcolor,
  },
};

const CustomTooltip = withStyles(styles)(Tooltip);

export default function annotRenderer(
  annotatedNodes,
  annotatedText,
  dispatch,
  docAnnotations
) {
  const {
    fontColor,
    backgroundColor,
    // startPosition,
    start,
    label,
  } = annotatedText;

  // const backgroundColorOpacity = hex2rgba(backgroundColor, 0.5);
  return (
    <CustomTooltip
      title={
        <span
          style={{
            fontSize: "12px",
            letterSpacing: "3px",
            color: fontColor,
          }}
        >
          {label.name}
        </span>
      }
      TransitionComponent={Zoom}
      placement="top"
      enterDelay={0}
      leaveDelay={0}
      arrow
      disableFocusListener
      // disableTouchListener
      enterNextDelay={0}
      backgroundcolor={backgroundColor}
    >
      <AnnotatedText
        key={`${start}-${label}`}
        onClick={() =>
          clearAnnot(start, docAnnotations, dispatch, setDocAnnotsAction)
        }
        color={fontColor}
        backgroundcolor={backgroundColor}
      >
        {annotatedNodes}
      </AnnotatedText>
    </CustomTooltip>
  );
}
