import debounce from "./helpers/debounce";

export default function selectTextHandler(onTextSelected, enabled) {
  const mouseEvent = () => {
    if (!enabled) {
      return false;
    }

    let selectedText;
    if (window.getSelection) {
      selectedText = window.getSelection().toString();
    } else if (document.selection && document.selection.type !== "Control") {
      selectedText = document.selection.createRange().text;
    }

    if (!selectedText || !selectedText.length) {
      return false;
    }
    const currentRange = window.getSelection().getRangeAt(0);

    const startContainerPosition = parseInt(
      currentRange.startContainer.parentNode.dataset.position
    );
    const endContainerPosition = parseInt(
      currentRange.endContainer.parentNode.dataset.position
    );

    const startHL =
      startContainerPosition < endContainerPosition
        ? startContainerPosition
        : endContainerPosition;
    const endHL =
      startContainerPosition < endContainerPosition
        ? endContainerPosition
        : startContainerPosition;

    onTextSelected(selectedText, startHL, endHL);
  };

  let doubleClicked;
  let dismissMouseUp;

  const onMouseUp = () => {
    debounce(() => {
      if (doubleClicked) {
        doubleClicked = false;
        dismissMouseUp++;
      } else if (dismissMouseUp > 0) {
        dismissMouseUp--;
      } else {
        mouseEvent();
      }
    }, 0)();
  };

  const onDoubleClick = (e) => {
    e.stopPropagation();

    doubleClicked = true;
    mouseEvent();
  };

  return { onMouseUp, onDoubleClick };
}
