String.prototype.indexOfRegex = function (regex, fromIndex) {
  const str = fromIndex ? this.substring(fromIndex) : this;
  let match = str.match(regex);
  return match ? str.indexOf(match[0]) + fromIndex : -1;
};

String.prototype.lastIndexOfRegex = function (regex, fromIndex) {
  const str = fromIndex ? this.substring(0, fromIndex) : this;
  let match = str.match(regex);
  return match ? str.lastIndexOf(match[match.length - 1]) : -1;
};

// import {indexOfRegex, lastIndexOfRegex} from "./helpers/stringOperators"

const firstValidCharPattern = /[a-zA-Z0-9~@#$^*_+?`:'%]/gi;
const lastValidCharPattern = /[^a-zA-Z0-9~@#$^*_+?`:)'%]/gi;

export default function smartTextSelection(selectedText, doc) {
  const { content, start, end } = selectedText;

  const startShift = content.indexOfRegex(firstValidCharPattern, 0);

  if (start !== 0) {
    if (startShift === -1) {
      return null;
    } else if (startShift !== 0) {
      selectedText.start += startShift;
    } else {
      selectedText.start =
        doc.lastIndexOfRegex(lastValidCharPattern, start) + 1;
    }
  }

  if (lastValidCharPattern.test(content[content.length - 1])) {
    selectedText.end = doc.lastIndexOfRegex(firstValidCharPattern, end);
  } else {
    const smartEnd = doc.indexOfRegex(lastValidCharPattern, end);
    selectedText.end = smartEnd <= 0 ? doc.length : smartEnd - 1;
  }

  selectedText.content = doc.slice(selectedText.start, selectedText.end + 1);

  // selectedText.start = newStart;
  // selectedText.end = newEnd - 1;

  return selectedText;
}
