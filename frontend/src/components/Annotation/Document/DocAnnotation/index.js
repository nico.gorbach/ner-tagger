import React from "react";
import { Doc } from "../../../../styles/Annotation";
import { useSelector, useDispatch } from "react-redux";
import setDocAnnotsAction from "../../../../store/actions/setDocAnnotsAction";
import useMergeOverlappingAnnots from "./prepareAnnotForRendering/useMergeOverlappingAnnots";
import smartTextSelection from "./smartTextSelection";
import usePrepareAnnotationsForRendering from "./prepareAnnotForRendering/usePrepareAnnotForRendering";
import selectTextHandler from "./selectTextHandler";

export default function DocAnnotation(props) {
  let { enabled } = props;

  const dispatch = useDispatch();
  const selectedTextStart = useSelector(
    (state) => state.annot.selectedTextStart
  );
  const currentLabel = useSelector(
    (state) => state.settings.config.currentLabel
  );
  const doSmartAnnotate = useSelector(
    (state) => state.settings.annot.doSmartAnnotate
  );
  const doc = useSelector((state) => state.annot.doc.text);
  const docAnnotations = useSelector((state) => state.annot.docAnnotations);

  const onTextSelected = (text, start, end) => {
    const selectedText = {
      content: text,
      start,
      end,
      label: currentLabel,
      startPosition: selectedTextStart,
      fontColor: currentLabel.font_color_hex,
      backgroundColor: currentLabel.background_color_hex,
    };

    const textToAnnotate = doSmartAnnotate
      ? smartTextSelection(selectedText, doc)
      : selectedText;

    if (textToAnnotate) {
      dispatch(setDocAnnotsAction([...docAnnotations, textToAnnotate]));
    }
  };

  window.getSelection().removeAllRanges();

  useMergeOverlappingAnnots();

  const newNodes = usePrepareAnnotationsForRendering();

  const { onMouseUp, onDoubleClick } = selectTextHandler(
    onTextSelected,
    enabled
  );

  return (
    <Doc
      onMouseUp={onMouseUp}
      onDoubleClick={(e) => onDoubleClick(e)}
      onTouchEnd={onMouseUp}
      onDoubleTap={(e) => onDoubleClick(e)}
    >
      {newNodes}
    </Doc>
  );
}
