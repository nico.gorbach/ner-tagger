export const indexOfRegex = (text,regex, fromIndex) => {
  const str = fromIndex ? text.substring(fromIndex) : this;
  let match = str.match(regex);
  return match ? str.indexOf(match[0]) + fromIndex : -1;
};

export const lastIndexOfRegex = (regex, fromIndex) => {
  const str = fromIndex ? this.substring(0, fromIndex) : this;
  let match = str.match(regex);
  return match ? str.lastIndexOf(match[match.length - 1]) : -1;
};
