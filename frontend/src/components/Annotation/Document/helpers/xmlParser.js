import XMLParser from "react-xml-parser";
import CVdata from "../../../../Data/CVdata";

export default function xmlParse() {
  const options = {
    // preserveNewlines: true,
  };
  const CVDataJson = new XMLParser().parseFromString(CVdata);
  var { htmlToText } = require("html-to-text");
  const corpusData = {
    html: CVDataJson.children.map((cv) => cv.children[1].children[4].value),
    text: CVDataJson.children
      .map((cv) => cv.children[1].children[4].value)
      .map((html) => htmlToText(html, options)),
  };
  // const data = CVDataJson.children;
  // const lastElement = data.pop();
  // const ca = data.map((cv) => cv.children[1].children[4].value);
  // const ca2 = ca.map((v) => ({ text: htmlToText(v, options), html: v }));
  // const datab = lastElement.children[1].children.slice(5);
  // const cb = datab.map((cv) => cv.children[1].children[4].value);
  // const cb2 = cb.map((v) => ({ text: htmlToText(v, options), html: v }));
  // const cc = [...ca2, ...cb2];
  // const handleSaveToPC = (jsonData) => {
  //   const fileData = JSON.stringify(jsonData);
  //   const blob = new Blob([fileData], { type: "text/plain" });
  //   const url = URL.createObjectURL(blob);
  //   const link = document.createElement("a");
  //   link.download = "filename.json";
  //   link.href = url;
  //   link.click();
  // };
  // // handleSaveToPC(cc);

  return corpusData;
}
