String.prototype.indexOfRegex = function (regex, fromIndex) {
  const str = fromIndex ? this.substring(fromIndex) : this;
  let match = str.match(regex);
  return match ? str.indexOf(match[0]) + fromIndex : -1;
};

String.prototype.lastIndexOfRegex = function (regex, fromIndex) {
  const str = fromIndex ? this.substring(0, fromIndex) : this;
  let match = str.match(regex);
  return match ? str.lastIndexOf(match[match.length - 1]) : -1;
};

export default String;
