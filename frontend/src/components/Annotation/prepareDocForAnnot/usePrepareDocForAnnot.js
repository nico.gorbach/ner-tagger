import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import setNodesAction from "../../../store/actions/setNodesAction";
import { Character } from "../../../styles/Annotation";

export default function usePrepareDocForAnnot(isMounted) {
  const dispatch = useDispatch();
  const doc = useSelector((state) => state.annot.doc);
  const currentLabel = useSelector(
    (state) => state.settings.config.currentLabel
  );
  const doSelectWhiteSpace = useSelector(
    (state) => state.settings.annot.doSelectWhiteSpace
  );
  const selectedSearchStrings = useSelector(
    (state) => state.settings.config.selectedSearchStrings
  );

  const isWhiteSpace = (str) => str.match(/^ *$/);

  const regex = (wholeWord, str) =>
    wholeWord ? "\\b" + str + "\\b((?!\\W(?=\\w))|(?=\\s))" : str;

  let indices,
    searchStrings = [];

  if (doc.text) {
    searchStrings = selectedSearchStrings.map((searchStr) => {
      indices = [
        ...doc.text.matchAll(
          new RegExp(regex(searchStr.whole_word, searchStr.name), "gi")
        ),
      ].map((a) => a.index);
      return { ...searchStr, indices };
    });
  }

  useEffect(() => {
    if (isMounted.current) {
      const { textArray } = doc;

      let fontColor, backgroundColor;
      if (
        currentLabel &&
        currentLabel.font_color_hex &&
        currentLabel.background_color_hex
      ) {
        fontColor = currentLabel.font_color_hex;
        backgroundColor = currentLabel.background_color_hex;
      }

      dispatch(
        setNodesAction(
          textArray.map((charElement, index) => (
            <Character
              data-position={index}
              key={`${index} - ${charElement}`}
              color={
                isWhiteSpace(charElement) && !doSelectWhiteSpace
                  ? "white"
                  : fontColor
              }
              backgroundcolor={
                isWhiteSpace(charElement) && !doSelectWhiteSpace
                  ? "white"
                  : backgroundColor
              }
              searchChar={
                searchStrings.filter((searchStr) =>
                  searchStr.indices.reduce(
                    (acc, currentIndex) =>
                      acc ||
                      (index >= currentIndex &&
                        index <= currentIndex + searchStr.name.length - 1),
                    false
                  )
                    ? searchStr
                    : false
                )[0]
              }
            >
              {charElement.match(/\r|\n/) ? (
                <>
                  <br /> {charElement}
                </>
              ) : (
                charElement
              )}
            </Character>
          ))
        )
      );
    } else {
      isMounted.current = true;
    }
  }, [doc, currentLabel, doSelectWhiteSpace, selectedSearchStrings]);
}
