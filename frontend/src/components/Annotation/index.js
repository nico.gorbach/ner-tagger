import React, { useEffect, useRef } from "react";
import { Main, Body } from "../../styles";
import usePrepareDocForAnnot from "./prepareDocForAnnot/usePrepareDocForAnnot";
import Header from "../Header";
import Panel from "../Panel";
import Document from "./Document";
import useUpdateWindowWidth from "./useUpdateWindowWidth";
import { useDispatch, useSelector } from "react-redux";
import setSelectedProjectAction from "../../store/actions/setSelectedProjectAction";
import { isMobile } from "react-device-detect";

export default function Annotation() {
  const dispatch = useDispatch();

  const projectOptions = useSelector(
    (state) => state.settings.config.projectOptions
  );

  // useEffect(() => {
  //   dispatch(setSelectedProjectAction(projectOptions[0]));
  // }, []);

  const windowWidth = useUpdateWindowWidth();

  const isMounted = useRef(false);
  usePrepareDocForAnnot(isMounted);

  return (
    <Main>
      <Header />
      <Body
        className={isMobile || windowWidth < 800 ? "small-window-width" : null}
      >
        <Panel displayLogo={isMobile || windowWidth < 800} />
        <Document />
      </Body>
    </Main>
  );
}
