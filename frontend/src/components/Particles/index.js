import React from "react";
import Particles from "react-particles-js";

export default function CustomParticles(props) {
  const {
    width,
    height,
    particleColor,
    linksColor,
    numParticles,
    linkOpacity,
    particleOpacity,
    size,
    interactive,
  } = props;
  return (
    <Particles
      style={{
        position: "absolute",
        width: "100%",
        left: "0",
        top: "0",
      }}
      width={width}
      height={height}
      id="tsparticles"
      params={{
        fpsLimit: 30,
        interactivity: {
          detectsOn: "canvas",
          events: {
            onHover: {
              enable: interactive,
              mode: "repulse",
            },
          },
          modes: {
            repulse: {
              distance: 200,
              duration: 0.4,
            },
          },
        },
        particles: {
          color: {
            value: particleColor,
          },
          links: {
            color: linksColor,
            distance: 150,
            enable: true,
            opacity: linkOpacity,
            width: 1,
          },
          collisions: {
            enable: true,
          },
          move: {
            direction: "none",
            enable: true,
            outMode: "bounce",
            random: false,
            straight: false,
          },
          number: {
            // density: {
            //   enable: true,
            //   value_area: 800,
            // },
            value: numParticles,
          },
          opacity: {
            value: particleOpacity,
          },
          shape: {
            type: "circle",
          },
          size: {
            random: true,
            value: size,
          },
        },
        detectRetina: true,
      }}
    />
  );
}
