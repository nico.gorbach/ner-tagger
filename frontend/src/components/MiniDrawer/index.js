import React, { useState, useEffect, useRef } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AccountTreeOutlined from "@material-ui/icons/AccountTreeOutlined";
import FormatListBulletedOutlined from "@material-ui/icons/FormatListBulletedOutlined";
import FormatColorText from "@material-ui/icons/FormatColorText";
import BorderColor from "@material-ui/icons/BorderColor";
import FindInPageOutlined from "@material-ui/icons/FindInPageOutlined";
import DonutLargeOutlined from "@material-ui/icons/DonutLargeOutlined";
import Document from "../Annotation/Document";
import { useDispatch, useSelector } from "react-redux";
import setSelectedProjectAction from "../../store/actions/setSelectedProjectAction";
import usePrepareDocForAnnot from "../Annotation/prepareDocForAnnot/usePrepareDocForAnnot";
import AnnotationLabels from "../Header/AnnotationLabels";
import { MUIToolbar } from "../../styles/MUI/Header";
import { MUIDrawer } from "../../styles/MUI/Drawer";
import CustomParticles from "../Particles";
import SelectProject from "../Panel/Settings/ProjectAndTaskSettings/SelectProject";
import SelectTasks from "../Panel/Settings/ProjectAndTaskSettings/SelectTasks";
import SearchDoc from "../Panel/Settings/ProjectAndTaskSettings/SelectSearchString";
import Progress from "../Panel/AnnotationProgress";
import SmartHighlight from "../Panel/Settings/HighlightSettings2/SmartHighlight";
import HighlightWhitespace from "../Panel/Settings/HighlightSettings2/HighlightWhitespace";
import NextDocButtons from "../Header/NextDocButtons";
import { MUIList, MUIListItem } from "../../styles/MUI/List";
// import logo from "../../assets/adeccoLogo.png";
// import { Logo } from "../../styles/Logo";
import LinearProgress from "@material-ui/core/LinearProgress";
import FadeLoader from "react-spinners/FadeLoader";
import { Backdrop } from "@material-ui/core";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import setProjectOptionsAction from "../../store/actions/setProjectOptionsAction";
import { setDoc } from "../../store/actions/setDocAction";
import { ReactComponent as LeaderboardIcon } from "../../assets/trophy4.svg";
import { MUIIconButton } from "../../styles/MUI/Buttons";
import { Snackbar, Slide, Zoom } from "@material-ui/core";
import MUITooltip from "@material-ui/core/Tooltip";
import { Alert, AlertTitle } from "@material-ui/lab";
import { fetchUser } from "../../store/actions/fetchUserAction";
import Leaderboard from "../Leaderboard";
import Chart from "../Chart";
import EqualizerOutlinedIcon from "@material-ui/icons/EqualizerOutlined";
import { setAnnotDistribution } from "../../store/actions/setAnnotDistributionAction";
import corpus from "../../Data/datastock_CV_samples.json";
import Axios from "../../axios/authenticated";

function SlideTransition(props) {
  return <Slide {...props} direction="down" />;
}

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#5db3ae",
    },
    secondary: {
      main: "#b4dfdd",
    },
  },
});

const drawerWidth = 250;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 2,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    userSelect: "none",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    userSelect: "none",
  },
  menuButton: {
    marginRight: 10,
    "&:focus": {
      outline: "none",
    },
  },
  hide: {
    "&:focus": {
      outline: "none",
    },
    cursor: "default",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
    userSelect: "none",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: "#7ec5c2",
    userSelect: "none",
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
    backgroundColor: "#7ec5c2",
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  linearProgressBar: {
    backgroundColor: "rgb(180,180,180)",
  },
  menuIcon: {
    "&:focus": {
      outline: "none",
    },
  },
  tooltip: {
    maxWidth: "110px",
  },
  tooltipLonger: {
    maxWidth: "120px",
  },
}));

export default function MiniDrawer() {
  // console.log(corpus);
  // useEffect(() => {
  //   corpus.forEach((doc) => {
  //     Axios.post("docs/admin_user", {
  //       name: "CV from datastock",
  //       content_pure_text: doc.text,
  //       type: "CV",
  //       projects: [1],
  //     })
  //       .then((resp) => console.log(resp))
  //       .catch((err) => console.log(err.response));
  //   });
  // }, []);

  const dispatch = useDispatch();

  const [showLeaderboard, setShowLeaderboard] = useState(false);
  const [showChart, setShowChart] = useState(false);
  const [stage, setStage] = useState("prep");

  const user = useSelector((state) => state.auth.user);
  const doc = useSelector((state) => state.annot.doc);

  useEffect(() => {
    dispatch(setDoc());
    // dispatch(fetchUser());
    if (user) {
      dispatch(setProjectOptionsAction(user.projects));
      dispatch(setSelectedProjectAction(user.projects[0]));
    }
  }, []);

  useEffect(() => {
    const stage =
      doc.amount_of_docs_in_project_annotated_by_user /
      doc.amount_of_docs_assigned_to_user;
    if (doc.amount_of_docs_in_project_annotated_by_user >= 1 && stage <= 0.33) {
      setStage("start");
    } else if (stage > 0.33 && stage <= 0.66) {
      setStage("middle");
    } else if (stage > 0.66) {
      setStage("end");
    }
  }, [doc]);

  const loadingDoc = useSelector((state) => state.annot.loadingDoc);

  // useEffect(() => {
  //   dispatch(setSelectedProjectAction(projectOptions[0]));
  // }, []);

  const isMounted = useRef(false);
  usePrepareDocForAnnot(isMounted);

  const classes = useStyles();
  // const theme = useTheme();
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleCloseLeaderBoard = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setShowLeaderboard(false);
  };

  const handleShowChart = (e) => {
    e.preventDefault();

    setShowChart(true);
    dispatch(setAnnotDistribution());
  };

  const handleShowLeaderboard = (e) => {
    e.preventDefault();

    setShowLeaderboard(true);
    dispatch(setAnnotDistribution());
  };

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <MUIToolbar>
            <CustomParticles
              width={"100%"}
              height={"65px"}
              particleColor={"#ffffff"}
              numParticles={10}
              size={"5"}
            />
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MUITooltip
                title={
                  <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                    Open drawer
                  </span>
                }
                TransitionComponent={Zoom}
                placement="bottom"
                enterDelay={2000}
                leaveDelay={0}
                enterNextDelay={2000}
              >
                <MenuIcon
                  style={{ fill: open ? "Transparent" : "rgb(240,240,240)" }}
                />
              </MUITooltip>
            </IconButton>
            <AnnotationLabels
              data-tip
              data-for="label"
              style={{ backgroundColor: "yellow" }}
            />
            <MUITooltip
              classes={{ tooltip: classes.tooltipLonger }}
              title={
                <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                  Number of submissions of this doc
                </span>
              }
              TransitionComponent={Zoom}
              placement="bottom"
              enterDelay={2000}
              leaveDelay={0}
              enterNextDelay={2000}
            >
              <span
                style={{
                  marginLeft: "70px",
                  marginRight: "0px",
                  color: "white",
                  fontSize: "20px",
                  zIndex: "999",
                  cursor: "default",
                }}
              >
                {doc.amount_submitted}
              </span>
            </MUITooltip>
            <MUITooltip
              title={
                <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                  Labels
                </span>
              }
              TransitionComponent={Zoom}
              placement="bottom"
              enterDelay={2000}
              leaveDelay={0}
              enterNextDelay={2000}
            >
              <MUIIconButton
                onClick={handleShowChart}
                style={{ marginLeft: "auto", marginRight: "50px" }}
              >
                <EqualizerOutlinedIcon
                  fontSize="large"
                  style={{ fill: "rgb(240,240,240)" }}
                />
              </MUIIconButton>
            </MUITooltip>
            <MUITooltip
              title={
                <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                  Leaderboard
                </span>
              }
              TransitionComponent={Zoom}
              placement="bottom"
              enterDelay={2000}
              enterNextDelay={2000}
            >
              <MUIIconButton
                onClick={handleShowLeaderboard}
                style={{ marginRight: "50px" }}
              >
                <LeaderboardIcon fill="white" height={30} width={30} />
              </MUIIconButton>
            </MUITooltip>
            <NextDocButtons style={{ marginRight: "30px" }} />
          </MUIToolbar>
          {loadingDoc ? (
            <LinearProgress
              color="secondary"
              className={classes.linearProgressBar}
            />
          ) : null}
        </AppBar>
        <MUIDrawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>
              <>
                <MUITooltip
                  title={
                    <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                      Close
                    </span>
                  }
                  TransitionComponent={Zoom}
                  placement="left"
                  enterDelay={2000}
                  leaveDelay={0}
                  enterNextDelay={2000}
                >
                  <ChevronLeftIcon
                    fontSize="large"
                    style={{ fill: "rgb(240,240,240)" }}
                  />
                </MUITooltip>
              </>
            </IconButton>
          </div>
          <Divider />
          <MUIList>
            {[<SelectProject />, <SelectTasks />].map((text, index) => (
              <MUITooltip
                title={
                  <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                    {index === 0 ? "Project" : "Tasks"}
                  </span>
                }
                TransitionComponent={Zoom}
                placement="right"
                enterDelay={2000}
                leaveDelay={0}
                disableHoverListener={open}
                enterNextDelay={2000}
              >
                <MUIListItem
                  button
                  key={index}
                  onClick={handleDrawerOpen}
                  isdraweropen={open ? 1 : 0}
                  disableRipple={true}
                  style={{ display: null }}
                >
                  <ListItemIcon>
                    {index === 0 ? (
                      <AccountTreeOutlined
                        style={{ fill: "rgb(240,240,240)" }}
                      />
                    ) : index === 1 ? (
                      <FormatListBulletedOutlined
                        style={{ fill: "rgb(240,240,240)" }}
                      />
                    ) : (
                      <FindInPageOutlined
                        style={{ fill: "rgb(240,240,240)" }}
                      />
                    )}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </MUIListItem>
              </MUITooltip>
            ))}
          </MUIList>
          <Divider />
          <MUIList>
            {[<SearchDoc />].map((text, index) => (
              <MUITooltip
                title={
                  <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                    Search
                  </span>
                }
                TransitionComponent={Zoom}
                placement="right"
                enterDelay={2000}
                leaveDelay={0}
                disableHoverListener={open}
                enterNextDelay={2000}
              >
                <MUIListItem
                  button
                  key={index}
                  onClick={handleDrawerOpen}
                  isdraweropen={open ? 1 : 0}
                  disableRipple={true}
                >
                  <ListItemIcon>
                    <FindInPageOutlined style={{ fill: "rgb(240,240,240)" }} />
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </MUIListItem>
              </MUITooltip>
            ))}
          </MUIList>
          <Divider />
          <List>
            {[<Progress />].map((text, index) => (
              <MUITooltip
                title={
                  <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                    Progress
                  </span>
                }
                TransitionComponent={Zoom}
                placement="right"
                enterDelay={2000}
                leaveDelay={0}
                disableHoverListener={open}
                enterNextDelay={2000}
              >
                <MUIListItem
                  button
                  key={index}
                  onClick={handleDrawerOpen}
                  isdraweropen={open ? 1 : 0}
                  disableRipple={true}
                >
                  <ListItemIcon>
                    <DonutLargeOutlined style={{ fill: "rgb(240,240,240)" }} />
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </MUIListItem>
              </MUITooltip>
            ))}
          </List>
          <Divider />
          <List>
            {[<SmartHighlight />, <HighlightWhitespace />].map(
              (text, index) => (
                <MUITooltip
                  classes={{ tooltip: classes.tooltip }}
                  title={
                    <span style={{ fontSize: "12px", letterSpacing: "3px" }}>
                      {index === 0 ? "Smart highlight" : "Highlight whitespace"}
                    </span>
                  }
                  TransitionComponent={Zoom}
                  placement="right"
                  enterDelay={2000}
                  leaveDelay={0}
                  disableHoverListener={open}
                  enterNextDelay={2000}
                >
                  <MUIListItem
                    button
                    key={index}
                    onClick={handleDrawerOpen}
                    isdraweropen={open ? 1 : 0}
                    disableRipple={true}
                  >
                    <ListItemIcon>
                      {index === 0 ? (
                        <FormatColorText style={{ fill: "rgb(240,240,240)" }} />
                      ) : (
                        <BorderColor style={{ fill: "rgb(240,240,240)" }} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </MUIListItem>
                </MUITooltip>
              )
            )}
          </List>
        </MUIDrawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Backdrop
            className={classes.backdrop}
            open={loadingDoc}
            style={{
              top: "72px",
              left: open ? "250px" : "73px",
              backgroundColor: "rgba(100,100,100,0.3)",
            }}
          >
            <FadeLoader
              color="rgb(80,80,80)"
              style={{ position: "relative", left: "50%", top: "50%" }}
            />
          </Backdrop>
          <Snackbar
            open={user.amount_text_annot_by_user === 0}
            // onClose={handleCloseLeaderBoard}
            autoHideDuration={6000}
            TransitionComponent={SlideTransition}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <Alert
              icon={
                stage === "start"
                  ? "👌"
                  : stage === "middle"
                  ? "✊"
                  : stage === "end"
                  ? "👊"
                  : "🤙"
              }
              severity="info"
              // onClose={handleCloseLeaderBoard}
            >
              <AlertTitle>
                {`${doc.amount_of_docs_in_project_annotated_by_user}/${
                  doc.amount_of_docs_assigned_to_user
                } ${
                  stage === "start"
                    ? "Good start "
                    : stage === "middle"
                    ? "You're halfway "
                    : stage === "end"
                    ? "The end is nigh "
                    : "Let's start annotating "
                } ${user.first_name}!`}
              </AlertTitle>
              {`${
                stage === "start"
                  ? ""
                  : stage === "middle"
                  ? "Life is like riding a bicycle, to keep your balance, you must keep moving. "
                  : stage === "end"
                  ? "Glory awaits you at the finish line!"
                  : "The secret to getting ahead is getting started."
              } `}
            </Alert>
          </Snackbar>
          <Leaderboard
            isOpen={showLeaderboard}
            setIsOpen={setShowLeaderboard}
          />
          <Chart isOpen={showChart} setIsOpen={setShowChart} />
          <Document />
        </main>
      </div>
    </ThemeProvider>
  );
}
