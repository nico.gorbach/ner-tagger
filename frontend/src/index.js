import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import { store } from "./store";
import { Provider } from "react-redux";
import Routes from "./routes";
import { loginAction } from "./store/actions/loginAction";
import Axios from "./axios/not_authenticated";

const token = localStorage.getItem("token");
if (token) {
  // Axios.post("auth/token/verify/", { token })
  //   .then(() => {
  //     store.dispatch(
  //       loginAction({
  //         token: token,
  //         user: JSON.parse(localStorage.getItem("user")),
  //       })
  //     );
  //   })
  //   .catch((err) => {
  //     // console.log("err", err.response);
  //   });

  store.dispatch(
    loginAction({
      token: token,
      user: JSON.parse(localStorage.getItem("user")),
    })
  );
}

ReactDOM.render(
  // <React.StrictMode>
  <Provider store={store}>
    <Routes />
  </Provider>,
  // </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
