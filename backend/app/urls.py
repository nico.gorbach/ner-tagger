"""NER Tagger URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls

#from apps.statistics.views import StatisticsBundleView

api_patterns = [
    path('api-docs/', include_docs_urls(title='NER Tagger API', schema_url='/', permission_classes=[])),
    path('auth/', include('app.registration.urls')),
    path('users/', include('app.users.urls')),
    path('project_settings/', include('app.project_settings.urls')),
    path('annotations/', include('app.annotations.urls')),
    path('docs/', include('app.docs.urls')),
]
urlpatterns = [
    path('backend/admin/', admin.site.urls),
    path('backend/api/', include(api_patterns)),
]
