from django.contrib import admin
from app.annotations.models.annotation_labels import AnnotationLabel
from app.annotations.models.annotated_texts import AnnotatedText

admin.site.register(AnnotationLabel)
admin.site.register(AnnotatedText)
