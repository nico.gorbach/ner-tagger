from django.urls import path
from app.annotations.views.annotation_labels import ListAnnotationLabelsView, GetAnnotationLabelView
from app.annotations.views.annotated_texts import ListCreateAnnotatedTextView, GetUpdateDeleteAnnotatedTextView

urlpatterns = [
    path('labels/', ListAnnotationLabelsView.as_view()),
    path('labels/<int:annotation_label_id>/', GetAnnotationLabelView.as_view()),
    path('texts/', ListCreateAnnotatedTextView.as_view()),
    path('texts/<int:annotated_text_id>/', GetUpdateDeleteAnnotatedTextView.as_view())
]

