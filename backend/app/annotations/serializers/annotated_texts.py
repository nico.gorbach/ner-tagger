from rest_framework import serializers
from app.annotations.models.annotated_texts import AnnotatedText

class AnnotatedTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotatedText
        exclude = ['created', 'updated']

