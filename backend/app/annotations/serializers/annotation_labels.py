from rest_framework import serializers
from app.annotations.models.annotation_labels import AnnotationLabel

class AnnotationLabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotationLabel
        exclude = ['created', 'updated']