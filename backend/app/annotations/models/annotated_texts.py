from django.db import models
from django.conf import settings
from app.annotations.models.annotation_labels import AnnotationLabel
from app.docs.models import Doc
from app.project_settings.models.projects import Project
from app.project_settings.models.tasks import Task

confidence_choices = [("low","Not confident"),("medium","Moderately confident"),("strong", "Very confident")]

class AnnotatedText(models.Model):
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='annotated_texts', on_delete=models.CASCADE)
    project = models.ForeignKey(to=Project, related_name='annotated_texts', on_delete=models.CASCADE)
    task = models.ForeignKey(to=Task, related_name='annotated_texts', on_delete=models.CASCADE, blank=True, null=True)
    annotation_label = models.ForeignKey(to=AnnotationLabel, related_name='annotated_texts', on_delete=models.CASCADE)
    doc = models.ForeignKey(to=Doc, related_name='annotated_texts', on_delete=models.CASCADE)
    content = models.TextField(verbose_name="content",blank=True)
    confidence = models.CharField(max_length=40, choices=confidence_choices, default=confidence_choices[1],blank=True, null=True)
    created = models.DateTimeField(verbose_name="created",auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated",auto_now=True, null=True)

    def __str__(self):
        return f"{self.annotation_label}: {self.content[0:20]} - {self.doc}"
