from django.db import models
from app.project_settings.models.tasks import Task
from colorfield.fields import ColorField

class AnnotationLabel(models.Model):
    task = models.ForeignKey(to=Task, related_name='annotation_labels', on_delete=models.CASCADE)
    name = models.CharField(verbose_name="name", max_length=120)
    label = models.CharField(verbose_name="label", max_length=120)
    font_color_hex = ColorField(verbose_name="font color", default="#FFFFFF")
    background_color_hex = ColorField(verbose_name="background color", default="#67a9cf")
    description = models.CharField(verbose_name="description", max_length=120, blank=True, null=True)
    created = models.DateTimeField(verbose_name="created", auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated", auto_now=True, null=True)

    def __str__(self):
        return f"{self.name}: {self.label}"

