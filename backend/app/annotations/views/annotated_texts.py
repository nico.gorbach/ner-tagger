from app.annotations.models.annotated_texts import AnnotatedText
from app.annotations.models.annotation_labels import AnnotationLabel
from app.annotations.serializers.annotated_texts import AnnotatedTextSerializer
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from app.annotations.permissions import IsOwnerOrReadOnly

class ListCreateAnnotatedTextView(ListCreateAPIView):
    queryset = AnnotatedText.objects.all()
    serializer_class = AnnotatedTextSerializer

    def perform_create(self, serializer,*args, **kwargs):
        serializer.save(user=self.request.user)

        #annotation_label_data = self.request.data['annotation_label']
       #AnnotatedText.objects.filter(task__annotation_labels__exact=annotation_label_data)

    def get_queryset(self):
        return AnnotatedText.objects.filter(user__exact=self.request.user)

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return super(ListCreateAnnotatedTextView, self).get_serializer(*args, **kwargs)

class GetUpdateDeleteAnnotatedTextView(RetrieveUpdateDestroyAPIView):
        queryset = AnnotatedText.objects.all()
        serializer_class = AnnotatedTextSerializer
        lookup_url_kwarg = 'annotated_text_id'
        permission_classes = [IsOwnerOrReadOnly]

        def get_queryset(self):
            owner_queryset = self.queryset.filter(user__exact=self.request.user)
            return owner_queryset

