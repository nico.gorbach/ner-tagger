from app.annotations.models.annotation_labels import AnnotationLabel
from app.annotations.serializers.annotation_labels import AnnotationLabelSerializer
from rest_framework.generics import ListAPIView, RetrieveAPIView

class ListAnnotationLabelsView(ListAPIView):
    queryset = AnnotationLabel.objects.all()
    serializer_class = AnnotationLabelSerializer

    def get_queryset(self):
        return AnnotationLabel.objects.filter(task__project__users__in=[self.request.user])

class GetAnnotationLabelView(RetrieveAPIView):
        queryset = AnnotationLabel.objects.all()
        serializer_class = AnnotationLabelSerializer
        lookup_url_kwarg = 'annotation_label_id'

        def get_queryset(self):
            return self.queryset.filter(task__project__users__in=[self.request.user])
