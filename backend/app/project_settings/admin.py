from django.contrib import admin
from app.project_settings.models.projects import Project
from app.project_settings.models.tasks import Task
from app.project_settings.models.search_strings import SearchString

admin.site.register(Project)
admin.site.register(Task)
admin.site.register(SearchString)
