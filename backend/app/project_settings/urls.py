from django.urls import path
from app.project_settings.views.projects import ListProjectsView, GetProjectView
from app.project_settings.views.tasks import ListTasksView, GetTaskView
from app.project_settings.views.search_strings import ListSearchStringView

urlpatterns = [
    path('projects/', ListProjectsView.as_view()),
    path('projects/<int:project_id>/', GetProjectView.as_view()),
    path('tasks/', ListTasksView.as_view()),
    path('tasks/<int:task_id>/', GetTaskView.as_view()),
    path('search_strings/', ListSearchStringView.as_view()),
]
