from django.db import models
from django.conf import settings

class Project(models.Model):
    users = models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='projects')
    name = models.CharField(verbose_name="name of project",max_length=120)
    label = models.CharField(verbose_name="label of project", max_length=120)
    description = models.CharField(verbose_name="description of project",max_length=120,blank=True, null=True)
    created = models.DateTimeField(verbose_name="created",auto_now_add=True,blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated",auto_now=True, null=True)

    def __str__(self):
        return f"{self.name}: {self.label}"
