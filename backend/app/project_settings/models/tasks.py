from django.db import models
from app.project_settings.models.projects import Project
from colorfield.fields import ColorField

smart_annotate_choices = [("on","Smart annotate on"),("off","Smart annotate off"),("flexible", "User can choose")]

class Task(models.Model):
    project = models.ForeignKey(to=Project, related_name='tasks', on_delete=models.CASCADE)
    name = models.CharField(verbose_name="name of task", max_length=120)
    label = models.CharField(verbose_name="label of task", max_length=120)
    smart_annotate = models.CharField(max_length=120, choices=smart_annotate_choices, default=smart_annotate_choices[0])
    optional = models.BooleanField(default=False)
    font_color_hex = ColorField(verbose_name="font color", default="#FFFFFF")
    background_color_hex = ColorField(verbose_name="background color", default="#addd8e")
    description = models.CharField(verbose_name="description of task", max_length=120, blank=True, null=True)
    created = models.DateTimeField(verbose_name="created", auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated", auto_now=True, null=True)

    def __str__(self):
        return f"{self.name}: {self.label}"
