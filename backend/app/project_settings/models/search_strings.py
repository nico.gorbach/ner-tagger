from django.db import models
from app.project_settings.models.tasks import Task
from colorfield.fields import ColorField

class SearchString(models.Model):
    tasks = models.ManyToManyField(to=Task, related_name='search_strings')
    name = models.CharField(verbose_name="name of search string",max_length=120)
    label = models.CharField(verbose_name="label of search string", max_length=120)
    whole_word = models.BooleanField(default=True)
    optional = models.BooleanField(default=False)
    font_color_hex = ColorField(verbose_name="font color", default="#000000")
    background_color_hex = ColorField(verbose_name="background color", default="#fdbb84")
    description = models.CharField(verbose_name="description of search string",max_length=120,blank=True, null=True)
    created = models.DateTimeField(verbose_name="created",auto_now_add=True,blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated",auto_now=True, null=True)

    def __str__(self):
        return f"{self.name}: {self.label}"
