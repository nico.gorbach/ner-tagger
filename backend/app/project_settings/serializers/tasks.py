from rest_framework import serializers
from app.project_settings.models.tasks import Task
from app.annotations.serializers.annotation_labels import AnnotationLabelSerializer
from app.project_settings.serializers.search_strings import SearchStringSerializer

class TaskSerializer(serializers.ModelSerializer):
    annotation_labels = AnnotationLabelSerializer(read_only=True, many=True)
    search_strings = SearchStringSerializer(read_only=True, many=True)

    class Meta:
        model = Task
        exclude = ['created','updated']
