from rest_framework import serializers
from app.project_settings.models.projects import Project
from app.project_settings.serializers.tasks import TaskSerializer
from app.annotations.serializers.annotated_texts import AnnotatedText

class ProjectSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(read_only=True, many=True)

    class Meta:
        model = Project
        exclude = ['created', 'updated']


# class ProjectAmountAnnotSerializer(serializers.ModelSerializer):
#     frac_annot_docs = serializers.SerializerMethodField()
#
#     class Meta:
#         model = Project
#         fields = ('frac_annot_docs')
#
#     @staticmethod
#     def get_frac_annot_docs(project):
#         amount_docs_assigned_to_user = Project.objects.filter(user_exact)
        # amount_of_docs_annotated_by_user = AnnotatedText.objects.filter(user__exact=user).all().count()
        # amount_of_docs_assigned_to_user = AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()
        # return min([amount_of_docs_annotated_by_user/amount_of_docs_assigned_to_user,1])

# class UserAmountAnnotSerializer(serializers.ModelSerializer):
#     username = serializers.SerializerMethodField()
#     first_name = serializers.SerializerMethodField()
#     last_name = serializers.SerializerMethodField()
#     frac_annot_docs = serializers.SerializerMethodField()
#
#     class Meta:
#         model = User
#         fields = ('username','first_name','last_name','frac_annot_docs')
#
#     @staticmethod
#     def get_username(user):
#         return user.username
#
#     @staticmethod
#     def get_first_name(user):
#         return user.first_name
#
#     @staticmethod
#     def get_last_name(user):
#         return user.last_name
#
#     @staticmethod
#     def get_frac_annot_docs(user):
#         amount_of_docs_annotated_by_user = AnnotatedText.objects.filter(user__exact=user).all().count()
#         amount_of_docs_assigned_to_user = AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()
#         return min([amount_of_docs_annotated_by_user/amount_of_docs_assigned_to_user,1])
