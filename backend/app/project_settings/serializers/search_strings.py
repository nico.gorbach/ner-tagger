from rest_framework import serializers
from app.project_settings.models.search_strings import SearchString

class SearchStringSerializer(serializers.ModelSerializer):

    class Meta:
        model = SearchString
        exclude = ['created','updated']
