from app.project_settings.models.tasks import Task
from app.project_settings.serializers.tasks import TaskSerializer
from rest_framework.generics import ListAPIView, RetrieveAPIView

class ListTasksView(ListAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        return Task.objects.filter(project__users__exact=user_id)

class GetTaskView(RetrieveAPIView):
        queryset = Task.objects.all()
        serializer_class = TaskSerializer
        lookup_url_kwarg = 'task_id'

        def get_queryset(self):
            return self.queryset.filter(project__users__in=[self.request.user])
