from app.project_settings.models.search_strings import SearchString
from app.project_settings.serializers.search_strings import SearchStringSerializer
from rest_framework.generics import ListAPIView

class ListSearchStringView(ListAPIView):
    queryset = SearchString.objects.all()
    serializer_class = SearchStringSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        return SearchString.objects.filter(tasks__project__users__exact=user_id)
