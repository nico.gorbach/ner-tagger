from app.project_settings.models.projects import Project
from app.project_settings.serializers.projects import ProjectSerializer
from rest_framework.generics import ListAPIView, RetrieveAPIView
#from .serializers import UserAmountAnnotSerializer

class ListProjectsView(ListAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def filter_queryset(self, queryset):
        return self.request.user.projects

class GetProjectView(RetrieveAPIView):
        queryset = Project.objects.all()
        serializer_class = ProjectSerializer
        lookup_url_kwarg = 'project_id'

        def get_queryset(self):
            return self.queryset.filter(users__in=[self.request.user])


class GetAnnotAmountsView(RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_url_kwarg = 'project_id'

    def get_queryset(self):
        return self.queryset.filter(users__in=[self.request.user])



# class ListAnnotAmountsView(ListAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserAmountAnnotSerializer
#
#     def get_queryset(self):
#         projects_assigned_to_user = self.request.user.projects.all()
#         return self.queryset.filter(projects__in=projects_assigned_to_user)