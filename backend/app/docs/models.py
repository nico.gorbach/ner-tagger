from django.db import models
from app.project_settings.models.projects import Project

doc_type_choices = [("CV","Curiculum vitae"),("JD","Job description")]

class Doc(models.Model):
    name = models.CharField(verbose_name="name of document",max_length=120)
    content_pure_text = models.TextField(verbose_name="content in pure text format" )
    content_html = models.TextField(verbose_name="content in html format", blank=True, null=True)
    type = models.CharField(verbose_name="type", max_length=120, choices=doc_type_choices, default=doc_type_choices[0])
    projects = models.ManyToManyField(to=Project, related_name='docs')
    description = models.CharField(verbose_name="description", max_length=120, blank=True, null=True)
    created = models.DateTimeField(verbose_name="created", auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(verbose_name="updated", auto_now=True, null=True)

    def __str__(self):
        return f"{self.type}: {self.content_pure_text[0:10]}..."