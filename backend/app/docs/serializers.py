from rest_framework import serializers
from .models import Doc
from app.annotations.models.annotated_texts import AnnotatedText

class DocSerializer(serializers.ModelSerializer):
    amount_submitted = serializers.SerializerMethodField()
    amount_assigned_to_loggedin_user = serializers.SerializerMethodField()
    amount_submitted_by_loggedin_user = serializers.SerializerMethodField()

    class Meta:
        model = Doc
        exclude = ['created', 'updated']

    @staticmethod
    def get_amount_submitted(doc):
        return AnnotatedText.objects.filter(doc__exact=doc).all().count()

    def get_amount_assigned_to_loggedin_user(self, doc):
        user = self.context['request'].user
        projects_assigned_to_user = user.projects.all()
        return Doc.objects.filter(projects__in=projects_assigned_to_user).all().count()

    def get_amount_submitted_by_loggedin_user(self, doc):
        user = self.context['request'].user
        return AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()
        # projects_assigned_to_user = user.projects.all()
        # docs_assigned_to_user = Doc.objects.filter(projects__in=projects_assigned_to_user)
        # return docs_assigned_to_user.filter(annotated_texts__user__exact=user).all().count()

    # @staticmethod
    # def get_amount_docs(user):
    #     projects_assigned_to_user = user.projects.all()
    #     return Doc.objects.filter(projects__in=projects_assigned_to_user).all().count()
    #
    # @staticmethod
    # def get_amount_submitted(user):
    #     return AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()
