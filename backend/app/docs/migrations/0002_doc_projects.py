# Generated by Django 3.1.3 on 2020-11-26 18:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('project_settings', '0001_initial'),
        ('docs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='doc',
            name='projects',
            field=models.ManyToManyField(related_name='docs', to='project_settings.Project'),
        ),
    ]
