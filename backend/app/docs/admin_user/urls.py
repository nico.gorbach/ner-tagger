from django.urls import path
from .views import ListCreateDocView, GetUpdateDeleteDocView

urlpatterns = [
    path('', ListCreateDocView.as_view()),
    path('<int:doc_id>/', GetUpdateDeleteDocView.as_view())
]
