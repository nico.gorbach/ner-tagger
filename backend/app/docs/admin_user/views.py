from app.docs.models import Doc
from app.docs.serializers import DocSerializer
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAdminUser


class ListCreateDocView(ListCreateAPIView):
    queryset = Doc.objects.all()
    serializer_class = DocSerializer
    permission_classes = [IsAdminUser]

    def perform_create(self, serializer):
        serializer.save()


class GetUpdateDeleteDocView(RetrieveUpdateDestroyAPIView):
    queryset = Doc.objects.all()
    serializer_class = DocSerializer
    lookup_url_kwarg = 'doc_id'
    permission_classes = [IsAdminUser]