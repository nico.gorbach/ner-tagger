from django.urls import path, include
from .admin_user.urls import urlpatterns as special_urlpatterns
from .loggedin_user.urls import urlpatterns as loggedin_user_urlpatterns

urlpatterns = [
    path('admin_user', include(special_urlpatterns)),
    path('', include(loggedin_user_urlpatterns)),
]
