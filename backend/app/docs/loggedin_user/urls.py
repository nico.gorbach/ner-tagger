from django.urls import path, include
from .views import ListDocsView, ListAnnotatedDocsView
from .unannotated.urls import urlpatterns as unannotated_urlpatterns

urlpatterns = [
    path('', ListDocsView.as_view()),
    path('annotated/', ListAnnotatedDocsView.as_view()),
    path('unannotated/', include(unannotated_urlpatterns)),
]
