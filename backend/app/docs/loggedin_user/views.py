from app.docs.models import Doc
from app.docs.serializers import DocSerializer
from rest_framework.generics import ListAPIView, RetrieveAPIView


class ListDocsView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        return Doc.objects.filter(projects__users__exact=self.request.user)


class GetDocView(RetrieveAPIView):
        queryset = Doc.objects.all()
        serializer_class = DocSerializer
        lookup_url_kwarg = 'doc_id'

        def get_queryset(self):
            return self.queryset.filter(projects__users__in=[self.request.user])


class ListAnnotatedDocsView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        docs_assigned_to_user = Doc.objects.filter(projects__users__exact=self.request.user)
        docs_annotated_and_assigned_to_user = docs_assigned_to_user.filter(annotated_texts__user__exact=self.request.user)
        return docs_annotated_and_assigned_to_user
