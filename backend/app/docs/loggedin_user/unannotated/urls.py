from django.urls import path
from .views import ListUnannotatedDocsView, \
    GetRandomUnannotatedDocView, GetLeastAnnotatedDocView, \
    GetMostAnnotatedDocView

urlpatterns = [
    path('', ListUnannotatedDocsView.as_view()),
    path('random/', GetRandomUnannotatedDocView.as_view()),
    path('max-exploration/', GetLeastAnnotatedDocView.as_view()),
    path('max-exploitation/', GetMostAnnotatedDocView.as_view()),
]
