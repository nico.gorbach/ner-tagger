from app.docs.models import Doc
from app.docs.serializers import DocSerializer
from rest_framework.generics import ListAPIView
from django.db.models import Count

class ListUnannotatedDocsView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        docs_assigned_to_user = Doc.objects.filter(projects__users__exact=self.request.user)
        docs_unannotated_and_assigned_to_user = docs_assigned_to_user.exclude(annotated_texts__user__exact=self.request.user)
        return docs_unannotated_and_assigned_to_user


class GetRandomUnannotatedDocView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        docs_assigned_to_user = Doc.objects.filter(projects__users__exact=self.request.user)
        docs_unannotated_and_assigned_to_user = docs_assigned_to_user.exclude(annotated_texts__user__exact=self.request.user)
        random_doc_unannotated_and_assigned_to_user = docs_unannotated_and_assigned_to_user.order_by('?')[:1]
        return random_doc_unannotated_and_assigned_to_user


class GetLeastAnnotatedDocView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        user = self.request.user
        docs = Doc.objects.filter(projects__users__exact=user)
        docs_with_number_of_user_annotations_by_users = docs.annotate(count=Count('annotated_texts'))
        least_annotated_doc = docs_with_number_of_user_annotations_by_users.order_by('count')[:1]
        return least_annotated_doc


class GetMostAnnotatedDocView(ListAPIView):
    serializer_class = DocSerializer
    queryset = Doc.objects.all()

    def get_queryset(self):
        docs_with_number_of_user_annotations_by_users = Doc.objects.filter(projects__users__exact=self.request.user).annotate(count=Count('annotated_texts'))
        most_annotated_doc = docs_with_number_of_user_annotations_by_users.order_by('-count')[:1]
        return most_annotated_doc