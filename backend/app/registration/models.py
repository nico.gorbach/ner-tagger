from django.db import models
from django.conf import settings

class Registration(models.Model):
    user = models.OneToOneField(
        verbose_name='users',
        on_delete=models.CASCADE,
        related_name='registration_profile',
        to=settings.AUTH_USER_MODEL
    )

    def __str__(self):
        return f'{self.user.email}'
