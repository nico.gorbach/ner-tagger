from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView
from .views import TokenUserObtainView

app_name = 'registration'

urlpatterns = [
    path('token/', TokenUserObtainView.as_view(), name='retrieve-token-and-users'),
    path('token/refresh/', TokenRefreshView.as_view(), name='retrieve-refreshed-token'),
    path('token/verify/', TokenVerifyView.as_view(), name='verify-token'),
]
