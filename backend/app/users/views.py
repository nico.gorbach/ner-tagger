from .serializers import UserSerializer
from .serializers import UserAmountAnnotSerializer
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser

from django.contrib.auth import get_user_model
User = get_user_model()

class ListUsersView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser]

class GetMeView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        return self.queryset.filter(id__exact=self.request.user.id)

class ListAnnotAmountsView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserAmountAnnotSerializer

    def get_queryset(self):
        projects_assigned_to_user = self.request.user.projects.all()
        return self.queryset.filter(projects__in=projects_assigned_to_user)
