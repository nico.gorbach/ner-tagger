from django.urls import path
from .views import ListUsersView, GetMeView, ListAnnotAmountsView

urlpatterns = [
    path('', ListUsersView.as_view()),
    path('me/', GetMeView.as_view()),
    path('annotation-distribution/', ListAnnotAmountsView.as_view())
]
