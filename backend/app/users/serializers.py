from django.contrib.auth import get_user_model
from rest_framework import serializers
from app.annotations.models.annotated_texts import AnnotatedText
from app.docs.models import Doc
from app.project_settings.serializers.projects import ProjectSerializer
from django.db.models import Count

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    projects = ProjectSerializer(read_only=True, many=True)
    amount_text_annot = serializers.SerializerMethodField()
    amount_docs_submitted = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ['password','email','last_login','is_superuser','is_staff','is_active','date_joined','groups','user_permissions']

    @staticmethod
    def get_amount_text_annot(user):
        return AnnotatedText.objects.filter(user__exact=user).all().count()

    @staticmethod
    def get_amount_docs_submitted(user):
        return AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()

class UserAmountAnnotSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    amount_docs = serializers.SerializerMethodField()
    amount_submitted = serializers.SerializerMethodField()
    amount_text_annot_by_label = serializers.SerializerMethodField()
    amount_unannotated_submitted = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('username','first_name','last_name','amount_docs',\
                  'amount_submitted','amount_text_annot_by_label',\
                  'amount_unannotated_submitted')

    @staticmethod
    def get_username(user):
        return user.username

    @staticmethod
    def get_first_name(user):
        return user.first_name

    @staticmethod
    def get_last_name(user):
        return user.last_name

    @staticmethod
    def get_amount_docs(user):
        projects_assigned_to_user = user.projects.all()
        return Doc.objects.filter(projects__in=projects_assigned_to_user).all().count()

    @staticmethod
    def get_amount_submitted(user):
        return AnnotatedText.objects.filter(user__exact=user).values("doc").distinct().count()

    @staticmethod
    def get_amount_text_annot_by_label(user):
        return AnnotatedText.objects.filter(user__exact=user).exclude(content="").values('annotation_label__name').annotate(count=Count('annotation_label__name'))

    @staticmethod
    def get_amount_unannotated_submitted(user):
        return AnnotatedText.objects.filter(user__exact=user,content="").values("doc").distinct().count()