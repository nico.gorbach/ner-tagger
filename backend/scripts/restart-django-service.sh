docker-compose down
sleep 5
docker volume rm backend_postgres
docker-compose run -p 5432:5432 -d postgres

./rm_migrations.sh
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser --username=labeling --email=labeling@email.com
python manage.py loaddata ./fixtures/sample_data.json

