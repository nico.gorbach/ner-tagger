#------------------------------------------------------------------------
#                  CONSTANTS: PROJECT AND APP NAMES
#------------------------------------------------------------------------
PROJ_NAME=labeling_app
APP_NAME1=users
APP_NAME2=project
APP_NAME3=Task
APP_NAME4=annotation_label

#------------------------------------------------------------------------
#                           CREATE PROJECT
#------------------------------------------------------------------------
./create_django_project_with_jwt_token.sh $PROJ_NAME

#------------------------------------------------------------------------
#                           CREATE APPS
#------------------------------------------------------------------------
./create_django_user_app.sh $PROJ_NAME $APP_NAME1
./create_django_post_app.sh $PROJ_NAME $APP_NAME2
./create_django_post_app.sh $PROJ_NAME $APP_NAME3
./create_django_post_app.sh $PROJ_NAME $APP_NAME4

#------------------------------------------------------------------------
#                           RUN SERVER
#------------------------------------------------------------------------
rm db.sqlite3
python manage.py makemigrations
python manage.py migrate

python manage.py runserver

